package it.armellini.isvb.SwingDaFileTesto;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

public class SalvaDatiSuTesto {

	public static void main(String[] args) {

		//Salvo informazioni su file
		List<Anagrafica> tempList = new Vector<Anagrafica>();
		tempList.add(new Anagrafica("Mario", "Rossi", "077587887"));
		tempList.add(new Anagrafica("Luigi", "Bianchi", "077587985"));
		tempList.add(new Anagrafica("Marco", "Verdi", "077587455"));
		tempList.add(new Anagrafica("Andrea", "Chiari", "077587847"));
		tempList.add(new Anagrafica("Matteo", "Gialli", "077587965"));
		tempList.add(new Anagrafica("Roberto", "Grigi", "077587955"));
		PersonaService ps = new PersonaService();
		try {
			ps.writePersone(tempList);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		
		
	}
}
