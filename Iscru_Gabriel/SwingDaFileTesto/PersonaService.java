package it.armellini.isvb.SwingDaFileTesto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

public class PersonaService {
	public static final String NOME_FILE = "Anagrafica.txt";

	private BufferedReader fbr = null;
	private BufferedWriter fbw = null;

	private void openFile(boolean inScrittura) throws IOException, FileNotFoundException {
		if (inScrittura) {
			try {
				this.fbw = new BufferedWriter(new FileWriter(NOME_FILE));
			} catch (IOException exc) {
				System.out.println(String.format("Durante l'apertura del file %s si verificato l'errore %s",
						new Object[] { NOME_FILE, exc.getMessage() }));
				throw exc;
			}
		} else {
			try {
				this.fbr = new BufferedReader(new FileReader(NOME_FILE));
			} catch (FileNotFoundException exc) {
				System.err.println(String.format("Durante l'apertura del file %s si verificato l'errore %s",
						new Object[] { NOME_FILE, exc.getMessage() }));
				throw exc;
			}
		}
	}

	public void listaFileNellaDirectory() {
		String[] contenutoDir = new File("").getAbsoluteFile().list();
		for (int i = 0; i < contenutoDir.length; i++) {
			if (contenutoDir[i].contains(".txt")) {
			System.out.println(contenutoDir[i]);
			}
		}
	}
	
	public void writePersone(List<Anagrafica> vectPersone) throws IOException {
		openFile(true);
		for (Anagrafica unaPersona : vectPersone) {
			this.fbw.write(unaPersona.toString());
			this.fbw.newLine();
		}
		this.fbw.flush();
		this.fbw.close();
	}

	public List<Anagrafica> readPersone() throws IOException {
		openFile(false);
		List<Anagrafica> resultPersone = new Vector<>();
		String row = this.fbr.readLine();
		while (row != null) {
			Anagrafica newPersona = new Anagrafica(row);
			resultPersone.add(newPersona);
			row = this.fbr.readLine();
		}
		this.fbr.close();
		return resultPersone;
	}
}
