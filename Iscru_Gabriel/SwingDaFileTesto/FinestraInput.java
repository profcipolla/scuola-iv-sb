package it.armellini.isvb.SwingDaFileTesto;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FinestraInput extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JPanel panello = new JPanel();
	private JLabel lNome = new JLabel("Nome");
	private JLabel lCognome = new JLabel("Cognome");
	private JLabel lTelefono = new JLabel("Telefono");
	private JTextField tNome = new JTextField();
	private JTextField tCognome = new JTextField();
	private JTextField tTelefono = new JTextField();
	private JButton bPrecedente = new JButton("<");
	private JButton bSuccessivo = new JButton(">");

	private List<Anagrafica> tempList;
	private int posizione = 0;

	public FinestraInput() {
		setTitle("Angrafica");
		this.panello.setLayout(new GridLayout(4, 2));
		setSize(400, 400);
		panello.add(lNome);
		panello.add(tNome);
		panello.add(lCognome);
		panello.add(tCognome);
		panello.add(lTelefono);
		panello.add(tTelefono);
		panello.add(bPrecedente);
		panello.add(bSuccessivo);
		add(panello);
		bPrecedente.addActionListener(this);
		bSuccessivo.addActionListener(this);
		init(posizione);
		setVisible(true);
	}

	private void init(int posizione) {
		PersonaService ps = new PersonaService();
		try {
			tempList = ps.readPersone();
			tNome.setText(tempList.get(posizione).getNome());
			tCognome.setText(tempList.get(posizione).getCognome());
			tTelefono.setText(tempList.get(posizione).getTelefono());
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		if (source == bPrecedente) {
			if(posizione-1 == -1) {
				return;
			}
			init(posizione -= 1);
				
		} else {
			if(posizione+1 > tempList.size()-1) {
				return;
			}
			init(posizione += 1);

		}
	}
	public static void main(String[] a) {
		new FinestraInput();
		new PersonaService().listaFileNellaDirectory();
	}
}
