package giocoDel15;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class GiocoDel15 extends JFrame implements ActionListener {
	private static final int RIGHE = 4;
	private static final int COLONNE = 4;

	private JButton[][] array2D = new JButton[RIGHE][COLONNE];
	private JButton jBottonePremuto;
	private JButton jBottoneBianco;
	private int[] pP;
	private int[] cB;
	private int[] numUsati = new int[17];// appoggio numeri per confronto

	public GiocoDel15() {
		setTitle("Gioco del Quindici");
		setVisible(true);
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(RIGHE, COLONNE));
		creaBottoni();

	}

	private void creaBottoni() {
		for (int r = 0; r < RIGHE; r++) {
			for (int c = 0; c < COLONNE; c++) {
				int i = (int) (Math.random() * 16 + 1); // metti in "i" un valore compreso tra 0-16
				while (i == numUsati[i]) { // se il valore della "i" è presente nella posizione "i" dell'array numUsati
					i = (int) (Math.random() * 16 + 1); // allora mischia è dammi un altro valore
				}
				numUsati[i] = i; // salva il valore della "i" dentro l'array numUsati[i]
				if (i != 0 && i < 16) { // verifica che la "i" non contenga 0 e 16
					JButton bottone = new JButton(String.valueOf(i));
					bottone.addActionListener(this);
					array2D[r][c] = bottone;
					add(bottone);
				} else if (i == 16) { // verifica che la "i" contenga 16
					JButton bottone = new JButton();// assegna una casella senza testo
					bottone.setBackground(Color.WHITE);
					bottone.addActionListener(this);
					array2D[r][c] = bottone;
					add(bottone);
				}
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		jBottonePremuto = (JButton) e.getSource();
		pP = cercaPosizioneBottone(jBottonePremuto); // posizione premuta
		cB = cercaCasellaBianca(); // metodo risalente alla posizione vuota
		jBottoneBianco = array2D[cB[0]][cB[1]];
		spostamento(); // metodo per verificare la prossimità tra il pulsante premuto e il pulsante
						// vuoto

		System.out.println(String.format("premuto jbotton: %s,%s,casella bianca: %s,%s", pP[0], pP[1], cB[0], cB[1]));
	}

	private int[] cercaPosizioneBottone(JButton posizioneBottone) {
		for (int r = 0; r < RIGHE; r++) {
			for (int c = 0; c < COLONNE; c++) {
				if (posizioneBottone == array2D[r][c]) {
					return new int[] { r, c };
				}
			}
		}
		return null;
	}

	private int[] cercaCasellaBianca() {
		for (int r = 0; r < RIGHE; r++) {
			for (int c = 0; c < COLONNE; c++) {
				if (array2D[r][c].getText().equals("")) {
					return new int[] { r, c };
				}
			}
		}
		return null;
	}
	
	private void spostamento() {// prossimità
		boolean vero = true;

		vero &= (pP[1] == cB[1] || pP[0] == cB[0]);

		if (vero) {
			movimentoV();
			movimentoH();
		}

	}
//	private void sonoVicini() {// prossimità
//		boolean vero = true;
//
//		vero &= (pP[0] + 1 == cB[0] && pP[1] == cB[1]) || (pP[0] - 1 == cB[0] && pP[1] == cB[1])
//				|| (pP[0] == cB[0] && pP[1] + 1 == cB[1]) || (pP[0] == cB[0] && pP[1] - 1 == cB[1]);
//		if (vero) {
//
//			jBottoneBianco.setText(jBottonePremuto.getText());
//			jBottoneBianco.setBackground(jBottonePremuto.getBackground());
//			jBottoneBianco.setForeground(jBottonePremuto.getForeground());
//
//			jBottonePremuto.setText("");
//			jBottonePremuto.setBackground(Color.WHITE);
//			jBottonePremuto.setForeground(Color.WHITE);
//
//		}
//	}

	
	public void movimentoH () {
		if (cB[1] < pP[1]) {
			for (int c = cB[1]; c < pP[1]; c++) {
				String temp = array2D[cB[0]][c + 1].getText();
				array2D[cB[0]][c].setText(temp);
			}

			jBottoneBianco.setBackground(jBottonePremuto.getBackground());
			jBottoneBianco.setForeground(jBottonePremuto.getForeground());

			jBottonePremuto.setText("");
			jBottonePremuto.setBackground(Color.WHITE);
			jBottonePremuto.setForeground(Color.WHITE);
		} else if (cB[1] > pP[1]) {
			for (int c = cB[1]; c > pP[1]; c--) {
				String temp = array2D[cB[0]][c - 1].getText();
				array2D[cB[0]][c].setText(temp);
			}

			jBottoneBianco.setBackground(jBottonePremuto.getBackground());
			jBottoneBianco.setForeground(jBottonePremuto.getForeground());

			jBottonePremuto.setText("");
			jBottonePremuto.setBackground(Color.WHITE);
			jBottonePremuto.setForeground(Color.WHITE);
		}
	}
	
	public void movimentoV () {
		if (cB[0] < pP[0]) {
			for (int r = cB[0]; r < pP[0]; r++) {
				String temp = array2D[r + 1][cB[1]].getText();
				array2D[r][cB[1]].setText(temp);
			}

			jBottoneBianco.setBackground(jBottonePremuto.getBackground());
			jBottoneBianco.setForeground(jBottonePremuto.getForeground());

			jBottonePremuto.setText("");
			jBottonePremuto.setBackground(Color.WHITE);
			jBottonePremuto.setForeground(Color.WHITE);
		} else if (cB[0] > pP[0]) {
			for (int r = cB[0]; r > pP[0]; r--) {
				String temp = array2D[r - 1][cB[1]].getText();
				array2D[r][cB[1]].setText(temp);
			}

			jBottoneBianco.setBackground(jBottonePremuto.getBackground());
			jBottoneBianco.setForeground(jBottonePremuto.getForeground());

			jBottonePremuto.setText("");
			jBottonePremuto.setBackground(Color.WHITE);
			jBottonePremuto.setForeground(Color.WHITE);
		}
	}
	
	public static void main(String[] args) { // avvio interno
		new GiocoDel15();

	}
}
