package it.armellini.ivsb.liste_Persone;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraPersona extends JFrame implements ActionListener {
	LeListe lst = new LeListe();
	
	private JPanel panelloTitolo = new JPanel();
	private JPanel panelloInserimentoDati = new JPanel();
	
	private JLabel lableTitolo = new JLabel("Persona");
	private JLabel labelNome = new JLabel("Nome:");
	private JLabel labelCognome = new JLabel("Cognome:");
	
	private JTextField textNome;
	private JTextField textCognome;
	
	private JButton bottoneInizio = new JButton("Inizio");
	private JButton bottoneAvanti = new JButton("Avanti");
	
	Font fontGenerici = new Font("Solomon Sans Normal", Font.BOLD, 18);
	Color coloreSfondoJText = new Color(250, 250, 210);
	

	public FinestraPersona() {
		getContentPane().setBackground(new Color(169, 169, 169));
		setBackground(new Color(169, 169, 169));
		setTitle("Lista Persone");
		setSize(400, 400);
		getContentPane().setLayout(null);
		setLocation(700, 700);
		setResizable(false);
		

		panelloTitolo.setBackground(new Color(119, 136, 153));
		panelloTitolo.setBounds(0, 0, 384, 60);
		panelloTitolo.setLayout(null);

		lableTitolo.setForeground(new Color(240, 230, 140));
		lableTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		lableTitolo.setFont(new Font("Solomon Sans Normal", Font.BOLD, 28));
		lableTitolo.setBounds(0, 0, 384, 60);

		panelloInserimentoDati.setBackground(Color.ORANGE);
		panelloInserimentoDati.setBounds(0, 60, 384, 226);
		panelloInserimentoDati.setLayout(new GridLayout(2, 2, 0, 0));

		labelNome.setFont(fontGenerici);
		labelNome.setHorizontalAlignment(SwingConstants.CENTER);
		labelCognome.setFont(fontGenerici);
		labelCognome.setHorizontalAlignment(SwingConstants.CENTER);
		

		textNome = new JTextField();
		textNome.setBackground(coloreSfondoJText);
		textNome.setFont(fontGenerici);
		textNome.setHorizontalAlignment(SwingConstants.CENTER);
		textCognome = new JTextField();
		textCognome.setBackground(coloreSfondoJText);
		textCognome.setFont(fontGenerici);
		textCognome.setHorizontalAlignment(SwingConstants.CENTER);
		

		bottoneInizio.setBackground(new Color(255, 140, 0));
		bottoneInizio.setFont(fontGenerici);
		bottoneInizio.setBounds(44, 297, 113, 50);
		
		bottoneAvanti.setBackground(new Color(255, 140, 0));
		bottoneAvanti.setFont(fontGenerici);
		bottoneAvanti.setBounds(232, 296,113, 50);
		
		
		add(panelloTitolo);
		add(panelloInserimentoDati);
		
		add(bottoneInizio);
		add(bottoneAvanti);
		
		panelloTitolo.add(lableTitolo);
		
		panelloInserimentoDati.add(labelNome);
		panelloInserimentoDati.add(textNome);
		panelloInserimentoDati.add(labelCognome);
		panelloInserimentoDati.add(textCognome);
		

		bottoneInizio.addActionListener(this);
		bottoneAvanti.addActionListener(this);
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Persona prs = lst.inzio();
		textNome.setText(prs.getNome());
		textCognome.setText(prs.getCognome());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == bottoneInizio) {
			Persona prs = lst.inzio();
			textNome.setText(prs.getNome());
			textCognome.setText(prs.getCognome());
		} else {
		Persona prs = lst.avanti();
		
		textNome.setText(prs.getNome());
		textCognome.setText(prs.getCognome());
		}


	}

}
