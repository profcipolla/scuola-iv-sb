package it.armellini.ivsb.liste_Persone;



public class Avvio {

	public static void main(String[] args) {
		LeListe lst = new LeListe();
		
		lst.aggiungiPersona(new Persona("Andrea","Bianchi"));

		lst.aggiungiPersona(new Persona("Freddy","Rossi"));
		
		lst.aggiungiPersona(new Persona("Michele","Verdi"));
		
		lst.aggiungiPersona(new Persona("Dario","Grigi"));
		
		lst.aggiungiPersona(new Persona("Valerio","Galli"));
		
		new FinestraPersona();
	}

}
