package it.armellini.ivsb.liste_Persone;

public class LeListe {
	private static Nodo head;
	private static Nodo tmp;
	
	public void aggiungiPersona(Persona insPersona) {
 		Nodo nuovoNodo = new Nodo(insPersona, head);
		head = nuovoNodo;
		tmp = head;
	}

	public Persona avanti() {
		
		Persona prs = tmp.getPersone();
		if (null != tmp.getProssimoNodo()) {
		tmp = tmp.getProssimoNodo();
		}
		return prs;
		
	}
	
	public Persona inzio() {
		
		tmp = head;
		Persona prs = tmp.getPersone();
		tmp = tmp.getProssimoNodo();
		return prs;
	}
}
