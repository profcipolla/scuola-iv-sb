package it.armellini.ivsb.liste_Persone;

public class Nodo {
	private Persona persone;
	private Nodo prossimoNodo;
	
	public Nodo (Persona insPersone, Nodo proxNodo) {
		this.setPersone(insPersone);
		this.setProssimoNodo(proxNodo);
	}

	public Persona getPersone() {
		return persone;
	}

	public void setPersone(Persona persone) {
		this.persone = persone;
	}

	public Nodo getProssimoNodo() {
		return prossimoNodo;
	}

	public void setProssimoNodo(Nodo prossimoNodo) {
		this.prossimoNodo = prossimoNodo;
	}
	
}
