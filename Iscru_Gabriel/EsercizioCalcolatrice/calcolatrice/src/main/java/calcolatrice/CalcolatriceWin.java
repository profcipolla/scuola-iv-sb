package calcolatrice;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalcolatriceWin extends JFrame implements ActionListener {
	private static final String[] scritte = { "7", "8", "9", "/", "4", "5", "6", "*", "1", "2", "3", "+", ".", "0", "=",
			"-" };
	private double num1 = 0.0, num2 = 0.0, resto = 0.0;
	private String tipoOperazione;
	private JButton[] pulsanti = new JButton[16];
	private JTextField display = new JTextField(5);
	private JButton pulsanteCE;

	public CalcolatriceWin(String titolo) {
		setLayout(new GridLayout(2, 1));
		display.setBounds(50, 25, 300, 50);
		add(display);

		JPanel pnlPulsanti = new JPanel();
		pnlPulsanti.setBounds(50, 100, 300, 300);
		add(pnlPulsanti);

		pnlPulsanti.setLayout(new GridLayout(4, 4));
		for (int i = 0; i < scritte.length; i++) {
			pulsanti[i] = new JButton(scritte[i]);
			pnlPulsanti.add(pulsanti[i]);
			pulsanti[i].setFont(new Font("Verdana", Font.BOLD, 26));
			pulsanti[i].addActionListener(this);
		}

		pulsanti[3].setBackground(Color.LIGHT_GRAY);
		pulsanti[7].setBackground(Color.LIGHT_GRAY);
		pulsanti[11].setBackground(Color.LIGHT_GRAY);
		pulsanti[15].setBackground(Color.LIGHT_GRAY);
		pulsanti[14].setBackground(Color.orange);

		pulsanteCE = new JButton("CE");
		pulsanteCE.setBounds(50, 430, 100, 50);
		pulsanteCE.addActionListener(this);
		Color colore = new Color(233, 116, 81);
		pulsanteCE.setBackground(colore);
		pulsanteCE.setFont(new Font("Verdana", Font.BOLD, 26));
		add(pulsanteCE);
		setLocation(700, 400);

		setTitle(titolo);
		setSize(415, 550);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	// TODO:
	// possibilit� di inserire un solo separatore decimale -- ok
	// sul display cifre e separatore -- ok
	// il + mette da parte il valore nel display, ripulisce il display -- ok
	// = se sono disponibili due valori mi dar� la somma -- ok

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source instanceof JButton) {
			JButton bottonePremuto = (JButton) source;
			String contenutoDisplay = display.getText();
			String cifra = bottonePremuto.getText();

			display.setFont(new Font("Age", Font.BOLD, 40));// font per il display

			switch (cifra) { // metodo per la ricerca del numero in formato String
			case "0":
				display.setText(contenutoDisplay + cifra);
				break;
			case "1":
				display.setText(contenutoDisplay + cifra);
				break;
			case "2":
				display.setText(contenutoDisplay + cifra);
				break;
			case "3":
				display.setText(contenutoDisplay + cifra);
				break;
			case "4":
				display.setText(contenutoDisplay + cifra);
				break;
			case "5":
				display.setText(contenutoDisplay + cifra);
				break;
			case "6":
				display.setText(contenutoDisplay + cifra);
				break;
			case "7":
				display.setText(contenutoDisplay + cifra);
				break;
			case "8":
				display.setText(contenutoDisplay + cifra);
				break;
			case "9":
				display.setText(contenutoDisplay + cifra);
				break;
			}

			if (cifra.equals("+")) {// se il simbolo + e uguale al bottone del index attuale... allora eseguo il
									// contenuto
				num1 = Double.parseDouble(contenutoDisplay); // converto in Double da String e salvo il contenuto del
																// display dentro una variabile
				tipoOperazione = "addizione"; // salvo la stringa su un variabile
				display.setText(""); // libero il display
				bottonePremuto.setBackground(Color.GREEN);// un po di modding quando si preme il bottone
			} else if (cifra.equals("-")) {
				num1 = Double.parseDouble(contenutoDisplay);
				tipoOperazione = "sottrazione";
				display.setText("");
				bottonePremuto.setBackground(Color.GREEN);
			} else if (cifra.equals("*")) {
				num1 = Double.parseDouble(contenutoDisplay);
				tipoOperazione = "moltiplicazione";
				display.setText("");
				bottonePremuto.setBackground(Color.GREEN);
			} else if (cifra.equals("/")) {
				num1 = Double.parseDouble(contenutoDisplay);
				tipoOperazione = "divisione";
				display.setText("");
				bottonePremuto.setBackground(Color.GREEN);
			}

			if (cifra.equals("=")) {
				num2 = Double.parseDouble(contenutoDisplay);
				if (tipoOperazione.equals("addizione")) {
					resto = num1 + num2;
				}
				if (tipoOperazione.contains("sottrazione")) {
					resto = num1 - num2;
				}
				if (tipoOperazione.contains("moltiplicazione")) {
					resto = num1 * num2;
				}
				if (tipoOperazione.contains("divisione")) {
					resto = num1 / num2;
				}
			}

			if (!contenutoDisplay.contains(".")) {
				if (contenutoDisplay.equals("")) {
					display.setText(contenutoDisplay + cifra);
				}
			}

			if (cifra.equals("=")) {
				num2 = Double.parseDouble(contenutoDisplay);//inserisco il valore che � presente sul display nella variabile
				display.setText(String.valueOf(resto));//immetto sul display il risultato della operazione svolata tra la var num1 e num2
				pulsanti[3].setBackground(Color.LIGHT_GRAY);
				pulsanti[7].setBackground(Color.LIGHT_GRAY);
				pulsanti[11].setBackground(Color.LIGHT_GRAY);
				pulsanti[15].setBackground(Color.LIGHT_GRAY);
			}

			if (bottonePremuto.equals(pulsanteCE)) { // bottone cancella tutto
				num2 = 0.0;
				resto = 0.0;
				cifra = "";
				contenutoDisplay = "";
				display.setText("");
			}

			System.out.println("Hai premuto un pulsante " + cifra);
		} else if (source instanceof JTextField) {
			JTextField text = (JTextField) source;
			System.out.println("Hai premuto un pulsante " + text.getText());
		}

	}

}