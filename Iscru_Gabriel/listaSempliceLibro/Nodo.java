package it.armellini.ivsb.listaSempliceLibro;

public class Nodo {
	private Libro libri;
	private Nodo prossimoNodo;
	
	public Nodo (Libro inslib, Nodo next) {
		this.setLibri(inslib);
		this.setProssimoNodo(next);
	}

	public Libro getLibri() {
		return libri;
	}

	public void setLibri(Libro libri) {
		this.libri = libri;
	}

	public Nodo getProssimoNodo() {
		return prossimoNodo;
	}

	public void setProssimoNodo(Nodo prossimoNodo) {
		this.prossimoNodo = prossimoNodo;
	}
	
}
