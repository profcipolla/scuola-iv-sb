package it.armellini.ivsb.listaSempliceLibro;



public class AvvioListeLibri {

	public static void main(String[] args) {
		LeListe lst = new LeListe();
		
		lst.aggiungiLibro(new Libro("Vita Nuova",(int)(Math.random()*1000),1294,"SVFG11"));

		lst.aggiungiLibro(new Libro("Divina commedia",(int)(Math.random()*100000),1306,"SVFG13"));
		
		lst.aggiungiLibro(new Libro("Convivio",(int)(Math.random()*1000),1307,"SVFG15"));
		
		lst.aggiungiLibro(new Libro("L'Elegia di Madonna Fiammetta",(int)(Math.random()*100000),1347,"SVFG17"));
		
		lst.aggiungiLibro(new Libro("Decameron",(int)(Math.random()*1000),1307,"SVFG19"));
		
		lst.cercaLibro(1307);
		
		
	}

}
