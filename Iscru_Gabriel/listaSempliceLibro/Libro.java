package it.armellini.ivsb.listaSempliceLibro;

public class Libro {
	private String titolo;
	private int nPag;
	private int anno;
	private String codISBN;
	
	public Libro(String titolo,int nPag,int anno,String codISBN) {
		this.setTitolo(titolo);
		this.setNPag(nPag);
		this.setAnno(anno);
		this.setCodISBN(codISBN);
		
	}
	
	public void setTitolo (String titolo) {
		this.titolo = titolo;
	}
	
	public String getTitolo() {
		return this.titolo;
	}
	
	public void setNPag(int nPag) {
		this.nPag = nPag;
	}
	
	public int getNPag() {
		return nPag;
	}
	
	public void setAnno(int anno) {
		this.anno = anno;
	}
	
	public int getAnno() {
		return anno;
	}
	
	public void setCodISBN(String codISBN) {
		this.codISBN = codISBN;
	}
	
	public String getCodISBN() {
		return codISBN;
	}
	@Override
	public String toString() {
		return String.format("Titolo: %s, Pagine: %d, Anno: %d, ISNB: %s",this.titolo, this.nPag, this.anno, this.codISBN);
	}
	
}
