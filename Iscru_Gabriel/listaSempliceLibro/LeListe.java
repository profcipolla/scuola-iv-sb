package it.armellini.ivsb.listaSempliceLibro;



public class LeListe {
	private Nodo inizio = null;

	public void aggiungiLibro(Libro insLib) {
		Nodo nuovoNodo = new Nodo(insLib, this.inizio);
		this.inizio = nuovoNodo;
	}

	public Libro cercaLibro(int anno) {
		Nodo corrNodo = this.inizio;
		
		while (null != corrNodo) {
			if (corrNodo.getLibri().getAnno() == anno) {
				System.out.println(corrNodo.getLibri());
			}
			corrNodo = corrNodo.getProssimoNodo();
		}
		return null;
	}
}
