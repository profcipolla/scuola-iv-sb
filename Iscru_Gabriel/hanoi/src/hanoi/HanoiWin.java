package hanoi;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class HanoiWin extends JFrame {
	HanoiModel hm = new HanoiModel();
	private JButton[] sinistraB = new JButton[hm.DISCHI];
	private JButton[] centroB = new JButton[hm.DISCHI];
	private JButton[] destraB = new JButton[hm.DISCHI];
	JPanel pnlPulsanti = new JPanel();

	public HanoiWin() {

		setTitle("Hanoi");
		setSize(400, 400);
		setLayout(new GridLayout(1, 1));

		// pnlPulsanti.setBounds(50, 100, 300, 300);
		add(pnlPulsanti);

		pnlPulsanti.setLayout(new GridLayout(hm.DISCHI, 3));
		for (int i = 0; i < sinistraB.length; i++) {
			sinistraB[i] = new JButton();
			centroB[i] = new JButton();
			destraB[i] = new JButton();
			pnlPulsanti.add(sinistraB[i]);
			pnlPulsanti.add(centroB[i]);
			pnlPulsanti.add(destraB[i]);
		}

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);

	}

	// nel costruttore fa un grid layout di DISCHI righe e 3 colonne
	// in ogni cella mette un jbutton

	public void disegna(int[] sinistra, int[] centro, int[] destra) {
		// riscrive le caption nei jbutton per simulare la nuova
		// situazione dei dischi
		for (int i = 0; i < sinistraB.length; i++) {
			if (sinistra[i] != 0) {
				sinistraB[i].setText("" + sinistra[i]);
				sinistraB[i].setBackground(Color.RED);
				sinistraB[i].setForeground(Color.WHITE);
			} else {
				sinistraB[i].setText("");
				sinistraB[i].setBackground(Color.BLACK);
				sinistraB[i].setForeground(Color.BLACK);
			}
			if (centro[i] != 0) {
				centroB[i].setText("" + centro[i]);
				centroB[i].setBackground(Color.RED);
				centroB[i].setForeground(Color.WHITE);
			} else {
				centroB[i].setText("");
				centroB[i].setBackground(Color.BLACK);
				centroB[i].setForeground(Color.BLACK);
			}
			if (destra[i] != 0) {
				destraB[i].setText("" + destra[i]);
				destraB[i].setBackground(Color.RED);
				destraB[i].setForeground(Color.WHITE);
			} else {
				destraB[i].setText("");
				destraB[i].setBackground(Color.BLACK);
				destraB[i].setForeground(Color.BLACK);
			}
		}

	}

}
