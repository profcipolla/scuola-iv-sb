package it.armellini.anagrafica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.armellini.utilita.CFException;
import it.armellini.utilita.CognomeException;
import it.armellini.utilita.MiaTastiera;
import it.armellini.utilita.NatoIlException;
import it.armellini.utilita.NomeException;
import it.armellini.utilita.ResidenzaException;

public class Exceptions_E_Metodi {
	MiaTastiera input = new MiaTastiera();// Input da tastiera
	Persona p = new Persona(); // classe padre controllo
	String nomeT, cognomeT, cfT, ResidenzaT, temp;// var per l'input da tastiera
	Date nascitaT;
	static Studente[] STDang = new Studente[5];// array
	static Professore[] PRFang = new Professore[5];
	public static int iS = 0;// contatore
	public static int iP = 0;

	public void insSTDang() throws Exception { // inserimento studente su array
		iS = 0;
		while (iS < STDang.length && STDang[iS] != null) {
			iS++;
		}
		verificaNome();
		verificaCognome();
		verificaNascita();
		verificaCF();
		verificaResidenza();
		if (iP < STDang.length - 1) {
			STDang[iS] = new Studente(nomeT, cognomeT, nascitaT, cfT, ResidenzaT);
			return;
		}
		System.out.println("Spazio Esaurito!!!\n");
	}

	public void listaStudenti() {
		System.out.println("Lista Studenti:");
		iS = 0;
		while (iS < STDang.length && STDang[iS] != null) {
			System.out.println(STDang[iS].toString());
			iS++;
		}
		if (STDang[0] == null) {
			System.out.println("\nNon sono stati trovati Studenti registrati\n");
		}
	}

	public void insPRFang() throws Exception { // inserimento professore su array
		iP = 0;
		while (iP < PRFang.length && PRFang[iP] != null) {
			iP++;
		}
		verificaNome();
		verificaCognome();
		verificaNascita();
		verificaCF();
		verificaResidenza();

		if (iP < PRFang.length - 1) {
			PRFang[iP] = new Professore(nomeT, cognomeT, nascitaT, cfT, ResidenzaT);
			return;
		}
		System.out.println("Spazio Esaurito!!!\n");
	}

	public void listaProfessori() {
		System.out.println("Lista Professori:");
		iP = 0;
		while (iS < PRFang.length && PRFang[iP] != null) {
			System.out.println(PRFang[iP].toString());
			iP++;
		}
		if (PRFang[0] == null) {
			System.out.println("\nNon sono stati trovati Professori registrati\n");
		}
	}

	// Verifiche tramite exception sul costruttore genitore
	public void verificaNome() throws NomeException {
		try {
			p.setNome(nomeT);
		} catch (NomeException e) {
			System.out.println(e.getMessage());
			return;
		}
	}

	public void verificaCognome() throws CognomeException {
		try {
			p.setCognome(cognomeT);
		} catch (CognomeException e) {
			System.out.println(e.getMessage());
			return;
		}
	}

	public void verificaNascita() throws NatoIlException, ParseException {
		try {
			p.setNascitaTastiera(temp);
			try {
				Locale.setDefault(Locale.ITALY);
				SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
				formato.setLenient(false);
				nascitaT = formato.parse(Persona.appoggio);
				p.setNatoIl(nascitaT);
			} catch (ParseException e) {
				System.out.println("Inserire il formato 'gg/mm/anno': ");
				return;
			}
		} catch (NatoIlException e) {
			System.out.println(e.getMessage());
			return;
		}
	}

	public void verificaCF() throws CFException {
		try {
			p.setCodiceFiscale(cfT);
		} catch (CFException e) {
			System.out.println(e.getMessage());
			return;
		}
	}

	public void verificaResidenza() throws ResidenzaException {
		try {
			p.setResidenza(ResidenzaT);
		} catch (ResidenzaException e) {
			System.out.println(e.getMessage());
			return;
		}
	}
}
