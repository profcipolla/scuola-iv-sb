package it.armellini.anagrafica;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraAnagraficaProfessori extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	Exceptions_E_Metodi operazioni = new Exceptions_E_Metodi();

	private JPanel panelloTitolo = new JPanel();
	private JPanel panelloInserimentoDati = new JPanel();

	private JLabel lableTitolo = new JLabel("Professore");
	private JLabel labelNome = new JLabel("Nome:");
	private JLabel labelCognome = new JLabel("Cognome:");
	private JLabel labelNatoIl = new JLabel("Data di Nascità:");
	private JLabel labelCodiceFiscale = new JLabel("Codice Fiscale:");
	private JLabel labelResidenza = new JLabel("Residenza:");

	private JTextField textNome;
	private JTextField textCognome;
	private JTextField textNatoIl;
	private JTextField textCodiceFiscale;
	private JTextField textResidenza;

	private JButton bottoneSalva = new JButton("Salva");

	Font fontGenerici = new Font("Solomon Sans Normal", Font.BOLD, 18);
	Color coloreSfondoJText = new Color(250, 250, 210);

	public FinestraAnagraficaProfessori() {
		setBackground(new Color(169, 169, 169));
		setTitle("Professore");
		setSize(400, 400);
		setLayout(null);
		setLocation(700, 700);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		panelloTitolo.setBackground(new Color(119, 136, 153));
		panelloTitolo.setBounds(0, 0, 384, 60);
		panelloTitolo.setLayout(null);

		lableTitolo.setForeground(new Color(255, 140, 0));
		lableTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		lableTitolo.setFont(new Font("Solomon Sans Normal", Font.BOLD, 28));
		lableTitolo.setBounds(0, 0, 384, 60);

		panelloInserimentoDati.setBackground(new Color(224, 255, 255));
		panelloInserimentoDati.setBounds(0, 60, 384, 226);
		panelloInserimentoDati.setLayout(new GridLayout(5, 2, 0, 0));

		labelNome.setFont(fontGenerici);
		labelNome.setHorizontalAlignment(SwingConstants.CENTER);
		labelCognome.setFont(fontGenerici);
		labelCognome.setHorizontalAlignment(SwingConstants.CENTER);
		labelNatoIl.setFont(fontGenerici);
		labelNatoIl.setHorizontalAlignment(SwingConstants.CENTER);
		labelCodiceFiscale.setFont(fontGenerici);
		labelCodiceFiscale.setHorizontalAlignment(SwingConstants.CENTER);
		labelResidenza.setFont(fontGenerici);
		labelResidenza.setHorizontalAlignment(SwingConstants.CENTER);

		textNome = new JTextField();
		textNome.setBackground(coloreSfondoJText);
		textNome.setFont(fontGenerici);
		textNome.setHorizontalAlignment(SwingConstants.CENTER);
		textCognome = new JTextField();
		textCognome.setBackground(coloreSfondoJText);
		textCognome.setFont(fontGenerici);
		textCognome.setHorizontalAlignment(SwingConstants.CENTER);
		textNatoIl = new JTextField();
		textNatoIl.setBackground(coloreSfondoJText);
		textNatoIl.setFont(fontGenerici);
		textNatoIl.setHorizontalAlignment(SwingConstants.CENTER);
		textCodiceFiscale = new JTextField();
		textCodiceFiscale.setBackground(coloreSfondoJText);
		textCodiceFiscale.setFont(fontGenerici);
		textCodiceFiscale.setHorizontalAlignment(SwingConstants.CENTER);
		textResidenza = new JTextField();
		textResidenza.setBackground(coloreSfondoJText);
		textResidenza.setFont(fontGenerici);
		textResidenza.setHorizontalAlignment(SwingConstants.CENTER);

		bottoneSalva.setBackground(new Color(255, 140, 0));
		bottoneSalva.setFont(fontGenerici);
		bottoneSalva.setBounds(137, 297, 113, 50);

		add(panelloTitolo);
		panelloTitolo.add(lableTitolo);
		add(panelloInserimentoDati);
		panelloInserimentoDati.add(labelNome);
		panelloInserimentoDati.add(textNome);
		panelloInserimentoDati.add(labelCognome);
		panelloInserimentoDati.add(textCognome);
		panelloInserimentoDati.add(labelNatoIl);
		panelloInserimentoDati.add(textNatoIl);
		panelloInserimentoDati.add(labelCodiceFiscale);
		panelloInserimentoDati.add(textCodiceFiscale);
		panelloInserimentoDati.add(labelResidenza);
		panelloInserimentoDati.add(textResidenza);
		add(bottoneSalva);
		
		bottoneSalva.addActionListener(this);

		
	}

	public void actionPerformed(ActionEvent e) {
			operazioni.nomeT = textNome.getText();
			operazioni.cognomeT = textCognome.getText();
			operazioni.temp = textNatoIl.getText();
			operazioni.cfT = textCodiceFiscale.getText();
			operazioni.ResidenzaT = textResidenza.getText();
			
			try {
				operazioni.insPRFang();
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.getMessage();
			}
			operazioni.listaProfessori();
		}

	}

