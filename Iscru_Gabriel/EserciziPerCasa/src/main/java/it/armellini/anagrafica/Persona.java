package it.armellini.anagrafica;

import java.util.Date;

import it.armellini.utilita.CFException;
import it.armellini.utilita.CognomeException;
import it.armellini.utilita.NatoIlException;
import it.armellini.utilita.NomeException;
import it.armellini.utilita.ResidenzaException;

public class Persona {
	protected String nome;
	protected String cognome;
	protected Date natoIl;
	protected String codiceFiscale;
	protected String residenza;
	public static String appoggio;

	public Persona(String nome, String cognome, Date natoIl, String codiceFiscale, String residenza) throws Exception {
		this.setNome(nome);
		this.setCognome(cognome);
		this.setNatoIl(natoIl);
		this.setCodiceFiscale(codiceFiscale);
		this.setResidenza(residenza);
	}

	public Persona() {
	}

	public void setNome(String nome) throws NomeException {
		if (nome == null || "".equals(nome.trim())) {
			throw new NomeException("Nome non inserito, si prega di inserirlo:\n");
		}
		this.nome = nome.trim();
	}

	public void setCognome(String cognome) throws CognomeException {
		if (cognome == null || "".equals(cognome.trim())) {
			throw new CognomeException("Cognome non inserito, si prega di inserirlo:\n");
		}
		this.cognome = cognome.trim();
	}

	public void setNatoIl(Date natoIl) {
		this.natoIl = natoIl;
	}

	public void setNascitaTastiera(String temp) throws NatoIlException {
		if (null == temp || "".equals(temp.trim())) {
			throw new NatoIlException("Data di nascita non inserita, si prega di inserirla:\n");
		}
		appoggio = temp;
	}

	public void setCodiceFiscale(String codiceFiscale) throws CFException {
		if (null == codiceFiscale || "".equals(codiceFiscale.trim())) {
			throw new CFException("Codice Fiscale non inserito:\n");
		}
		if (16 != codiceFiscale.length()) {
			throw new CFException("Codice Fiscale non completo o superiore ai 16 caratteri:\n");
		}
		this.codiceFiscale = codiceFiscale.trim();
	}

	public void setResidenza(String residenza) throws ResidenzaException {
		if (residenza == null || "".equals(residenza.trim())) {
			throw new ResidenzaException("La residenza non è valida, si prega di inserirlo:\n");
		}
		this.residenza = residenza.trim();
	}

}
