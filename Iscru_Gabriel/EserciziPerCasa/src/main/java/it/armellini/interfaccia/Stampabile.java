package it.armellini.interfaccia;

public interface Stampabile {
	void print();
	int numeroCopie();
}
