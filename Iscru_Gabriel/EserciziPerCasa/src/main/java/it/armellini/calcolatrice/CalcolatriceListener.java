package it.armellini.calcolatrice;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

public class CalcolatriceListener implements ActionListener {

	private JTextField displayCalcolatrice;

	public CalcolatriceListener(JTextField display) {
		this.displayCalcolatrice = display;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Anche questo listener ascolta");
	}

}
