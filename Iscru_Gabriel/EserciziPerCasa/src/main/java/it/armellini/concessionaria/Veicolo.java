package it.armellini.concessionaria;

public class Veicolo{
    // Proprietà / Attributo
    private float prezzo;
    private Integer annoProduzione;
        
    private CasaAutomobilistica dati;
    private Motore parteFisica;
    
    // Costruttore 
    public Veicolo(Integer annoProduzione,float prezzo,Motore parteFisica){
        this.setAnnoProduzione(annoProduzione);
        this.setPrezzo(prezzo);
        this.setParteFisica(parteFisica);
    }
    // getter
    public Integer getAnnoProduzione() {
        return this.annoProduzione;
    }
    
    public float getPrezzo() {
        return this.prezzo;
    }
    
    public Motore getParteFisica() {
        return this.parteFisica;
    }
    
    // setter
    public void setAnnoProduzione(Integer annoProduzione) {
        this.annoProduzione = annoProduzione;
    }
    
    public void setPrezzo(float prezzo) {
        this.prezzo = prezzo;
    }
    
    public void setParteFisica(Motore parteFisica) {
        this.parteFisica = parteFisica;
    }
}