package it.armellini.concessionaria;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraAutoveicolo extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	CasaAutomobilistica ca = new CasaAutomobilistica();
	Motore mt = new Motore();
	OperazioniConcessionaria op = new OperazioniConcessionaria();

	private JPanel panelloTitolo = new JPanel();
	private JPanel panelloInserimentoDati1 = new JPanel();
	private JPanel panelloInserimentoDati2 = new JPanel();
	private JPanel panelloInserimentoDati3 = new JPanel();

	private JLabel labelTitolo = new JLabel("Gestione Concessionaria");
	private JLabel labelMotore = new JLabel("Caratteristiche Motore");
	private JLabel labelAuto = new JLabel("Caratteristiche Auto");

	private JLabel labelCasaAutomobilisticaDenominazione = new JLabel("Casa Automobilistica");
	private JLabel labelCasaAutomobilisticaNazione = new JLabel("Nazione");

	private JLabel labelAutoPrezzo = new JLabel("Prezzo");
	private JLabel labelAutoAnno = new JLabel("Anno Produzione");
	private JLabel labelAutoNrPosti = new JLabel("Numero Posti");
	private JLabel labelAutoColore = new JLabel("Colore");

	private JLabel labelMotoreCilindrata = new JLabel("Cilindrata");
	private JLabel labelMotoreCavalli = new JLabel("Cavalli");
	private JLabel labelMotoreTipoAlimentazione = new JLabel("Tipo Alimentazione");

	private JTextField textCasaAutomobilisticaDenominazione = new JTextField();
	private JTextField textCasaAutomobilisticaNazione = new JTextField();

	private JTextField textAutoAnno = new JTextField();
	private JTextField textAutoPrezzo = new JTextField();
	private JTextField textAutoNrPosti = new JTextField();
	private JTextField textAutoColore = new JTextField();

	private JTextField textMotoreCilindrata = new JTextField();
	private JTextField textMotoreCavalli = new JTextField();
	private JTextField textMotoreTipoAlimentazione = new JTextField();

	private JButton bottoneSalva = new JButton("Salva");

	public FinestraAutoveicolo() {
		setTitle("Concessionario");
		setSize(841, 575);
		setLocation(700, 700);
		setLayout(null);
		setBackground(new Color(0, 149, 182));
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Color colorePannelliInserimento = new Color(94, 134, 193);
		Color coloreSfondoJtext = new Color(255, 253, 208);
		Font fontLabelEText = new Font("Solomon Sans Normal", Font.BOLD, 13);

		panelloTitolo.setBackground(Color.GRAY);
		panelloTitolo.setBounds(0, 0, 826, 95);
		panelloTitolo.setLayout(null);

		panelloInserimentoDati1.setBackground(colorePannelliInserimento);
		panelloInserimentoDati1.setBounds(0, 170, 412, 118);
		panelloInserimentoDati1.setLayout(new GridLayout(2, 2, 0, 0));

		panelloInserimentoDati2.setBackground(colorePannelliInserimento);
		panelloInserimentoDati2.setBounds(414, 170, 412, 188);
		panelloInserimentoDati2.setLayout(new GridLayout(3, 2, 0, 0));

		panelloInserimentoDati3.setBounds(0, 291, 412, 213);
		panelloInserimentoDati3.setBackground(colorePannelliInserimento);
		panelloInserimentoDati3.setLayout(new GridLayout(4, 2, 0, 0));

		labelTitolo.setBounds(0, 5, 829, 90);
		labelTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitolo.setFont(new Font("Solomon Sans Thin", Font.PLAIN, 30));
		labelTitolo.setForeground(new Color(255, 140, 0));

		labelAuto.setBounds(0, 98, 412, 71);
		labelAuto.setHorizontalAlignment(SwingConstants.CENTER);
		labelAuto.setFont(fontLabelEText);

		labelAutoAnno.setHorizontalAlignment(SwingConstants.CENTER);
		labelAutoAnno.setFont(fontLabelEText);
		textAutoAnno.setBackground(coloreSfondoJtext);
		textAutoAnno.setFont(fontLabelEText);
		textAutoAnno.setHorizontalAlignment(SwingConstants.CENTER);

		labelAutoPrezzo.setHorizontalAlignment(SwingConstants.CENTER);
		labelAutoPrezzo.setFont(fontLabelEText);
		textAutoPrezzo.setBackground(coloreSfondoJtext);
		textAutoPrezzo.setFont(fontLabelEText);
		textAutoPrezzo.setHorizontalAlignment(SwingConstants.CENTER);

		labelAutoNrPosti.setHorizontalAlignment(SwingConstants.CENTER);
		labelAutoNrPosti.setFont(fontLabelEText);
		textAutoNrPosti.setBackground(coloreSfondoJtext);
		textAutoNrPosti.setFont(fontLabelEText);
		textAutoNrPosti.setHorizontalAlignment(SwingConstants.CENTER);

		labelAutoColore.setHorizontalAlignment(SwingConstants.CENTER);
		labelAutoColore.setFont(fontLabelEText);
		textAutoColore.setBackground(coloreSfondoJtext);
		textAutoColore.setFont(fontLabelEText);
		textAutoColore.setHorizontalAlignment(SwingConstants.CENTER);

		labelCasaAutomobilisticaDenominazione.setHorizontalAlignment(SwingConstants.CENTER);
		labelCasaAutomobilisticaDenominazione.setFont(fontLabelEText);
		textCasaAutomobilisticaDenominazione.setBackground(coloreSfondoJtext);
		textCasaAutomobilisticaDenominazione.setFont(fontLabelEText);
		textCasaAutomobilisticaDenominazione.setHorizontalAlignment(SwingConstants.CENTER);

		labelCasaAutomobilisticaNazione.setHorizontalAlignment(SwingConstants.CENTER);
		labelCasaAutomobilisticaNazione.setFont(fontLabelEText);
		textCasaAutomobilisticaNazione.setBackground(coloreSfondoJtext);
		textCasaAutomobilisticaNazione.setFont(fontLabelEText);
		textCasaAutomobilisticaNazione.setHorizontalAlignment(SwingConstants.CENTER);

		labelMotore.setHorizontalAlignment(SwingConstants.CENTER);
		labelMotore.setFont(fontLabelEText);
		labelMotore.setBounds(414, 98, 412, 71);

		labelMotoreCilindrata.setHorizontalAlignment(SwingConstants.CENTER);
		labelMotoreCilindrata.setFont(fontLabelEText);
		textMotoreCilindrata.setBackground(coloreSfondoJtext);
		textMotoreCilindrata.setFont(fontLabelEText);
		textMotoreCilindrata.setHorizontalAlignment(SwingConstants.CENTER);

		labelMotoreCavalli.setHorizontalAlignment(SwingConstants.CENTER);
		labelMotoreCavalli.setFont(fontLabelEText);
		textMotoreCavalli.setBackground(coloreSfondoJtext);
		textMotoreCavalli.setFont(fontLabelEText);
		textMotoreCavalli.setHorizontalAlignment(SwingConstants.CENTER);

		labelMotoreTipoAlimentazione.setHorizontalAlignment(SwingConstants.CENTER);
		labelMotoreTipoAlimentazione.setFont(fontLabelEText);
		textMotoreTipoAlimentazione.setBackground(coloreSfondoJtext);
		textMotoreTipoAlimentazione.setFont(fontLabelEText);
		textMotoreTipoAlimentazione.setHorizontalAlignment(SwingConstants.CENTER);

		bottoneSalva.setFont(new Font("Solomon Sans Normal", Font.BOLD, 18));
		bottoneSalva.setBounds(532, 415, 192, 83);

		panelloTitolo.add(labelTitolo);

		panelloInserimentoDati1.add(labelCasaAutomobilisticaDenominazione);
		panelloInserimentoDati1.add(textCasaAutomobilisticaDenominazione);
		panelloInserimentoDati1.add(labelCasaAutomobilisticaNazione);
		panelloInserimentoDati1.add(textCasaAutomobilisticaNazione);

		panelloInserimentoDati3.add(labelAutoAnno);
		panelloInserimentoDati3.add(textAutoAnno);
		panelloInserimentoDati3.add(labelAutoPrezzo);
		panelloInserimentoDati3.add(textAutoPrezzo);
		panelloInserimentoDati3.add(labelAutoNrPosti);
		panelloInserimentoDati3.add(textAutoNrPosti);
		panelloInserimentoDati3.add(labelAutoColore);
		panelloInserimentoDati3.add(textAutoColore);

		panelloInserimentoDati2.add(labelMotoreCilindrata);
		panelloInserimentoDati2.add(textMotoreCilindrata);
		panelloInserimentoDati2.add(labelMotoreCavalli);
		panelloInserimentoDati2.add(textMotoreCavalli);
		panelloInserimentoDati2.add(labelMotoreTipoAlimentazione);
		panelloInserimentoDati2.add(textMotoreTipoAlimentazione);

		add(labelMotore);
		add(labelAuto);

		add(panelloTitolo);
		add(panelloInserimentoDati1);
		add(panelloInserimentoDati2);
		add(panelloInserimentoDati3);
		add(bottoneSalva);

		bottoneSalva.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ca.setDenominazione(textCasaAutomobilisticaDenominazione.getText());
		ca.setNazione(textCasaAutomobilisticaDenominazione.getText());

		Integer anno = Integer.parseInt(textAutoAnno.getText());
		Float prezzo = Float.parseFloat(textAutoPrezzo.getText());
		String colore = textAutoColore.getText();
		int numPosti = Integer.parseInt(textAutoNrPosti.getText());

		mt.setCilindrata(Integer.parseInt(textMotoreCilindrata.getText()));
		mt.setCavalli(Integer.parseInt(textMotoreCavalli.getText()));
		mt.setTipoAlimentazione(textMotoreTipoAlimentazione.getText());

		Motore mot = new Motore(mt.getCilindrata(), mt.getCavalli(), mt.getTipoAlimentazione());
		op.addCasaProduttrice(new CasaAutomobilistica(ca.getDenominazione(), ca.getNazione()));
		op.addAuto(new Auto(anno, prezzo, mot, colore, numPosti));
		op.faiInsert();
		op.stampaTutto();

	}

}
