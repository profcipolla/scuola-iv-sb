package it.armellini.concessionaria;

public class Motore {
	// Proprietà / Attributo
	private String tipoAlimentazione;
	private int cavalli, cilindrata;

	// Costruttore
	public Motore(int cilindrata, int cavalli, String tipoAlimentazione) {
		this.setCilindrata(cilindrata);
		this.setCavalli(cavalli);
		this.setTipoAlimentazione(tipoAlimentazione);
	}

	public Motore() {}

	// GETTER
	public int getCilindrata() {
		return this.cilindrata;

	}

	public int getCavalli() {
		return this.cavalli;
	}

	public String getTipoAlimentazione() {
		return tipoAlimentazione;
	}

	// SETTER
	public void setCilindrata(int cilindrata) {
		this.cilindrata = cilindrata;
	}

	public void setCavalli(int cavalli) {
		this.cavalli = cavalli;
	}

	public void setTipoAlimentazione(String tipoAlimentazione) {
		this.tipoAlimentazione = tipoAlimentazione;
	}

	public void stampaMotore() {
		System.out.println("Cilindrata: " + " " + cilindrata);
		System.out.println("Cavalli: " + cavalli);
	}
}