package it.armellini.concessionaria;

public class Auto extends Veicolo {
    // Proprietà / Attributo 
    private String colore;
    private int numeroPosti;
    
    private Motore dati;
    // Costruttore
    
    public Auto(Integer anno,float prezzo,Motore parteFisica,String colore,int numeroPosti){
        super(anno,prezzo,parteFisica);
        this.colore = colore;
        this.numeroPosti = numeroPosti;
    }
    
    //getter
    public String getColore() {
        return colore;
    }
    public int getNumeroPosti() {
        return numeroPosti;
    }    
    //setter
    public void setColore(String colore){
        this.colore = colore;
    }
    public void setNumeroPosti(int numeroPosti){
        this.numeroPosti = numeroPosti;
    }
}