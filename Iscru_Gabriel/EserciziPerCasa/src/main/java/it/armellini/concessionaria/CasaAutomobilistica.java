package it.armellini.concessionaria;

public class CasaAutomobilistica{
    // Proprietà / Attributo 
    private String denominazione, nazione;
    
    public CasaAutomobilistica() {}      
    // Costruttore 
    public CasaAutomobilistica(String denominazione,String nazione){
        this.denominazione = denominazione;
        this.nazione = nazione;
    }
    //GETTER
    public String getDenominazione() {
        return this.denominazione;
    }
    
    public String getNazione() {
        return this.nazione;
    }    
    //SETTER
    public void setDenominazione(String denominazione) {
        this.denominazione = denominazione;
    }
    
    public void setNazione(String nazione) {
        this.nazione = nazione;
    }
    
    public void stampaCasaAutomobilistica() {
        System.out.println("Denominazione: "+" " + denominazione);
    }
}