package it.armellini.concessionaria;

public class OperazioniConcessionaria {
	private OperazioniConcessionaria[] automobili = new OperazioniConcessionaria[5];
	private CasaAutomobilistica[] casaProd = new CasaAutomobilistica[5];
	private Auto[] auto = new Auto[5];
	private Motore[] motore = new Motore[5];
	static int i = 0;
	public OperazioniConcessionaria() {
	}

	public OperazioniConcessionaria(CasaAutomobilistica[] inCa, Auto[] inAut, Motore[] inMot) {
		this.casaProd = inCa;
		this.auto = inAut;
		this.motore = inMot;
	}
	

	public void addCasaProduttrice(CasaAutomobilistica cpInput) {
		//int i = 0;
		while (i < casaProd.length && casaProd[i] != null) {
			i++;
		}
		if (i < casaProd.length) {
			this.casaProd[i] = cpInput;
		}
	}

	public void addAuto(Auto autInput) {
		//int i = 0;
		while (i < auto.length && auto[i] != null) {
			i++;
		}
		if (i < auto.length) {
			this.auto[i] = autInput;
		}
	}

	public void faiInsert() {
		addTutto(new OperazioniConcessionaria(casaProd,auto,motore));
									
	}
	
	public void addTutto(OperazioniConcessionaria ins) {
		//int i = 0;
		while (i < automobili.length && automobili[i] != null) {
			i++;
		}
		if (i < automobili.length) {
			automobili[i] = ins;
		}

	}

	public void stampaTutto() {
		int i = 0;
		
		while (i < automobili.length && automobili[i] != null) {
			int ID = i+1;
			System.out.println(
					ID+"\n"+"Resoconto Automobili:\n\n" 
					+ "Casa Produttrice: "+automobili[i].casaProd[i].getDenominazione()+ "\n"
					+ "Nazione: "+automobili[i].casaProd[i].getNazione() + "\n"
					+ "Prezzo: " +automobili[i].auto[i].getPrezzo() + " €\n"
					+ "Numero Posti: " +automobili[i].auto[i].getNumeroPosti() + "\n"
					+ "Colore: "+automobili[i].auto[i].getColore() + "\n"
					+ "Cilindrata: "+automobili[i].auto[i].getParteFisica().getCilindrata() + "cv\n"
					+ "Cavalli: "+automobili[i].auto[i].getParteFisica().getCavalli() + "\n"
					+ "Tipo Alimentazione: "+automobili[i].auto[i].getParteFisica().getTipoAlimentazione() + "\n");
			i++;
		}
		if (i > automobili.length-1) {
			System.out.println("Attenzione spazio esaurito!!!");
		}

	}


}
