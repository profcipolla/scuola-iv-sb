package it.armellini.contoCorrente;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraContoCorrente extends JFrame implements ActionListener {

	ContoCorrenteOperazioni cc = new ContoCorrenteOperazioni();
	JPanel pTitolo = new JPanel();
	JLabel lTitolo = new JLabel("Operazioni Conto Corrente");

	JPanel pScelta = new JPanel();
	JButton bPreleva = new JButton("Preleva");
	JButton bDeposita = new JButton("Deposita");
	JButton bUltimiMovimenti = new JButton("UltimiMovimenti");

	JPanel pSituazione = new JPanel();
	JTextArea textDisplay = new JTextArea();
	JTextField textSaldo = new JTextField();

	JPanel pInserimento = new JPanel();
	JTextField textOperazioni = new JTextField("€ ");

	public FinestraContoCorrente() {
		setBackground(new Color(255, 255, 255));
		setTitle("Sportello Conto Corrente");
		setSize(668, 513);
		setLayout(new GridLayout(4, 1));
		setLocation(700, 700);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		add(pTitolo);
		pTitolo.setBackground(Color.DARK_GRAY);
		pTitolo.setLayout(new GridLayout(0, 1, 0, 0));
		lTitolo.setFont(new Font("Aston", Font.BOLD, 33));
		lTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		pTitolo.add(lTitolo);

		add(pSituazione);
		pSituazione.setLayout(new GridLayout(1, 2));
		textSaldo.setFont(new Font("Arial", Font.BOLD, 20));
		textSaldo.setHorizontalAlignment(SwingConstants.CENTER);
		pSituazione.add(textSaldo);
		textDisplay.setFont(new Font("Arial", Font.BOLD, 20));
		pSituazione.add(textDisplay);
		textSaldo.setBackground(Color.ORANGE);
		textDisplay.setBackground(Color.ORANGE);
		textSaldo.setEditable(false);
		textDisplay.setEditable(false);

		add(pScelta);
		pTitolo.setBackground(Color.LIGHT_GRAY);
		pScelta.setLayout(new GridLayout(0, 3, 0, 0));
		bDeposita.setBackground(new Color(255, 165, 0));
		bDeposita.setFont(new Font("Aston-Normal", Font.BOLD, 20));
		pScelta.add(bDeposita);
		bPreleva.setBackground(new Color(255, 165, 0));
		bPreleva.setFont(new Font("Aston-Normal", Font.BOLD, 20));
		pScelta.add(bPreleva);
		bUltimiMovimenti.setBackground(new Color(255, 165, 0));
		bUltimiMovimenti.setFont(new Font("Aston-Normal", Font.BOLD, 20));
		pScelta.add(bUltimiMovimenti);
		pInserimento.setBackground(new Color(128, 128, 128));

		add(pInserimento);
		pInserimento.setLayout(null);
		textOperazioni.setBounds(156, 11, 334, 92);
		textOperazioni.setHorizontalAlignment(SwingConstants.CENTER);
		textOperazioni.setFont(new Font("Arial", Font.BOLD, 20));
		pInserimento.add(textOperazioni);

		bDeposita.addActionListener(this);
		bPreleva.addActionListener(this);
		bUltimiMovimenti.addActionListener(this);
		
	}

	public void Operazioni() {}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		if (source == bDeposita) {
			if ("€ ".equals(textOperazioni.getText()) || textOperazioni.getText().contains("Inserire")) {
				textSaldo.setBackground(Color.yellow);
				textOperazioni.setBackground(Color.yellow);
				textSaldo.setText("Inserire l'importo da Depositare:\n");
				return;
			}
			String newStr = textOperazioni.getText().replace("€", "");
			double deposita = Double.parseDouble(newStr);
			cc.deposita(deposita);
			textSaldo.setBackground(Color.ORANGE);
			textDisplay.setText("Hai Depositato € " + deposita);
			textSaldo.setText(String.valueOf(cc.dammiSaldo()));
			textOperazioni.setBackground(Color.WHITE);
			textOperazioni.setText("€ ");

		} else if (source == bPreleva) {
			if ("€ ".equals(textOperazioni.getText()) || textOperazioni.getText().contains("Inserire")) {
				textSaldo.setBackground(Color.yellow);
				textOperazioni.setBackground(Color.yellow);
				textSaldo.setText("Inserire l'importo da Prelevare:\n");
				return;
			}
			String newStr = textOperazioni.getText().replace("€", "");
			double prelievo = Double.parseDouble(newStr);
			cc.preleva(prelievo);
			textSaldo.setBackground(Color.ORANGE);
			textDisplay.setText("Hai Prelevato € " + prelievo);
			textSaldo.setText(String.valueOf(cc.dammiSaldo()));
			textOperazioni.setBackground(Color.WHITE);
			textOperazioni.setText("€ ");
			
		} else if (source == bUltimiMovimenti) {
			textDisplay.setText(String.valueOf(cc.dammiMovimenti()));//richiama il metodo dammiMovimenti per i ultimi movimenti in ordine decrescente
			cc.salvaStringhe.delete(0, cc.salvaStringhe.length());//cancella il buffer del StringBuilder(System.out.print) ad ogni esecuzione del metodo dammiMovimenti così da non avere 
			
		}

	}
}