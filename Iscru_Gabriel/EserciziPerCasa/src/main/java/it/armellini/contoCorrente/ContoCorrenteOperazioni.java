package it.armellini.contoCorrente;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ContoCorrenteOperazioni {
	StringBuilder salvaStringhe = new StringBuilder();
	String residuo;
	private Movimento[] movimenti = new Movimento[10];
	private double saldo = 0.0d;
	
	public ContoCorrenteOperazioni() {}
	
	public boolean deposita(double inImporto) {
		int i = 0;
		while (i < movimenti.length - 1 && this.movimenti[i] != null) {
			i++;
		}
		if (i < movimenti.length) {
			movimenti[i] = new Movimento(inImporto, new Date());
			saldo = saldo + inImporto;
			
			return true;
		} else {
			return false;
		}
	}

	public double preleva(double sottrai) {
		if (deposita(-sottrai)) {
			
			return sottrai;
		}
		return 0.0d;
	}

	public String dammiSaldo() {
		
		int i = 0;
		while (i < movimenti.length - 1 && this.movimenti[i] != null) {
			i++;
		}
		if (i < movimenti.length - 1) {
			 residuo = "\nSaldo Attuale € " + saldo + "\n";
		}

		return residuo;
	}

	public StringBuilder dammiMovimenti() {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		
		int count = 0;
		for (int i = movimenti.length - 1; i > -1; i--) {

			if (movimenti[i] != null && count != 5) {
				count++;
				
				salvaStringhe.append("NR " + count +" : "+ movimenti[i].getImporto() + " - " + formato.format(movimenti[i].getData())+"\n");
				
			}
		}
		
		return salvaStringhe; 
	}

}
