package it.armellini.contoCorrente;

import java.util.Date;

public class Movimento {

	private double importo;
	private Date data;

	public Movimento(double importo, Date data) {
		this.setImporto(importo);
		this.setData(data);
	}

	public void setImporto(double importo) {
		this.importo = importo;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public double getImporto() {
		return this.importo;
	}

	public Date getData() {
		return this.data;
	}

}