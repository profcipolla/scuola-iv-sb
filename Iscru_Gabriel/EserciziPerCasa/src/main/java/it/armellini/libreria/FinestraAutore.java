package it.armellini.libreria;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraAutore extends JFrame implements ActionListener {
	// private static final long serialVersionUID = 1L;

	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();

	JPanel panelloMain = new JPanel();
	JPanel pannelloInserimentoDati = new JPanel();

	JLabel labelMain = new JLabel("AUTORI");
	JLabel labelNome = new JLabel("Nome");
	JLabel labelCognome = new JLabel("Cognome");
	JLabel labelNazionalita = new JLabel("Nazionalità");
	JTextField textNome = new JTextField();
	JTextField textCognome = new JTextField();
	JTextField textNazionalita = new JTextField();

	JButton bottoneSalva = new JButton("Salva");

	Font fontA = new Font("Solomon Normal", Font.BOLD, 16);

	public FinestraAutore() {
		setTitle("Gestione Autore");
		setSize(400, 400);
		setLocation(700, 700);
		getContentPane().setLayout(null);
		setBackground(Color.gray);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);

		panelloMain.setBackground(new Color(171, 205, 239));// colore riempimento
		panelloMain.setBounds(0, 0, 384, 70);// posizione
		panelloMain.setLayout(new GridLayout(1, 1));// griglia
		labelMain.setHorizontalAlignment(SwingConstants.CENTER);// posizionamento testo
		labelMain.setFont(new Font("Solomon", Font.BOLD, 30));// testo

		pannelloInserimentoDati.setBackground(Color.GRAY);
		pannelloInserimentoDati.setBounds(0, 70, 384, 220);
		pannelloInserimentoDati.setLayout(new GridLayout(3, 2));

		labelNome.setFont(fontA);// font grande
		labelNome.setHorizontalAlignment(SwingConstants.CENTER);// testo al centro
		labelNome.setForeground(Color.LIGHT_GRAY);// colore testo
		labelCognome.setFont(fontA);
		labelCognome.setHorizontalAlignment(SwingConstants.CENTER);
		labelCognome.setForeground(Color.LIGHT_GRAY);
		labelNazionalita.setFont(fontA);
		labelNazionalita.setHorizontalAlignment(SwingConstants.CENTER);
		labelNazionalita.setForeground(Color.LIGHT_GRAY);

		textNome.setHorizontalAlignment(SwingConstants.CENTER);
		textNome.setBackground(new Color(250, 235, 215));
		textNome.setFont(fontA);// font piccolo
		textCognome.setHorizontalAlignment(SwingConstants.CENTER);
		textCognome.setBackground(new Color(250, 235, 215));
		textCognome.setFont(fontA);
		textNazionalita.setHorizontalAlignment(SwingConstants.CENTER);
		textNazionalita.setBackground(new Color(250, 235, 215));
		textNazionalita.setFont(fontA);

		bottoneSalva.setFont(fontA);
		bottoneSalva.setBackground(new Color(233, 116, 81));
		bottoneSalva.setBounds(134, 308, 112, 42);

		add(panelloMain);
		panelloMain.add(labelMain);// aggiunto Scritta al pannello Titolo
		add(pannelloInserimentoDati);
		pannelloInserimentoDati.add(labelNome);// layer titolo libro
		pannelloInserimentoDati.add(textNome);// testo
		pannelloInserimentoDati.add(labelCognome);// layer pagine
		pannelloInserimentoDati.add(textCognome);// testo pagine
		pannelloInserimentoDati.add(labelNazionalita);// layer autore
		pannelloInserimentoDati.add(textNazionalita);// testo autore

		add(bottoneSalva);
		bottoneSalva.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		classeAutore.setNome(textNome.getText());
		classeAutore.setCognome(textCognome.getText());
		classeAutore.setNazionalita(textNazionalita.getText());

		textNome.setText("");
		textCognome.setText("");
		textNazionalita.setText("");

		operazioni.addAutori(new Autore(classeAutore.getNome(), classeAutore.getCognome(), classeAutore.getNazionalita()));
		operazioni.stampaAutori();
	}

}
