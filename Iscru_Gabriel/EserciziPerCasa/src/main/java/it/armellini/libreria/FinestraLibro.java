package it.armellini.libreria;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraLibro extends JFrame implements ActionListener {
	// private static final long serialVersionUID = 1L;
	Libro classeLibro = new Libro();
	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();

	JPanel panelloMain = new JPanel();
	JPanel pannelloInserimentoDati = new JPanel();

	JLabel labelMain = new JLabel("LIBRI");
	JLabel labelTitolo = new JLabel("Titolo Libro");
	JLabel labelNumeroPag = new JLabel("Pagine");

	JTextField textTitolo = new JTextField();
	JTextField textNumeroPag = new JTextField();
	JButton bottoneSalva = new JButton("Salva");

	Font fontA = new Font("Solomon Normal", Font.BOLD, 16);

	public FinestraLibro() {
		setTitle("Gestione Libro");
		setSize(400, 400);
		setLocation(700, 700);
		setLayout(null);
		setBackground(Color.gray);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);

		panelloMain.setBackground(new Color(171, 205, 239));// colore riempimento
		panelloMain.setBounds(0, 0, 384, 70);// posizione
		panelloMain.setLayout(new GridLayout(1, 1));// griglia
		labelMain.setHorizontalAlignment(SwingConstants.CENTER);// posizionamento testo
		labelMain.setFont(new Font("Solomon", Font.BOLD, 30));// testo

		pannelloInserimentoDati.setBackground(Color.GRAY);
		pannelloInserimentoDati.setBounds(0, 70, 384, 220);
		pannelloInserimentoDati.setLayout(new GridLayout(2, 2));

		labelTitolo.setFont(fontA);// font grande
		labelTitolo.setHorizontalAlignment(SwingConstants.CENTER);// testo al centro
		labelTitolo.setForeground(Color.LIGHT_GRAY);// colore testo
		labelNumeroPag.setFont(fontA);
		labelNumeroPag.setHorizontalAlignment(SwingConstants.CENTER);
		labelNumeroPag.setForeground(Color.LIGHT_GRAY);

		textTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		textTitolo.setBackground(new Color(250, 235, 215));
		textTitolo.setFont(fontA);// font piccolo
		textNumeroPag.setHorizontalAlignment(SwingConstants.CENTER);
		textNumeroPag.setBackground(new Color(250, 235, 215));
		textNumeroPag.setFont(fontA);

		bottoneSalva.setFont(fontA);
		bottoneSalva.setBackground(new Color(233, 116, 81));
		bottoneSalva.setBounds(134, 308, 112, 42);

		add(panelloMain);
		panelloMain.add(labelMain);// aggiunto Scritta al pannello Titolo
		add(pannelloInserimentoDati);
		pannelloInserimentoDati.add(labelTitolo);// layer titolo libro
		pannelloInserimentoDati.add(textTitolo);// testo
		pannelloInserimentoDati.add(labelNumeroPag);// layer pagine
		pannelloInserimentoDati.add(textNumeroPag);

		add(bottoneSalva);
		bottoneSalva.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		classeLibro.setTitolo(textTitolo.getText());
		classeLibro.setPagine(Integer.parseInt(textNumeroPag.getText()));

		textTitolo.setText("");
		textNumeroPag.setText("");

		operazioni.addLibro(new Libro(classeLibro.getTitolo(), classeLibro.getPagine()));
		operazioni.stampaLibri();
	}
}
