package it.armellini.utilita;

public class ResidenzaException extends Exception{
	public ResidenzaException(String msgErrore) {
		super(msgErrore);
	}
}
