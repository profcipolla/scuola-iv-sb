package it.armellini.utilita;

public class NomeException extends Exception {
	public NomeException(String msgErrore) {
		super(msgErrore);
	}
}