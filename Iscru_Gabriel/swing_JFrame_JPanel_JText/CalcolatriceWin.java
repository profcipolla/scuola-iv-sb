package it.armellini.ivsb.swing_JFrame_JPanel_JText;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalcolatriceWin {
	String[] scritte = { "7", "8", "9", "/", "4", "5", "6", "*", "1", "2", "3", "+", ",", "0", "=", "-" };
	JButton[] pulsanti = new JButton[16];

	public CalcolatriceWin() {
		JFrame calcWin = new JFrame();// Crea Finestra
		calcWin.setTitle("CALCOLATRICE");// Nome Finestra
		calcWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Ferma l'esecuzione chiudendo la finestra
		calcWin.setSize(400, 500);// Misura Finestra
		calcWin.setLayout(null);// Nessun Layout sulla Finestra
		calcWin.setLocation(700, 600);// Posizione della finestra sull desktop

		JTextField testo = new JTextField();// Crea casella Testo
		testo.setBounds(50, 45, 300, 50);// Posizione casella di testo sulla finestra

		JPanel mioPan = new JPanel();// Crea Pannello
		mioPan.setBounds(50, 100, 300, 300);// Posizione
		GridLayout gl = new GridLayout(4, 4, 5, 5);// Layout di tipo griglia
		mioPan.setLayout(gl);// Inserisco la griglia dentro il pannello

		for (int i = 0; i < scritte.length; i++) { // inserisco scritte sui bottoni
			pulsanti[i] = new JButton(scritte[i]);
			pulsanti[i].setFont(new Font("Verdana", Font.BOLD, 26));// imposto tipologia caratteri e dimensioni
			mioPan.add(pulsanti[i]);
		}

		calcWin.add(mioPan);// aggiungo il pannello alla finestra
		calcWin.add(testo);// aggiungo la casella di testo alla finestra
		//calcWin.pack();//?
		calcWin.setVisible(true);// imposto la visibilita di tutti i oggetti prima dichiarati
	}
}