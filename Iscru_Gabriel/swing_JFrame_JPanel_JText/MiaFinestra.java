package it.armellini.ivsb.swing_JFrame_JPanel_JText;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MiaFinestra {
	
	public MiaFinestra() {
		JFrame win = new JFrame();
		
		win.setTitle("Questo è il titolo della mia finestra");
		win.setSize(800,400);//misura della finestra
		win.setLocation(700, 600);//posizione della finestra
		Container c = win.getContentPane();
		c.setBackground(Color.gray); //impostare il colore della finestra
		
		//Gestisce la disposizione dei componenti all'interno della finestra
		//Fl è un gestore che accorda i componenti finche c'è spazio sulla riga,
		//poi va su una nuova riga
		LayoutManager mioLayout = new FlowLayout(); 
		GridLayout pLayuot = new GridLayout(8,2);//collone e righe
		win.setLayout(pLayuot);
		
		JLabel nome = new JLabel("Nome");
		JTextField insNome = new JTextField("Inserisci Nome");
		JLabel cognome = new JLabel("Cognome");
		JTextField insCognome = new JTextField("Inserisci Cognome");
		JLabel natoIl = new JLabel("Data di nascità");
		JTextField insNatoIl = new JTextField("Inserisci Data di nascità");
		JLabel cf = new JLabel("Codice fiscale");
		JTextField insCf = new JTextField("Codice fiscale");
		
		JButton invia = new JButton("Invia");
		JButton annulla = new JButton("Annulla");
		invia.setSize(2, 3);
		win.add(nome);
		win.add(insNome);
		win.add(cognome);
		win.add(insCognome);
		win.add(natoIl);
		win.add(insNatoIl);
		win.add(cf);
		win.add(insCf);
		
		win.add(invia);
		win.add(annulla);
/*		
		JButton b1 = new JButton("Bottone 1");
		JButton b2 = new JButton("Bottone 2");
		JButton b3 = new JButton("Bottone 3");
		JButton b4 = new JButton("Bottone 4");

		win.add(l1);
		win.add(t1);
		win.add(b1);
		win.add(b2);
		win.add(b3);
		win.add(b4);
*/		
		//win.pack();
		//win.size(800,600) per versione 8 java
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//se chiudo la finestra termina l'applicazione
		win.setVisible(true);//per visualizzare la finestra
	}

}
