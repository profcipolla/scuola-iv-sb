package it.armellini.ivsb.EsercizioException;

public class Persona {
	// nome diverso da vuoto
	private String nome;
	// cognome diverso da vuoto
	private String cognome;
	// eta > 0 e < 120
	private Integer eta;
	private static int MIN = 1;
	private static int MAX = 120;

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public Integer getEta() {
		return eta;
	}

	public void setNome(String nome) throws Exception {
		if (null == nome || "".equals(nome.trim())) {
			throw new NomeException("Il nome non è valido");
		}
		this.nome = nome.trim();
	}

	public void setCognome(String cognome) throws Exception {
		if (null == cognome || "".equals(cognome.trim())) {
			throw new CognomeException("Il cognome non e' valido");
		}
		this.cognome = cognome.trim();
	}

	public void setEta(Integer eta) throws Exception {
		if (eta > MAX || MIN > eta) {
			throw new EtaException("L'eta non è valida");
		}
		this.eta = eta;
	}

	public String Stampa() {
		return "Nome: " + nome + "\nCognome: " + cognome + "\nEtà: " + eta;
	}

}
