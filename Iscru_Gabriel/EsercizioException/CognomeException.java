package it.armellini.ivsb.EsercizioException;

public class CognomeException extends Exception {
	public CognomeException(String msgErrore) {
		super(msgErrore);
	}
}
