package it.armellini.ivsb.EsercizioException;

import it.armellini.ivsb.tastiera.MiaTastiera;

public class AvvioEsercizioExcpetion {

	public static void main(String[] args) throws Exception {
		Persona p = new Persona();
		MiaTastiera kb = new MiaTastiera();

		// lettura del nome , richiedere il nome dell'utente dinche ne valido(il setter
		// non ritorna uan exception))
		boolean isInputCorretto = false;
		do {
			System.out.println("Per favore inserisci un nome: ");
			String nome = kb.leggiTesto();
			try {
				p.setNome(nome);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while (!isInputCorretto);

		// lettura del cognome,finche non e valido ovvero il metodo setter non tirona un
		// valore valido
		isInputCorretto = false;
		do {
			System.out.println("Per favore inserisci un cognome: ");
			String cognome = kb.leggiTesto();
			try {
				p.setCognome(cognome);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while (!isInputCorretto);

		// lettura della eta costring l'utente ad inserire finche non inserisce un
// valore valido
		isInputCorretto = false;
		do {
			System.out.println("Per favore inserisci l'eta: ");
			Integer eta = kb.leggiIntero();
			try {
				p.setEta(eta);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while (!isInputCorretto);

		// scrivere il valori inseriti println del toString di persona
		System.out.println(p.Stampa());

	}

}
