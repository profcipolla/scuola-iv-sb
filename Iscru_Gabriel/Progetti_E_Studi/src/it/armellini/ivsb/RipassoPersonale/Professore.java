package it.armellini.ivsb.RipassoPersonale;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Professore extends Persona{
	
	SimpleDateFormat formato = new SimpleDateFormat("dd MM yyyy");
	
	
	public Professore(String nome, String cognome ,Date natoIl,String codiceFiscale,String residenza) throws Exception {
		super(nome, cognome,natoIl,codiceFiscale,residenza);
	}
	
	public String toString() {
		Locale.setDefault(Locale.ITALY);
		formato.setLenient(false);
		
			return "Nome " + this.nome
				 + "\nCognome: " + this.cognome
				 + "\nData di nascità: " + this.formato.format(natoIl)
				 + "\nC.F.: " + this.codiceFiscale
				 + "\nResidenza: " + this.residenza + "\n";
		}
}
