package it.armellini.ivsb.RipassoPersonale;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import classi_metodi_e_utilita.CFException;
import classi_metodi_e_utilita.CognomeException;
import classi_metodi_e_utilita.MiaTastiera;
import classi_metodi_e_utilita.NatoIlException;
import classi_metodi_e_utilita.NomeException;
import classi_metodi_e_utilita.ResidenzaException;

public class Exceptions_E_Metodi {
	MiaTastiera input = new MiaTastiera();// Input da tastiera
	Persona p = new Persona(); // classe padre controllo
	String nomeT, cognomeT, cfT, ResidenzaT;// var per l'input da tastiera
	Date nascitaT;
	static Studente[] STDang = new Studente[5];// array
	static Professore[] PRFang = new Professore[5];
	public static int iS = 0;// contatore
	public static int iP = 0;
	
	public void insSTDang() throws Exception {  //inserimento studente su array
		iS = 0;
		while (iS < STDang.length && STDang[iS] != null) {
			iS++;
		}
		verificaNome();
		verificaCognome();
		verificaNascita();
		verificaCF();
		verificaResidenza();
		STDang[iS] = new Studente(nomeT, cognomeT, nascitaT, cfT, ResidenzaT);
	}
	
	public void listaStudenti() {
		System.out.println("Lista Studenti:");
		iS = 0;
		while (iS < STDang.length && STDang[iS] != null) {
			System.out.println(STDang[iS].toString());
			iS++;
		} if (STDang[0] == null) {
			System.out.println("\nNon sono stati trovati Studenti registrati\n");
		}
		
	}
	
	public void insPRFang() throws Exception {	//inserimento professore su array
		iP = 0;
		while (iP < PRFang.length && PRFang[iP] != null) {
			iP++;
		}
		verificaNome();
		verificaCognome();
		verificaNascita();
		verificaCF();
		verificaResidenza();

		PRFang[iP++] = new Professore(nomeT, cognomeT, nascitaT, cfT, ResidenzaT);
	}
	
	public void listaProfessori() {
		System.out.println("Lista Professori:");
		iP = 0;
		while (iS < PRFang.length && PRFang[iP] != null) {
			System.out.println(PRFang[iP].toString());
			iP++;
		} if (PRFang[0] == null) {
			System.out.println("\nNon sono stati trovati Professori registrati\n");
		}
	}
	
	//Verifiche tramite exception sul costruttore genitore	
	public void verificaNome() throws NomeException {
		System.out.println("Inserire Nome: ");
		boolean risolto = false;
		do {
			try {
				nomeT = input.leggiTesto();
				p.setNome(nomeT);
				risolto = true;
			} catch (NomeException e) {
				System.out.println(e.getMessage());
			}
		} while (!risolto);
	}

	public void verificaCognome() throws CognomeException {
		System.out.println("Inserire Cognome: ");
		boolean risolto = false;
		do {
			try {
				cognomeT = input.leggiTesto();
				p.setCognome(cognomeT);
				risolto = true;
			} catch (CognomeException e) {
				System.out.println(e.getMessage());
			}
		} while (!risolto);
	}

	public void verificaNascita() throws NatoIlException, ParseException {
		System.out.println("Inserire Data di Nascità: ");
		boolean risolto = false;
		do {
			try {
				String temp = input.leggiTesto();
				p.setNascitaTastiera(temp);
				boolean step2 = false;
				do {
					try {
						Locale.setDefault(Locale.ITALY);
						SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
						formato.setLenient(false);
						nascitaT = formato.parse(Persona.appoggio);
						p.setNatoIl(nascitaT);
						step2 = true;
					} catch (ParseException e) {
						System.out.println("Inserire il formato 'gg-mm-anno': ");
						Persona.appoggio = input.leggiTesto();
					}
				} while (!step2);
				risolto = true;
			} catch (NatoIlException e) {
				System.out.println(e.getMessage());
			}
		} while (!risolto);
	}

	public void verificaCF() throws CFException {
		System.out.println("Inserire il Codice Fiscale: ");
		boolean risolto = false;
		do {
			try {
				cfT = input.leggiTesto();
				p.setCodiceFiscale(cfT);
				risolto = true;
			} catch (CFException e) {
				System.out.println(e.getMessage());
			}
		} while (!risolto);
	}

	public void verificaResidenza() throws ResidenzaException {
		System.out.println("Inserire la Residenza o attuale Domicilio: ");
		boolean risolto = false;
		do {
			try {
				ResidenzaT = input.leggiTesto();
				p.setResidenza(ResidenzaT);
				risolto = true;
			} catch (ResidenzaException e) {
				System.out.println(e.getMessage());
			}
		} while (!risolto);
	}
}
