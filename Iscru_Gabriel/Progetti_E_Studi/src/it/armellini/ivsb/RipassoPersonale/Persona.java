package it.armellini.ivsb.RipassoPersonale;

import java.util.Date;

import classi_metodi_e_utilita.CFException;
import classi_metodi_e_utilita.CognomeException;
import classi_metodi_e_utilita.NatoIlException;
import classi_metodi_e_utilita.NomeException;
import classi_metodi_e_utilita.ResidenzaException;

public class Persona {
	protected String nome;
	protected String cognome;
	protected Date natoIl;
	protected String codiceFiscale;
	protected String residenza;
	public static String appoggio;

	public Persona(String nome, String cognome, Date natoIl, String codiceFiscale, String residenza) throws Exception {
		this.setNome(nome);
		this.setCognome(cognome);
		this.setNatoIl(natoIl);
		this.setCodiceFiscale(codiceFiscale);
		this.setResidenza(residenza);
	}

	public Persona() {
	}

	public void setNome(String nome) throws NomeException {
		if (nome == null || "".equals(nome.trim())) {
			throw new NomeException("Nome non inserito, si prega di inserirlo: ");
		}
		this.nome = nome.trim();
	}

	public void setCognome(String cognome) throws CognomeException {
		if (cognome == null || "".equals(cognome.trim())) {
			throw new CognomeException("Cognome non inserito, si prega di inserirlo: ");
		}
		this.cognome = cognome.trim();
	}

	public void setNatoIl(Date natoIl) {
		this.natoIl = natoIl;
	}

	public void setNascitaTastiera(String temp) throws NatoIlException {
		if (null == temp || "".equals(temp.trim())) {
			throw new NatoIlException("Data di nascita non inserita, si prega di inserirla: ");
		}
		appoggio = temp;
	}

	public void setCodiceFiscale(String codiceFiscale) throws CFException {
		if (null == codiceFiscale || "".equals(codiceFiscale.trim())) {
			throw new CFException("Codice Fiscale non inserito: ");
		}
		if (16 != codiceFiscale.length()) {
			throw new CFException("Codice Fiscale non completo: ");
		}
		this.codiceFiscale = codiceFiscale.trim();
	}

	public void setResidenza(String residenza) throws ResidenzaException {
		if (residenza == null || "".equals(residenza.trim())) {
			throw new ResidenzaException("La residenza non è valida, si prega di inserirlo: ");
		}
		this.residenza = residenza.trim();
	}

}
