package it.armellini.ivsb.RipassoPersonale;

import classi_metodi_e_utilita.MiaTastiera;

public class Avvio_Ripasso {
	public static void main(String[] args) throws Exception {
		MiaTastiera input = new MiaTastiera();
		Exceptions_E_Metodi metodi = new Exceptions_E_Metodi();

		String sceltaMenu;
		String sceltaSottoMenu;

		do {
			menuPrincipale();
			sceltaMenu = input.leggiTesto();
			if ("1".equals(sceltaMenu)) {

				do {
					menuANG();
					sceltaSottoMenu = input.leggiTesto();
					if ("1".equals(sceltaSottoMenu)) {
						metodi.insSTDang();
					} else if ("2".equals(sceltaSottoMenu)) {
						metodi.listaStudenti();
					} else if ("3".equals(sceltaSottoMenu)) {
						metodi.insPRFang();
					} else if ("4".equals(sceltaSottoMenu)) {
						metodi.listaProfessori();
					}
				} while (!"x".equals(sceltaSottoMenu));
			}	
		} while (!"x".equals(sceltaMenu));
	}

	public static void menuPrincipale() {
		System.out.println("==> 1 Menu Angrafica");
		System.out.println("=> x Uscire");
	}

	public static void menuANG() {
		System.out.println("==> 1 Inserire un Studente");
		System.out.println("====> 2 Visulizzare i Studenti");
		System.out.println("==> 3 Inserire un Professore");
		System.out.println("====> 4 Visulizzare i Professori");
		System.out.println("=> x Uscire");
	}
}
