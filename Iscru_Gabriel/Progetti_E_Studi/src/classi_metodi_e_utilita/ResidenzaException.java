package classi_metodi_e_utilita;

public class ResidenzaException extends Exception{
	public ResidenzaException(String msgErrore) {
		super(msgErrore);
	}
}
