package tictactoe;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Console extends JFrame implements ActionListener {
	public static final int DIM_R = 3;
	public static final int DIM_C = 3;

	private boolean isSimboloX = true;
	private JButton[][] scacchiera = new JButton[Console.DIM_R][Console.DIM_C];

	private int[] modelScacchiera = new int[Console.DIM_R * Console.DIM_C];

	public Console() {
		setTitle("Gioco del Tris");
		setSize(600, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(Console.DIM_R, Console.DIM_C));

		this.initScacchiera();
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton tmpJButton = (JButton) e.getSource();
		int[] coord = this.getCoordinate(tmpJButton);

		// trova il simbolo che sta giocando
		String strSimbolo = this.isSimboloX ? "X" : "O";
		tmpJButton.setText(strSimbolo);
		System.out.println(String.format("Hai premuto il BOTTONE R: %d, C: %d - Ho scritto il simbolo %s", coord[0],
				coord[1], strSimbolo));

		if (this.vincita(strSimbolo)) {
			JOptionPane.showMessageDialog(this, String.format("Ha vinto il simbolo %s", strSimbolo), "AVVISO",
					JOptionPane.INFORMATION_MESSAGE);
		}

		isSimboloX = !isSimboloX;
		tmpJButton.removeActionListener(this);

	}

	private int[] mossaComputer(String simboloComputer) {
		return null;
		// controlla se può vincere
		// copio la scacchiera attuale
		// provo una alla volta le mosse sulla scacchiera copia e verifico se quella
		// situazione mi fa vincere (richiamando vincita)
		// se vincita restituisce true quella è la mossa che il computer deve fare

		// controlla se può vincere l'avversario
		// copio la scacchiera (per evitare di alterare quella del gioco)
		// provo una alla volta tutte le possibili mosse con il simbolo dell'avversario
		// e per ciascuna verifico la possibilità di vincita
		// tramite una chiamata a vincita (che deve essere in grado di lavorare sulla
		// copia della scacchiera)
		// se trovo una mossa che permette all'avversario di vincere quella è la mossa
		// del computer

		// altrimenti esco con una mossa random

	}

	private int[] getCoordinate(JButton tmpButton) {
		for (int r = 0; r < Console.DIM_R; r++) {
			for (int c = 0; c < Console.DIM_C; c++) {
				if (tmpButton == scacchiera[r][c]) {
					return new int[] { r, c };
				}
			}
		}
		return null;
	}

	private void initScacchiera() {
		for (int r = 0; r < Console.DIM_R; r++) {
			for (int c = 0; c < Console.DIM_C; c++) {
				JButton tmpBottone = new JButton();
				tmpBottone.addActionListener(this);
				add(tmpBottone);
				scacchiera[r][c] = tmpBottone;
			}
		}
	}

	// su quale scacchiera?
	private boolean vincita(String simbolo) {
		boolean haVinto = true;
		// riga
		for (int r = 0; r < DIM_R; r++) {
			for (int c = 0; c < DIM_C; c++) {
				haVinto &= simbolo == scacchiera[r][c].getText();
			}
			if (haVinto) {
				return true;
			}
		}

		// colonna
		for (int c = 0; c < DIM_C; c++) {
			haVinto = true;
			for (int r = 0; r < DIM_R; r++) {
				haVinto &= simbolo == scacchiera[r][c].getText();
			}
			if (haVinto) {
				return true;
			}
		}

		// diagonale
		if (DIM_R == DIM_C) {
			haVinto = true;
			for (int i = 0; i < DIM_C; i++) {

				haVinto &= simbolo == scacchiera[i][i].getText();

			}
			if (haVinto) {
				return true;
			}

			haVinto = true;
			for (int r = 0; r < DIM_R; r++) {
				for (int c = 0; c < DIM_C; c++) {
					if (r + c == 2) {
					haVinto &= simbolo == scacchiera[r][c].getText();
					}
				}
			}
			if (haVinto) {
				return true;
			}
		}

		return false;
	}

	public static void main(String[] a) {
		new Console();
	}

}
