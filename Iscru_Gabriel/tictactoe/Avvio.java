package tictactoe;

public class Avvio {

	public static void main(String[] args) {
		// new Console();
		// System.out.println(fattorialeR(5));
		//System.out.println(fattorialeStringa("pippo"));
		
		//System.out.println(revesciaI("pippo"));
		
		//System.out.println(revesciaR("pippo"));
		System.out.println(fibonacciR(0));
		System.out.println(fibonacciR(1));
		
		System.out.println(fibonacciR(3));
		
		System.out.println(fibonacciR(11));
		
	}
	// il fattoriale di un numero n è il prodotto di n per n-1 per n-2 per .... per
	// 2 * 1,
	// per esempio voglio il fatrtoriale di 5 devo fare 5*4*3*2*1
	// 4 4*3*2*1
	// 5| = 5 * 4
	// 5 fattoriale si indica con !5, per convernzione 0" = 1

	// soluzione ricorsiva
	
	// 0! = 1
	// N! = N * (N -1)!
	// soluzione ricorsiva
	
	public static long fattorialeR(long n) {
		if (0 == n) {
			return 1;
		}
		return n * fattorialeR(n - 1);
	}

	// soluzione iterativa
	public static long fattorialeI(long n) {
		long prodotto = 1;
		for (long i = 1; i <= n; i++) {
			prodotto *= n;
		}
		return prodotto;
	}

	// scrivere usando la ricorsione, in una stringa al rovescio, dal'ultimo al primo carattere

	public static String fattorialeStringa(String input) {
		if (input.length() == 1) {
			return input;
		}

		return fattorialeStringa(input.substring(1)) + input.charAt(0);
	}
	
	// scrivo una stringa al contrario iterativa
	public static String revesciaI(String input) {
		if (null == input) {
			return null;
		}
		StringBuilder risposta = new StringBuilder();
		for (int i = input.length() - 1; i >= 0; i--) {
			risposta.append(input.charAt(i));
			
		}
		return risposta.toString();
	}
	
	private static String rovesciaRPos(String input, int numCar) {
		System.out.println("Hai chiamato rovesciaRPos con numCar " + numCar);
		if (numCar == input.length() - 1) {
			System.out.println("Stai per uscire rovesciaRPos con numCar " + numCar + "risposta " + input.charAt(numCar));
			return ""+input.charAt(numCar);
		}
		String temp = rovesciaRPos(input,numCar + 1) + input.charAt(numCar);
		System.out.println("Stai per uscire rovesciaRPos con numCar " + numCar + " risposta " + temp);
		return rovesciaRPos(input,numCar + 1) + input.charAt(numCar);
	}
	
	
	public static String revesciaR(String input) {
		if (null == input) {
			return null;
		}
		return rovesciaRPos(input, 0);
	}
	
	
	// finbonacci(n) = finbonacci(n-1) + finbonacci(n-2)
	public static long fibonacciR(int n) {
		
		if (0 == n ) {
			return 0;
		}
		if (1 == n) {
			return 1;
		}
		return fibonacciR(n-1) + fibonacciR(n-2);
	}
	// calcolo dell'ennesimo numero di Fibonacci
	// 1 1 2 3 5 8 13 21
	
	//Ragionate sul gioco delle Torri di Hanoi
}
