package it.armellini.libreria;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraLibro extends JFrame implements ActionListener {
	
	Libro classeLibro = new Libro();
	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();

	JPanel titoloPannello = new JPanel();
	JPanel inserimentoDatiPannello = new JPanel();

	JLabel etichettaTitolo = new JLabel("Inserisci dati Libro");
	JLabel titoloLibroEtichetta = new JLabel("Nome Libro:");
	JLabel numeroPagineEtichetta = new JLabel("Pagine:");

	JTextField titoloLibroInput = new JTextField();
	JTextField numeroPagineInput = new JTextField();
	JButton salvabottone = new JButton("Salva");


	public FinestraLibro() {
		setTitle("LIBRO");
		setSize(400, 400);
		setLocation(700, 700);
		getContentPane().setLayout(null);
		setBackground(new Color(139, 69, 19));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);

		titoloPannello.setBackground(new Color(245, 222, 179));
		titoloPannello.setBounds(0, 0, 384, 70);
		titoloPannello.setLayout(new GridLayout(1, 1));
		etichettaTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		etichettaTitolo.setFont(new Font("Garamond", Font.BOLD, 28));

		inserimentoDatiPannello.setBackground(new Color(0, 0, 139));
		inserimentoDatiPannello.setBounds(0, 70, 384, 220);
		titoloLibroEtichetta.setBounds(0, 0, 121, 110);

		titoloLibroEtichetta.setFont(new Font("Garamond", Font.BOLD, 16));
		titoloLibroEtichetta.setHorizontalAlignment(SwingConstants.CENTER);
		titoloLibroEtichetta.setForeground(Color.LIGHT_GRAY);
		numeroPagineEtichetta.setBounds(0, 110, 121, 110);
		numeroPagineEtichetta.setFont(new Font("Garamond", Font.BOLD, 16));
		numeroPagineEtichetta.setHorizontalAlignment(SwingConstants.CENTER);
		numeroPagineEtichetta.setForeground(Color.LIGHT_GRAY);
		titoloLibroInput.setBounds(131, 0, 253, 110);

		titoloLibroInput.setHorizontalAlignment(SwingConstants.CENTER);
		titoloLibroInput.setBackground(new Color(224, 255, 255));
		titoloLibroInput.setFont(new Font("Garamond", Font.BOLD, 16));
		numeroPagineInput.setBounds(131, 110, 253, 110);
		numeroPagineInput.setHorizontalAlignment(SwingConstants.CENTER);
		numeroPagineInput.setBackground(new Color(224, 255, 255));
		numeroPagineInput.setFont(new Font("Garamond", Font.BOLD, 16));

		salvabottone.setFont(new Font("Garamond", Font.BOLD, 16));
		salvabottone.setBackground(new Color(233, 116, 81));
		salvabottone.setBounds(91, 301, 221, 60);

		getContentPane().add(titoloPannello);
		titoloPannello.add(etichettaTitolo);
		getContentPane().add(inserimentoDatiPannello);
		inserimentoDatiPannello.setLayout(null);
		inserimentoDatiPannello.add(titoloLibroEtichetta);
		inserimentoDatiPannello.add(titoloLibroInput);
		inserimentoDatiPannello.add(numeroPagineEtichetta);
		inserimentoDatiPannello.add(numeroPagineInput);

		getContentPane().add(salvabottone);
		salvabottone.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		classeLibro.setTitolo(titoloLibroInput.getText());
		classeLibro.setPagine(Integer.parseInt(numeroPagineInput.getText()));

		titoloLibroInput.setText("");
		numeroPagineInput.setText("");

		operazioni.addLibro(new Libro(classeLibro.getTitolo(), classeLibro.getPagine()));
		operazioni.stampaLibri();
	}
}
