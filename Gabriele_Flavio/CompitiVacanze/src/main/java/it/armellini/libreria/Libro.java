package it.armellini.libreria;

public class Libro {
	public String titolo;
	public int pagine;
	
	public Libro() {}

	public Libro(String titolo, int pagine) {// era da aggiungere esempio: this.setNome(nome)
		this.setTitolo(titolo);
		this.setPagine(pagine);
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public int getPagine() {
		return pagine;
	}

	public void setPagine(int pagine) {
		this.pagine = pagine;
	}

	@Override
	public String toString() {
		return "Titolo: " + this.titolo + "\nPagine: " + this.pagine;
	}

}
