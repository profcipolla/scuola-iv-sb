package it.armellini.libreria;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraAutore extends JFrame implements ActionListener {
	

	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();

	JPanel titoloPanello = new JPanel();
	JPanel datiPanello = new JPanel();

	JLabel titoloEtichetta = new JLabel("Inserisci dati Autore");
	JLabel nomeEtichetta = new JLabel("Nome:");
	JLabel cognomeEtichetta = new JLabel("Cognome:");
	JLabel nazionalitaEtichetta = new JLabel("Nazionalità:");
	JTextField NomeInput = new JTextField();
	JTextField cognomeInput = new JTextField();
	JTextField nazionalitaInput = new JTextField();

	JButton salvaBottone = new JButton("Salva");

	public FinestraAutore() {
		getContentPane().setBackground(new Color(105, 105, 105));
		setTitle("AUTORE");
		setSize(400, 400);
		setLocation(700, 700);
		getContentPane().setLayout(null);
		setBackground(new Color(139, 69, 19));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);

		titoloPanello.setBackground(new Color(245, 222, 179));
		titoloPanello.setBounds(0, 0, 384, 70);
		titoloPanello.setLayout(new GridLayout(1, 1));
		titoloEtichetta.setHorizontalAlignment(SwingConstants.CENTER);
		titoloEtichetta.setFont(new Font("Garamond", Font.BOLD, 28));

		datiPanello.setBackground(new Color(0, 0, 139));
		datiPanello.setBounds(0, 70, 384, 220);
		
		nomeEtichetta.setBounds(0, 0, 121, 73);
		nomeEtichetta.setFont(new Font("Garamond", Font.BOLD, 16));
		nomeEtichetta.setHorizontalAlignment(SwingConstants.CENTER);
		nomeEtichetta.setForeground(Color.LIGHT_GRAY);
		cognomeEtichetta.setBounds(0, 73, 121, 73);
		cognomeEtichetta.setFont(new Font("Garamond", Font.BOLD, 16));
		cognomeEtichetta.setHorizontalAlignment(SwingConstants.CENTER);
		cognomeEtichetta.setForeground(Color.LIGHT_GRAY);
		nazionalitaEtichetta.setBounds(0, 146, 121, 73);
		nazionalitaEtichetta.setFont(new Font("Garamond", Font.BOLD, 16));
		nazionalitaEtichetta.setHorizontalAlignment(SwingConstants.CENTER);
		nazionalitaEtichetta.setForeground(Color.LIGHT_GRAY);
		NomeInput.setBounds(131, 0, 253, 73);

		NomeInput.setHorizontalAlignment(SwingConstants.CENTER);
		NomeInput.setBackground(new Color(240, 255, 255));
		NomeInput.setFont(new Font("Garamond", Font.BOLD, 16));
		cognomeInput.setBounds(131, 73, 253, 73);
		cognomeInput.setHorizontalAlignment(SwingConstants.CENTER);
		cognomeInput.setBackground(new Color(240, 255, 255));
		cognomeInput.setFont(new Font("Garamond", Font.BOLD, 16));
		nazionalitaInput.setBounds(131, 146, 253, 73);
		nazionalitaInput.setHorizontalAlignment(SwingConstants.CENTER);
		nazionalitaInput.setBackground(new Color(240, 255, 255));
		nazionalitaInput.setFont(new Font("Garamond", Font.BOLD, 16));

		salvaBottone.setFont(new Font("Garamond", Font.BOLD, 24));
		salvaBottone.setBackground(new Color(0, 139, 139));
		salvaBottone.setBounds(98, 301, 194, 60);

		getContentPane().add(titoloPanello);
		titoloPanello.add(titoloEtichetta);
		getContentPane().add(datiPanello);
		datiPanello.setLayout(null);
		datiPanello.add(nomeEtichetta);
		datiPanello.add(NomeInput);
		datiPanello.add(cognomeEtichetta);
		datiPanello.add(cognomeInput);
		datiPanello.add(nazionalitaEtichetta);
		datiPanello.add(nazionalitaInput);

		getContentPane().add(salvaBottone);
		salvaBottone.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		classeAutore.setNome(NomeInput.getText());
		classeAutore.setCognome(cognomeInput.getText());
		classeAutore.setNazionalita(nazionalitaInput.getText());

		NomeInput.setText("");
		cognomeInput.setText("");
		nazionalitaInput.setText("");

		operazioni.addAutori(new Autore(classeAutore.getNome(), classeAutore.getCognome(), classeAutore.getNazionalita()));
		operazioni.stampaAutori();
	}

}
