package it.armellini.ivsb.EsercizioException;

public class Persona {
	private String nome;
	private String cognome;
	private Integer eta;
	
	private static int EtaMinima = 1;
	private static int EtaMassima = 120;

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public Integer getEta() {
		return eta;
	}

	public void setNome(String nome) throws Exception {
		if (null == nome || "".equals(nome.trim())) {
			throw new NomeException("Il nome non riconoscuto");
		}
		this.nome = nome.trim();
	}

	public void setCognome(String cognome) throws Exception {
		if (null == cognome || "".equals(cognome.trim())) {
			throw new CognomeException("Il cognome non riconosciuto");
		}
		this.cognome = cognome.trim();
	}

	public void setEta(Integer eta) throws Exception {
		if (eta > EtaMassima || EtaMinima > eta) {
			throw new EtaException("L'eta non riconosciuta");
		}
		this.eta = eta;
	}

	public String Stampa() {
		return "Nome: " + nome + "\nCognome: " + cognome + "\nEta : " + eta;
	}

}
