package it.armellini.ivsb.EsercizioException;

import it.armellini.ivsb.tastiera.MiaTastiera;

public class AvvioEsercizioExcpetion {

	public static void main(String[] args) throws Exception {
		Persona p = new Persona();
		MiaTastiera kb = new MiaTastiera();
    
		boolean isInputCorretto = false;
		do {
			System.out.println("Per favore inserisci un nome: ");
			String nome = kb.leggiTesto();
			try {
				p.setNome(nome);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while (!isInputCorretto);

		isInputCorretto = false;
		do {
			System.out.println("Per favore inserisci un cognome: ");
			String cognome = kb.leggiTesto();
			try {
				p.setCognome(cognome);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while (!isInputCorretto);

		isInputCorretto = false;
		do {
			System.out.println("Per favore inserisci l'eta: ");
			Integer eta = kb.leggiIntero();
			try {
				p.setEta(eta);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while (!isInputCorretto);

		System.out.println(p.Stampa());

	}

}
