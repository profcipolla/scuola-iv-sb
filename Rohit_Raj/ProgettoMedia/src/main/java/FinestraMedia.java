import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FinestraMedia extends JFrame implements ActionListener {

	private JTextField numero1 = new JTextField("0");
	private JTextField numero2 = new JTextField("0");
	private JTextField txtMedia = new JTextField();
	
	private JButton bMediaAritmetica = new JButton("Media Aritmentica");
	private JButton bMediaGiometrica = new JButton("Media Giometrica");
	
	public FinestraMedia () {
		setSize(400,440);
		setTitle("Media");
		setLayout(new GridLayout(3,2));
		
		add(numero1);
		add(numero2);
		
		add(bMediaAritmetica);
		add(bMediaGiometrica);
		
		bMediaAritmetica.addActionListener(this);
		bMediaGiometrica.addActionListener(this);
		
		add(txtMedia);
		
		
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		if (s == bMediaAritmetica) {
			Double num1 = Double.parseDouble(numero1.getText());
			Double num2 = Double.parseDouble(numero2.getText());
			Double risultatoA = mediaA(num1,num2);
			String risultatoT = String.valueOf(risultatoA);
			txtMedia.setText(risultatoT);
		} else {
			Double num1 = Double.parseDouble(numero1.getText());
			Double num2 = Double.parseDouble(numero2.getText());
			Double risultatoG = mediaG(num1,num2);
			String risultatoT = String.valueOf(risultatoG);
			txtMedia.setText(risultatoT);
		}
		
	}
	
	public Double mediaA(Double num1,Double num2) {
		Double risultato = (num1 + num2)/2;
		return risultato;
	}
	
	public Double mediaG(Double num1,Double num2) {
		Double risultato = Math.sqrt(num1 * num2);
		return risultato;
	}
	
	public static void main(String[] args) {
		
		new FinestraMedia();
	}

}
	
