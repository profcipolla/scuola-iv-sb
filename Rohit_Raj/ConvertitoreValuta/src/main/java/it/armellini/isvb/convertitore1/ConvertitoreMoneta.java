package it.armellini.isvb.convertitore1;

public class ConvertitoreMoneta {
	private Double euro;
	private Double valutaConvertita;
	private Double tassoCoversione = 1.2D;
	
	public Double conversione(Double euro) {
		valutaConvertita = euro * tassoCoversione;
		
		return valutaConvertita;
		
	}

	public Double getEuro() {
		return euro;
	}

	public void setEuro(Double euro) {
		this.euro = euro;
	}

	public Double getValutaConvertita() {
		return valutaConvertita;
	}

	public void setValutaConvertita(Double valutaConvertita) {
		this.valutaConvertita = valutaConvertita;
	}

	public Double getTassoCoversione() {
		return tassoCoversione;
	}

	public void setTassoCoversione(Double tassoCoversione) {
		this.tassoCoversione = tassoCoversione;
	}
	
	
}
