package it.armellini.isvb.convertitore1;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FinestraConvertitore extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	ConvertitoreMoneta convert = new ConvertitoreMoneta();
	
	private JTextField testoEuro = new JTextField();
	private JTextField testoDollaro = new JTextField();
	private JTextField testoTasso = new JTextField();
	 
	private JLabel etichettaEuro = new JLabel("Euro");
	private JLabel etichettaDollaro = new JLabel("Dollaro");
	private JButton bottoneConverti = new JButton("Esegui");
	public FinestraConvertitore() {
		setTitle("Covertitore");
		setSize(400, 400);
		
		setLayout(new GridLayout(3,2));
		
		add(etichettaEuro);
		add(testoEuro);
		add(etichettaDollaro);
		add(testoDollaro);
		add(bottoneConverti);
		add(testoTasso);
		testoTasso.setText(String.valueOf(convert.getTassoCoversione()));
		bottoneConverti.addActionListener(this);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);	
	}
	
	public void actionPerformed(ActionEvent e) {
		
		Double euroT = Double.parseDouble(testoEuro.getText());
		convert.setTassoCoversione(Double.valueOf(testoTasso.getText()));
		testoDollaro.setText(String.valueOf(convert.conversione(euroT)));
	}
	
	
	public static void main(String[] args) {
		new FinestraConvertitore();
	}

}
