import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MiaClasse extends JFrame implements ActionListener  {
	ClasseConverzione convert = new ClasseConverzione();
	
	private JTextField Euro = new JTextField();
	private JTextField Dollaro = new JTextField();
	private JTextField Tasso = new JTextField();
	 
	private JLabel etichettaEuro = new JLabel("Euro");
	private JLabel etichettaDollaro = new JLabel("Dollaro");
	private JButton bottoneConverti = new JButton("Esegui");
	public MiaClasse() {
		setTitle("Covertitore");
		setSize(400, 400);
		
		setLayout(new GridLayout(3,2));
		
		add(etichettaEuro);
		add(Euro);
		add(etichettaDollaro);
		add(Dollaro);
		add(bottoneConverti);
		add(Tasso);
		Tasso.setText(String.valueOf(convert.getTassoCoversione()));
		bottoneConverti.addActionListener(this);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);	
	}
	
	
	public void actionPerformed(ActionEvent e) {
		
		Double euroT = Double.parseDouble(Euro.getText());
		convert.setTassoCoversione(Double.valueOf(Tasso.getText()));
		Dollaro.setText(String.valueOf(convert.conversione(euroT)));
	}
}


