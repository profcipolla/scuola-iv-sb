import java.util.Scanner;

public class ClasseConverzione {
  private Double euro;
  private Double convertitore;
  private Double tassoconvertitore = 1.2D;
  
  public Double conversione(Double euro) {
	  convertitore = euro * tassoconvertitore;
    
    return convertitore;
    
  }

  public Double getEuro() {
    return euro;
  }

  public void setEuro(Double euro) {
    this.euro = euro;
  }

  public Double getValutaConvertita() {
    return convertitore;
  }

  public void setValutaConvertita(Double convertitore) {
    this.convertitore = convertitore;
  }

  public Double getTassoCoversione() {
    return tassoconvertitore;
  }

  public void setTassoCoversione(Double tassoconvertitore) {
    this.tassoconvertitore = tassoconvertitore;
  }
  
  
}