package Hanoi.scuola;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class HanoiWin extends JFrame  {
	HanoiModel hm = new HanoiModel();
	private JButton[] sinistraB = new JButton[hm.DISCHI];
	private JButton[] centroB = new JButton[hm.DISCHI];
	private JButton[] destraB = new JButton[hm.DISCHI];
	JPanel pnlPulsanti = new JPanel();
	
	
	public HanoiWin() {
		
		setTitle("Progetto Hanoi");
		setSize(400, 400);
		// Creo la barra del menu
	    JMenuBar barraMenu = new JMenuBar();
	    setJMenuBar(barraMenu);
	    
	    // Aggiungo menu ed elementi
	    JMenu file = new JMenu("File");
	    JMenuItem carica = new JMenuItem("Carica Partita");
	    JMenuItem exit = new JMenuItem("Exit");
	    
	    file.add(carica);
	    file.add(exit);
	    
	    barraMenu.add(file);
	   
		
	setTitle("Hanoi");
	setSize(400,400);
	setLayout(new GridLayout(1,1));
	
    
    //pnlPulsanti.setBounds(50, 100, 300, 300);
	add(pnlPulsanti);
    
    pnlPulsanti.setLayout(new GridLayout(hm.DISCHI, 3));
    for (int i = 0; i < sinistraB.length; i++) {
    	sinistraB[i] = new JButton();
    	centroB[i] = new JButton();
    	destraB[i] = new JButton();
      pnlPulsanti.add(sinistraB[i]);
      pnlPulsanti.add(centroB[i]);
      pnlPulsanti.add(destraB[i]);
      
    }
    
    // Fine menu
	setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setVisible(true);
    
	}

	// nel costruttore fa un grid layout di DISCHI righe e 3 colonne
	// in ogni cella mette un jbutton
	
	public void disegna(int[] sinistra, int[] centro, int[] destra) {
		// riscrive le caption nei jbutton per simulare la nuova
		// situazione dei dischi
		for (int i = 0; i < sinistraB.length; i++) {
	    	sinistraB[i].setText(""+sinistra[i]);
	    	if (sinistra[i] != 0){
	    	sinistraB[i].setBackground(Color.ORANGE);
	    	} else {
	    		sinistraB[i].setBackground(Color.white);
	    	}
	    	centroB[i].setText(""+centro[i]);
	    	if (centro[i] != 0){
	    	centroB[i].setBackground(Color.green);
	    	} else {
	    		centroB[i].setBackground(Color.white);
	    	}
	    	destraB[i].setText(""+destra[i]);
	    	if (destra[i] != 0){
	    	destraB[i].setBackground(Color.CYAN);
	    	} else {
	    		destraB[i].setBackground(Color.white);
	    	}
	    	
	    }
		 
	}

	 
	

	
	
}
