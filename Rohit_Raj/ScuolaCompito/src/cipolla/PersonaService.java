package cipolla;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

public class PersonaService {
  public static final String NOME_FILE = "Anagrafica.txt";
    
    private BufferedReader fbr = null;
    private BufferedWriter fbw = null;
    
    private void openFile(boolean inScrittura) throws IOException, FileNotFoundException {
      if (inScrittura) {
        try {
          this.fbw = new BufferedWriter(new FileWriter("Anagrafica.txt"));
        } catch (IOException exc) {
          System.out.println(String.format("Durante l'apertura del file %s si verificato l'errore %s", new Object[] { "Anagrafica.txt", exc.getMessage() }));
          throw exc;
        } 
      } else {
        try {
          this.fbr = new BufferedReader(new FileReader("Anagrafica.txt"));
        } catch (FileNotFoundException exc) {
          System.err.println(String.format("Durante l'apertura del file %s si verificato l'errore %s", new Object[] { "Anagrafica.txt", exc.getMessage() }));
          throw exc;
        } 
      } 
    }
    
    public void writePersone(List<Anagrafica> vectPersone) throws IOException {
      openFile(true);
      for (Anagrafica unaPersona : vectPersone) {
        this.fbw.write(unaPersona.toString());
        this.fbw.newLine();
      } 
      this.fbw.flush();
      this.fbw.close();
    }
    
    public List<Anagrafica> readPersone() throws IOException {
      openFile(false);
      List<Anagrafica> resultPersone = new Vector<>();
      String row = this.fbr.readLine();
      while (row != null) {
        Anagrafica newPersona = new Anagrafica(row);
        resultPersone.add(newPersona);
        row = this.fbr.readLine();
      } 
      this.fbr.close();
      return resultPersone;
    }
  }