package cipolla;

import java.io.Serializable;

public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String cognome;
	private String telefono;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Persona(String rigaFile, String nome, String cogmome, String telefono) {
		String[] items = rigaFile.split("\t");
		setNome(items[0]);
		setCognome(items[1]);
		setTelefono(items[2]);
	}
	
	public String toString() {
		return String.format("%s\t%s\t%s", this.nome, this.cognome, this.telefono);
	}
}
