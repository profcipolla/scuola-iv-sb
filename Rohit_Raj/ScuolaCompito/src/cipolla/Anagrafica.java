package cipolla;

import java.io.Serializable;

public class Anagrafica implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String nome;
  
  private String cognome;
  
  private String telefono;
  
  public Anagrafica() {}
  
  public Anagrafica(String nome, String cognome, String telefono) {
    setNome(nome);
    setCognome(cognome);
    setTelefono(telefono);
  }
  
  public Anagrafica(String rigaFile) {
    String[] items = rigaFile.split("\t");
    setNome(items[0]);
    setCognome(items[1]);
    setTelefono(items[2]);
  }
  
  public String toString() {
    return String.format("%s\t%s\t%s", new Object[] { this.nome, this.cognome, this.telefono });
  }
  
  public String getNome() {
    return this.nome;
  }
  
  public void setNome(String nome) {
    this.nome = nome;
  }
  
  public String getCognome() {
    return this.cognome;
  }
  
  public void setCognome(String cognome) {
    this.cognome = cognome;
  }
  
  public String getTelefono() {
    return this.telefono;
  }
  
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
}