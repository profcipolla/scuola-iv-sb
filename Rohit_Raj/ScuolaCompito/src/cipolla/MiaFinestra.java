package cipolla;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MiaFinestra extends JFrame implements ActionListener{
	JButton avanti = new JButton(">");
	JButton indietro= new JButton ("<");
	JTextField t1 = new JTextField();
	JTextField t2 = new JTextField();
	JTextField t3 = new JTextField();
	JLabel lab = new JLabel ("Nome");
	JLabel lab1 = new JLabel("Cognome");
	JLabel lab2 = new JLabel("Telefono");
	private List<Anagrafica> tempList;
	  private int origine = 0;
	  
	public MiaFinestra() {
		setTitle("Mio progetto");
		setSize(400,400);
		setLayout(new GridLayout(4,2));
		add(lab);
		add(t1);
		add(lab1);
		add(t2);
		add(lab2);
		add(t3);
		add(indietro);
		add(avanti);
		
		richiamo(origine);
		avanti.addActionListener(this);
		indietro.addActionListener(this);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
	}
	  private void richiamo(int origine) {
		    PersonaService ps = new PersonaService();
		    try {
		      tempList = ps.readPersone();
		      t1.setText(tempList.get(origine).getNome());
		      t2.setText(tempList.get(origine).getCognome());
		      t3.setText(tempList.get(origine).getTelefono());
		    } catch (IOException e) {
		      //e.printStackTrace();
		    }
		  }
{
	
}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JButton bott = (JButton)e.getSource();
		System.out.println(bott.getText());
		if (bott == indietro) {
		      if(origine-1 == -1) {
		        return;
		      }
		      richiamo(origine -= 1);
		        
		    } else {
		      if(origine+1 > tempList.size()-1) {
		        return;
		      }
		      richiamo(origine += 1);

		    }
		  }
	
	public static void main (String [] args) {
		new MiaFinestra();
	}
}