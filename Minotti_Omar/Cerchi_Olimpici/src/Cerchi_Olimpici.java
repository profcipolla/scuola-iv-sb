import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Panel;

import javax.swing.JFrame;

public class Cerchi_Olimpici extends Panel {
    public void paint(Graphics g) {
        //g.drawLine(0, 0, 300, 300);
        //g.drawString("Cerchi Olimpici", 50, 50);
        Font f;
        
        for(int x = 300; x < 305; x++) {
            for(int y = 200; y < 205; y++) {
                g.setColor(Color.blue);
                g.drawOval(x, y, 125, 125);
            }
        }
        
        for(int x = 410; x < 415; x++) {
            for(int y = 200; y < 205; y++) {
                g.setColor(Color.black);
                g.drawOval(x, y, 125, 125);
            }
        }
        
        for(int x = 520; x < 525; x++) {
            for(int y = 200; y < 205; y++) {
                g.setColor(Color.red);
                g.drawOval(x, y, 125, 125);
            }
        }
        
        for(int x = 355; x < 360; x++) {
            for(int y = 250; y < 255; y++) {
                g.setColor(Color.yellow);
                g.drawOval(x, y, 125, 125);
            }
        }
        
        for(int x = 455; x < 460; x++) {
            for(int y = 250; y < 255; y++) {
                g.setColor(Color.green);
                g.drawOval(x, y, 125, 125);
            }
        }
        
        f = new Font("Helvetica", Font.ITALIC, 50);
        g.setFont(f);
        g.setColor(Color.black);
        g.drawString("GRECIA 2030", 305, 455);
    }
    
    public static void main(String[] args) {
    	JFrame f = new JFrame("Cerchi Olimpici");
		Cerchi_Olimpici c = new Cerchi_Olimpici();
		f.setBounds(0, 0, 900, 800);
		f.add(c);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);
	}
}