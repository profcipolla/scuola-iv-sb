import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Gioco_del_15 extends JFrame {
    JButton[][] button = new JButton[4][4];
    int righe,col;

    public Gioco_del_15() {
        Container container = getContentPane();
        container.setLayout(new GridLayout(4,4));

        Avvio avvia = new Avvio();
        
        //Aggiungo i bottoni;
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                JButton b = new JButton();
                button[i][j] = b;
                button[i][j].addActionListener(avvia);
                button[i][j].setBackground(Color.orange);
                container.add(button[i][j]);
            }
        }
    }

    //Creo l'interfaccia;
    public static void main(String args[]) {
    	Gioco_del_15 game = new Gioco_del_15();
        game.setTitle("Gioco del 15 - Minotti Omar");
        game.setVisible(true);
        game.setSize(400,400);
        game.mischia();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    //Mischio i numeri dei bottoni casualmente;
    public void mischia() {
        boolean[] b = new boolean[16];
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                int valore = (int)(16 * Math.random());
                while(b[valore]) {
                    valore = (int)(16 * Math.random());
                }
                b[valore] = true;
                if(valore != 0) {
                	button[i][j].setText("" + valore);
                }
                else {
                	button[i][j].setBackground(Color.white);
                	righe = i; col = j;
                }
            }
        }
    }

    //Avvio del gioco;
    class Avvio implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Object quadrato = e.getSource();
            for(int i = 0; i < 4; i++) {
                for(int j = 0; j < 4; j++) {
                    if(button[i][j] == quadrato) {
                        sposta(i,j);
                    }
                }
            }
        }
    }
    
    //Sposto i bottoni in base alla loro posizione;
    public void sposta(int i, int j) {
        if((i + 1 == righe && j == col) || (i - 1 == righe && j == col) || (i == righe && j + 1 == col) || (i == righe && j - 1 == col)) {
        	button[righe][col].setText(button[i][j].getText());
        	button[righe][col].setBackground(Color.orange);
            button[i][j].setText("");
            button[i][j].setBackground(Color.white);
            righe = i; col = j;
        } else if ((i + 2 == righe && j == col) || (i - 2 == righe && j == col)) {
        	button[righe][col].setText(button[i][j].getText());
        	button[righe][col].setBackground(Color.orange);
            button[i][j].setText("");
            button[i][j].setBackground(Color.white);
            righe = i; col = j;
        }
    }
}