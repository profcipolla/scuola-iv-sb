import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

public class PersonaService {
	public static final String NOME_FILE = "rubrica.txt";

	private BufferedReader fbr = null;
	private BufferedWriter fbw = null;

	private void openFile(boolean inScrittura) throws IOException, FileNotFoundException {
		// se si vuole aprire il file in scrittura...
		if (inScrittura) {
			try {
				this.fbw = new BufferedWriter(new FileWriter(NOME_FILE));
			} catch (IOException exc) {
				System.out.println(String.format("Durante l'apertura del file %s si � verificato l'errore %s",
						NOME_FILE, exc.getMessage()));
				throw exc;
			}
		} else {
			// ... altrimenti lo apre in lettura
			try {
				this.fbr = new BufferedReader(new FileReader(NOME_FILE));
			} catch (FileNotFoundException exc) {
				System.err.println(String.format("Durante l'apertura del file %s si � verificato l'errore %s",
						NOME_FILE, exc.getMessage()));

				throw exc;
			}
		}
	}

	public void writePersone(List<Persona> vectPersone) throws IOException {
		openFile(true);

		for (Persona unaPersona : vectPersone) {
			this.fbw.write(unaPersona.toString());
			this.fbw.newLine();
		}

		/*
		 * int numPersone = vectPersone.size(); for (int i = 0; i < numPersone; i++) {
		 * Persona unaPersona = vectPersone.get(i);
		 * this.fbw.write(unaPersona.toString()); this.fbw.newLine(); }
		 */

		this.fbw.flush();
		this.fbw.close();
	}

	public List<Persona> readPersone() throws IOException {
		openFile(true);

		List<Persona> resultPersone = new Vector<Persona>();

		String row = this.fbr.readLine();
		while (null != row) {
			// trasforma la stringa proveniente dalla riga del file in una Persona
			Persona newPersona = new Persona(row);
			resultPersone.add(newPersona);
			row = this.fbr.readLine();
		}

		this.fbr.close();
		
		return resultPersone;
	}
}
