
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class MainWin extends JFrame {
	private JTable tabUtenti;
	
	public  MainWin() {
		preparaDati();
		
		setTitle("Finestra con tabella");
		
		this.tabUtenti = new JTable(this.datiRighe, this.etichette);
		this.tabUtenti.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		this.tabUtenti.getColumnModel().getColumn(0).setPreferredWidth(1);
		this.tabUtenti.getColumnModel().getColumn(1).setPreferredWidth(127);
		this.tabUtenti.getColumnModel().getColumn(2).setPreferredWidth(127);
		JScrollPane pnlTab = new JScrollPane(tabUtenti);
		this.tabUtenti.setFillsViewportHeight(true);
		add(pnlTab);
		setSize(800, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	
	
	private String[] etichette = {"Posizione", "Nome", "Cognome"};
	private Object[][] datiRighe;
	
	private void preparaDati() {
		// dati da visualizzare nelle righe
		datiRighe = new Object[100][3];
		
		for (int i = 0; i < 100; i++) {
			Object[] uO = {i + 1, "Omar", "Minotti"};
			datiRighe[i] = uO;
		}
	}
}
