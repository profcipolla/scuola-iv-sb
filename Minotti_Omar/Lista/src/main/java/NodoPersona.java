
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NodoPersona {
	private Persona persona;
	private NodoPersona succ;
	private int progressivo;
	
	public NodoPersona() {}	
}
