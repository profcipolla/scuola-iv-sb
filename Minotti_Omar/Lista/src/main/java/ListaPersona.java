

public class ListaPersona {
	private int conta = 0;
	private NodoPersona testa = null;
	private NodoPersona current = null;

	public void add(Persona persona) {
		this.testa = new NodoPersona(persona, this.testa, ++this.conta);
	}

	public void inizio() {
		this.current = this.testa;
	}

	public Persona getCorrente() {
		if (null == this.current) {
			this.inizio();
		}

		Persona p = null;
		if (null != this.current) {
			p = this.current.getPersona();
			this.current = this.current.getSucc();
		}
		return p;
	}
	
	public NodoPersona cercaProgeressivo(int progressivo) {
		NodoPersona tempPunt = this.testa;

		while (null != tempPunt) {
			if (tempPunt.getProgressivo() == progressivo) {
				return tempPunt;
			}
			tempPunt = tempPunt.getSucc();
		}
		return null;
		
	}
	
	public void cancellaNodo(int progressivo) {
		
		NodoPersona precNodo = null;
		NodoPersona corrNodo = this.testa;
		while(null != corrNodo && !(progressivo == corrNodo.getProgressivo())) {
			precNodo = corrNodo;
			corrNodo = corrNodo.getSucc();
		}
		
		if (null != corrNodo) {
			if (null == precNodo) {
				this.testa = this.testa.getSucc();
			} else {
				
				precNodo.setSucc(corrNodo.getSucc());
			}
		}
		
	}

	public PersonaConProgressivo getCorrenteConProgressivo() {
		if (null == this.current) {
			this.inizio();
		}

		PersonaConProgressivo pcp = null;
		if (null != this.current) {
			pcp = new PersonaConProgressivo(this.current.getPersona(), this.current.getProgressivo());
			this.current = this.current.getSucc();
		}
		return pcp;
	}
}
