

public class Avvio {

	public static void main(String[] args) {
		ListaPersona lp = new ListaPersona();
		
		lp.add(new Persona("Mario", "Rossi"));
		lp.add(new Persona("Luigi", "Bianchi"));
		lp.add(new Persona("Marco", "Verdi"));
		lp.add(new Persona("Laura", "Rosa"));
		lp.add(new Persona("Letizia", "Gialla"));
		Persona p = new Persona("Omar", "Minotti");
		p.getNome();
		
		new PersonaFinestra(lp);
		
	}
}
