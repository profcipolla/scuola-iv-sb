package Ripetizione;


public class Avvio {

	public static void main(String[] args) throws Exception {
		Persona p = new Persona();
		MiaTastiera kb = new MiaTastiera();
		
		boolean isInputCorretto = false;
		
		//lettura del nome, richiedere il nome all'utente
		//finch� non � valido (il setter non ritorna una exc);
		do {
			System.out.println("Per favore, inserisci un nome: ");
			String nome = kb.leggiTesto();
			try {
				p.setNome(nome); 
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			  }
		} while(!isInputCorretto);
		isInputCorretto = false;
		
		//lettura del cognome, richiedere il nome all'utente
		//finch� non � valido;
		do {
			System.out.println("Per favore, inserisci un cognome: ");
			String cognome = kb.leggiTesto();
			try {
				p.setCognome(cognome); 
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			  }
		} while(!isInputCorretto);
		isInputCorretto = false;
		
		//lettura dell'et�, costringo l'utente ad inserire;
		//finch� non � valido;
		do {
			System.out.println("Per favore, inserisci l'et�: ");
			Integer et� = kb.leggiIntero();
			try {
				p.setEt�(et�); 
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			  }
		} while(!isInputCorretto);
		isInputCorretto = false;
		
		
		//scrivere i valori inseriti;
		System.out.println(p.Stampa());
	}

}
