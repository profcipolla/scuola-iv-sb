package Ripetizione;



public class Persona {
	private String nome;
	private String cognome;
	private Integer et�;
	private int max = 100;
	private int min = 0;
	
	
	
	
	
	public String getNome() {
		return nome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public Integer getEt�() {
		return et�;
	}
	
	
	
	
	public void setNome(String nome) throws Exception {
		if(null == nome || "".equals(nome.trim())) {
			throw new Exception("Il nome non � valido");
		} 
		this.nome = nome;
	}
	
	public void setCognome(String cognome) throws Exception {
		if(null == cognome || "".equals(cognome.trim())) {
			throw new Exception("Il cognome non � valido");
		} 
		this.cognome = cognome;
	}
	
	public void setEt�(Integer et�) throws Exception {
		if (et� > max || et� < min) {
			throw new Exception("L'et� non � valida");
		}
		this.et� = et�;
	}
	
	
	
	
	public String Stampa() {
		return "Nome: " + nome + "\nCognome: " + cognome + "\nEt�: " + et�;
	}
}
