
public class Avvio {

	public static void main(String[] args) throws Exception {
		Pila miaPila = new Pila();
		
		miaPila.push("Prima Stringa");
		miaPila.push("Seconda Stringa");
		miaPila.push("Terza Stringa");
		miaPila.push("Quarta Stringa");
		
		System.out.println(miaPila.pop());
		System.out.println(miaPila.pop());
		System.out.println(miaPila.pop());
		
		miaPila.push("Quinta Stringa");
		
		System.out.println(miaPila.pop());
		System.out.println(miaPila.pop());
		//System.out.println(miaPila.pop()); Errore, la pila non ha pi� elementi
		
		Pila<Persona> nuovaPila = new Pila<Persona>();
		//Pila<Automobile> nuovaPila = new Pila<Automobile>();
		nuovaPila.push(new Persona());
	}
}
