public class Pila<Tipo> {
	private Nodo<Tipo> head; // String sostituisce Tipo
	
	public void push (Tipo info) {
		this.head = new Nodo<Tipo>(info, this.head);
	}
	
	public Tipo pop() throws Exception {
		if(null == this.head) {
			// la pila � vuota, restituisco un'eccezione
			throw new Exception("Pila vuota. Operazione pop() non consentita");
		}
		Tipo info = this.head.getInfo();
		this.head = this.head.getLink();
		return info;
	}
	
	public Tipo pull() throws Exception {
		if(null == this.head) {
			// la pila � vuota, restituisco un'eccezione
			throw new Exception("Pila vuota. Operazione pop() non consentita");
		}
		return this.head.getInfo();
	}
	
	public boolean isEmpty() throws Exception {
		return null == this.head;
	}
}