
public class Coda<Tipo> {
	private Nodo<Tipo> head;
	
	// Metti in coda
	public void enQueue(String info) {
		//this.head = new Nodo<Tipo>(info, this.head);
	}
	
	// Togli dalla coda
	public String deQueue() throws Exception {
		// Chiedersi sempre se c'� qualcosa
		if(null != this.head) {
			throw new Exception("Coda vuota. Impossibile eseguire il metodo deQueue()");
		}
		
		Nodo<String> tempNodo = null; //this.head;
		Nodo<String> precNodo = null;
		
		// La coda � popolata
		while(null != tempNodo.getLink()) {
			precNodo = tempNodo;
			tempNodo = tempNodo.getLink();
		}
		String info = tempNodo.getInfo();
		
		// Rimuovere il precedente
		if(null == precNodo) {
			this.head = null;
		} else {
			precNodo.setLink(null);
		}
 		return info;
	}
}
