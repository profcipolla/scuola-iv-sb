import lombok.Data;

@Data
public class Nodo<Tipo> { // T per generalizzare le info. , K> per inserire un altro tipo
	private Tipo info;
	//private K info2; K per inserire un'altra info
	private Nodo<Tipo> link;
	
	public Nodo (Tipo info2, Nodo link) {
		this.info = info2;
		this.link = link;
	}
}