
public interface ILista {
	void add(Automobile auto);
	void print();
	void delete(String targa);
	Automobile search(String targa);
}
