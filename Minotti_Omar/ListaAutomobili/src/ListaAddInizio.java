
public class ListaAddInizio implements ILista {
	private Nodo head;
	
	/*
	public Lista() {
		if(null == this.head) {
			System.out.println(this.head.getInfo().toString());
		}
		this.head = null;
	}
	*/
	
	public void add(Automobile automobile) {
		Nodo nuovoNodo = new Nodo(automobile, this.head);
		this.head = nuovoNodo;
	}
	
	public void add2end(Automobile automobile) {
		Nodo nuovoNodo = new Nodo(automobile, null);
		if(null == this.head) {
			this.head = nuovoNodo;
		} else {
			Nodo copiaHead = this.head;
		
			while(null != copiaHead.getSucc()) {
				copiaHead = copiaHead.getSucc();
			}
			copiaHead.setSucc(nuovoNodo);
		}
	}
	
	public void addOrdered(Automobile automobile) {
		
	}
	
	public void delete(String targa) {
		if(null == targa || "".equals(targa.trim())) {
			return;
		}
		
		targa = targa.trim().toUpperCase();
		
		if(null == this.head) {
			return;
		}
		
		Nodo prevNodo = null;
		Nodo currNodo = this.head;
		while(null != currNodo && !targa.equals(currNodo.getInfo().getTarga())) {
			prevNodo = currNodo;
			currNodo = currNodo.getSucc();
		}
		if(null != currNodo) {
			if(null == prevNodo) {
				this.head = this.head.getSucc();
			} else {
				prevNodo.setSucc(currNodo.getSucc());
			}
		}
	}
	
	public Automobile search(String targa) {
		Nodo tempPunt = this.head;
		
		while(null != tempPunt) {
			if(tempPunt.getInfo().getTarga().equals(targa.toUpperCase())) {
				return tempPunt.getInfo();
			}
			//System.out.println(tempPunt.getInfo());
			tempPunt = tempPunt.getSucc();
		}
		return null;
	}
	
	public void print() {
		Nodo tempPunt = this.head;
		// se ci sono elementi posizionati sul primo scrivi il contenuto, se c'� un successivo posizionati sul successivo, scrivi il contenuto, se c'� un successivo po..
		while(null != tempPunt) {
			System.out.println(tempPunt.getInfo());
			tempPunt = tempPunt.getSucc();
		}
	}
}
