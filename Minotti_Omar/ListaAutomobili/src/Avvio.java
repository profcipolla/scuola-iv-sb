
public class Avvio {
	public static void main(String[] args) {
		System.out.println("Avvio Programma" + "\n");
		
		ILista miaLista = new ListaAddFine();
		try {
			Thread.sleep(2000);
			System.out.println("Caricamento Lista.." + "\n");
			Thread.sleep(2000);
			
			miaLista.add(new Automobile("RM0001", 1999, "Fiat Punto"));
			miaLista.add(new Automobile("RM0002", 1999, "Fiat Panda"));
	
			Automobile ferrari = new Automobile("RM0003", 2018, "Ferrari Roma");
			miaLista.add(ferrari);
	
			Automobile maserati = new Automobile("RM0004", 2016, "Maserati Ghibli");
			//miaLista.add2end(maserati);
			miaLista.add(maserati);
			miaLista.print();
			System.out.println("\n");
			
			miaLista.delete("RM0003");
			miaLista.delete("RM0002");
			miaLista.delete("RM0001");
			miaLista.print();
			
			} catch(Exception exc) {
					System.out.println("Si � verificato l'errore " + exc.getMessage());
			}
	}
}
