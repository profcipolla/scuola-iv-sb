
public class Automobile {
		private String targa;
		private String modello;
		private int anno;
		
		public Automobile(String targa, int anno, String modello) {
			this.targa = targa;
			this.anno = anno;
			this.modello = modello;
		}

		public String getTarga() {
			return targa;
		}

		

		public String getModello() {
			return modello;
		}

		

		public int getAnno() {
			return anno;
		}

		

		public void setTarga(String targa) throws Exception {
			if(null == targa || 0 == targa.trim().length()) {
				throw new Exception("Targa non valida");
			}
			this.targa = targa.trim().toUpperCase();
		}
		
		public void setModello(String modello) {
			this.modello = modello;
		}
		
		public void setAnno(int anno) throws Exception {
			if(anno < 1880) {
				throw new Exception("Anno non valido");
			}
			this.anno = anno;
		}
		
		public String toString() {
			return String.format("Targa: %s, Modello: %s, Anno: %d.", this.targa, this.modello, this.anno);
		}
}
