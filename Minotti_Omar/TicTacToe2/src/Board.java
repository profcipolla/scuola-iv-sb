import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Board implements ActionListener {
	Random random = new Random();
	JFrame frame = new JFrame();
	JPanel titolo = new JPanel();
	JPanel button = new JPanel();
	JLabel testo = new JLabel();
	JButton[] buttons = new JButton[9];
	boolean turno_giocatore1;
	
	//Creo l'interfaccia;
	Board() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(450,450);
		frame.getContentPane().setBackground(new Color(50,50,50));
		frame.setLayout(new BorderLayout());
		frame.setTitle("Esercizio Minotti Omar");
		frame.setVisible(true);
		
		testo.setBackground(new Color(25,25,25));
		testo.setForeground(new Color(25,255,0));
		testo.setFont(new Font("Calibri",Font.BOLD,36));
		testo.setHorizontalAlignment(JLabel.CENTER);
		testo.setText("Tic-Tac-Toe");
		testo.setOpaque(true);
		
		titolo.setLayout(new BorderLayout());
		titolo.setBounds(0,0,450,100);
		
		button.setLayout(new GridLayout(3,3));
		button.setBackground(new Color(68,68,68));
		
		for(int i = 0; i < 9; i++) {
			buttons[i] = new JButton();
			button.add(buttons[i]);
			buttons[i].setFont(new Font("Verdana",Font.BOLD,60));
			buttons[i].setFocusable(false);
			buttons[i].addActionListener(this);
		}
		
		titolo.add(testo);
		frame.add(titolo,BorderLayout.NORTH);
		frame.add(button);
		
		primoTurno();
	}
	
	//Implemento l'evento per inserire i segni;
	@Override
	public void actionPerformed(ActionEvent e) {
		for(int i = 0; i < 9; i++) {
			if(e.getSource() == buttons[i]) {
				if(turno_giocatore1) {
					if(buttons[i].getText() == "") {
						buttons[i].setForeground(new Color(255,0,0));
						buttons[i].setText("X");
						turno_giocatore1 = false;
						testo.setText("Turno di O");
						controllo();
					}
				} else {
					if(buttons[i].getText() == "") {
						buttons[i].setForeground(new Color(0,0,255));
						buttons[i].setText("O");
						turno_giocatore1 = true;
						testo.setText("Turno di X");
						controllo();
					}
				}
			}
		}
	}
	
	//Stabilisco chi inizia;
	public void primoTurno() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(random.nextInt(2) == 0) {
			turno_giocatore1 = true;
			testo.setText("Turno di X");
		} else {
			turno_giocatore1 = false;
			testo.setText("Turno di O");
		}
	}
	
	public void controllo() {
		//Controllo come pu� vincere X;
		if((buttons[0].getText() == "X" &&
			buttons[1].getText() == "X" &&
			buttons[2].getText() == "X")
			) {
			xVince(0,1,2);
		}
		if((buttons[3].getText() == "X" &&
			buttons[4].getText() == "X" &&
			buttons[5].getText() == "X")
			) {
			xVince(3,4,5);
		}
		if((buttons[6].getText() == "X" &&
			buttons[7].getText() == "X" &&
			buttons[8].getText() == "X")
			) {
			xVince(6,7,8);
		}
		if((buttons[0].getText() == "X" &&
			buttons[3].getText() == "X" &&
			buttons[6].getText() == "X")
			) {
			xVince(0,3,6);
		}
		if((buttons[1].getText() == "X" &&
			buttons[4].getText() == "X" &&
			buttons[7].getText() == "X")
			) {
			xVince(1,4,7);
		}
		if((buttons[2].getText() == "X" &&
			buttons[5].getText() == "X" &&
			buttons[8].getText() == "X")
			) {
			xVince(2,5,8);
		}
		if((buttons[0].getText() == "X" &&
			buttons[4].getText() == "X" &&
			buttons[8].getText() == "X")
			) {
			xVince(0,4,8);
		}
		if((buttons[2].getText() == "X" &&
			buttons[4].getText() == "X" &&
			buttons[6].getText() == "X")
			) {
			xVince(2,4,6);
		}
		
		//Controllo come pu� vincere O;
		if((buttons[0].getText() == "O" &&
				buttons[1].getText() == "O" &&
				buttons[2].getText() == "O")
				) {
				oVince(0,1,2);
			}
			if((buttons[3].getText() == "O" &&
				buttons[4].getText() == "O" &&
				buttons[5].getText() == "O")
				) {
				oVince(3,4,5);
			}
			if((buttons[6].getText() == "O" &&
				buttons[7].getText() == "O" &&
				buttons[8].getText() == "O")
				) {
				oVince(6,7,8);
			}
			if((buttons[0].getText() == "O" &&
				buttons[3].getText() == "O" &&
				buttons[6].getText() == "O")
				) {
				oVince(0,3,6);
			}
			if((buttons[1].getText() == "O" &&
				buttons[4].getText() == "O" &&
				buttons[7].getText() == "O")
				) {
				oVince(1,4,7);
			}
			if((buttons[2].getText() == "O" &&
				buttons[5].getText() == "O" &&
				buttons[8].getText() == "O")
				) {
				oVince(2,5,8);
			}
			if((buttons[0].getText() == "O" &&
				buttons[4].getText() == "O" &&
				buttons[8].getText() == "O")
				) {
				oVince(0,4,8);
			}
			if((buttons[2].getText() == "O" &&
				buttons[4].getText() == "O" &&
				buttons[6].getText() == "O")
				) {
				oVince(2,4,6);
			}
	}
	
	public void xVince(int a, int b, int c) {
		buttons[a].setBackground(Color.GREEN);
		buttons[b].setBackground(Color.GREEN);
		buttons[c].setBackground(Color.GREEN);
		
		for(int i = 0; i < 9; i++) {
			buttons[i].setEnabled(false);
		}
		testo.setText("X ha vinto!");
	}
	
	public void oVince(int a, int b, int c) {
		buttons[a].setBackground(Color.GREEN);
		buttons[b].setBackground(Color.GREEN);
		buttons[c].setBackground(Color.GREEN);
		
		for(int i = 0; i < 9; i++) {
			buttons[i].setEnabled(false);
		}
		testo.setText("O ha vinto!");
	}
}
