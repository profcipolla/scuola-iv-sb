
public class Avvio {

	public static void main(String[] args) {
		ListaPersona lp = new ListaPersona();
		
		lp.add(new Persona("Mario", "Rossi"));
		lp.add(new Persona("Luigi", "Bianchi"));
		lp.add(new Persona("Marco", "Verdi"));
		lp.add(new Persona("Laura", "Rosa"));
		lp.add(new Persona("Letizia", "Gialla"));
		
		new PersonaFinestra(lp);
		
	}
}