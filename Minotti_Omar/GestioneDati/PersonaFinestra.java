
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PersonaFinestra extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	private JTextField tProgressivo = new JTextField();
	private JTextField tNome = new JTextField();
	private JTextField tCognome= new JTextField();
	private JButton bInizio = new JButton("Inizio");
	private JButton bSuccessivo= new JButton("Successivo");
	private JButton bModifica = new JButton("Modifica");
	private JButton bCancella = new JButton("Cancella");
	
	private ListaPersona lp;
	
	public PersonaFinestra(ListaPersona lp) {
		this.lp = lp;
		this.initUi();
		this.init();
	}

	private void initUi() {
		setLayout(new GridLayout(4, 2));
		
		add(new JLabel("Progressivo:"));
		add(tProgressivo);
		
		add(new JLabel("Nome:"));
		add(tNome);
		
		add(new JLabel("Cognome:"));
		add(tCognome);
		
		add(bInizio);
		add(bSuccessivo);
		add(bModifica);
		add(bCancella);
		
		bInizio.addActionListener(this);
		bSuccessivo.addActionListener(this);
		bModifica.addActionListener(this);
		bCancella.addActionListener(this);
		
		
		setSize(800, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private void init() {
		this.lp.inizio();
		PersonaConProgressivo pcp = lp.getCorrenteConProgressivo();
		
		if (null != pcp) {
			this.tProgressivo.setText("" + pcp.getProgressivo());
			
			Persona p = pcp.getPersona();
			this.tNome.setText(p.getNome());
			this.tCognome.setText(p.getCognome());
		}
	}

	public void actionPerformed(ActionEvent e) {
		JButton bottone = (JButton)e.getSource();
		
		if (bInizio.equals(bottone)) {
			// riporto la lista su inizio
			this.lp.inizio();
		}
		
		 // prendere il current dalla lista
		PersonaConProgressivo pcp = lp.getCorrenteConProgressivo();
		
		if (null != pcp) {
			this.tProgressivo.setText("" + pcp.getProgressivo());
			
			Persona p = pcp.getPersona();
			this.tNome.setText(p.getNome());
			this.tCognome.setText(p.getCognome());
		}
	}
}
