
import it.armellini.ivsb.listaswing.anagrafica.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersonaConProgressivo {
	private Persona persona;
	private int progressivo;
}
