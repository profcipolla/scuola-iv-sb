
public class AvvioHanoi {
	private static int numMosse = 0;
	public static void main(String[] args) {
		// numMosse = 0;
		// hanoi(5, "SINISTRA", "CENTRO", "DESTRA");
	
		HanoiWin hw = new HanoiWin();
		HanoiModel hm = new HanoiModel(hw);
		hm.avvia();
		
		
	}
	
	
	
	public static void hanoi(int numDischi, String sorgente, String appoggio, String destinazione) {
		if (1 == numDischi) {
			System.out.println((++numMosse) + " Sposta il disco da " + sorgente + " a " + destinazione);
		} else {
			hanoi(numDischi - 1, sorgente, destinazione, appoggio);
			System.out.println((++numMosse) + " Sposta il disco da " + sorgente + " a " + destinazione);
			hanoi(numDischi - 1, appoggio, sorgente, destinazione);
		}
	}
}
