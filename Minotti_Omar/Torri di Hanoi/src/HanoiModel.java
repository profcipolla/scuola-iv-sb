
public class HanoiModel {
	public static final int DISCHI = 10;
	private HanoiWin hw;
	
	public int[] sinistra = new int[DISCHI];
	public int[] centro = new int[DISCHI];
	public int[] destra = new int[DISCHI];
	public HanoiModel() {}
	public HanoiModel(HanoiWin hw) {
		this.hw = hw;
	}
	
	public void avvia() {
		for (int i = 0; i < DISCHI; i++) {
			sinistra[i] = i +1;
		}
		
		risolviHanoi(DISCHI, sinistra, centro, destra);
	}
	
	private void risolviHanoi(int numDischi, int[] sorgente, int[] appoggio, int[] destinazione) {
		if (1 == numDischi) {
			// System.out.println("Sposta il disco da " + sorgente + " a " + destinazione);
			int disco = this.pop(sorgente);
			this.push(disco, destinazione);
			// mostra a video questa situazione e aspetta un secondo
			disegna();
		} else {
			risolviHanoi(numDischi - 1, sorgente, destinazione, appoggio);
			// System.out.println((++numMosse) + " Sposta il disco da " + sorgente + " a " + destinazione);
			int disco = this.pop(sorgente);
			this.push(disco, destinazione);
			// mostra a video questa situazione e aspetta un secondo
			disegna();
			risolviHanoi(numDischi - 1, appoggio, sorgente, destinazione);
		}
	}
	
	private int pop(int[] torre) {
		// immaginate 0 0 0 3 4 5, deve estrarre 3 e sostituirlo con 0 nell'array
		int i = 0;
		while(i < DISCHI && 0 == torre[i]) {
			i++;
		}
		// ho trovato il primo valore diverso da 0
		int result = torre[i];
		// lo sostituisco con 0 per simulare che ho preso il disco
		torre[i] = 0;
		return result;
	}
	
	private void push(int valore, int[] torre) {
		// immaginate la situazione
		// 0 0 0 0 6 7 e di volere inserire il disco 3
		// nel seguente modo 0 0 0 3 6 7
		int i = DISCHI - 1;
		while(i >= 0 && 0 != torre[i]) {
			i--;
		}
		torre[i] = valore;
	}
	
	private void disegna() {
		this.hw.disegna(sinistra, centro, destra);
		attesa();
	}
	
	private void attesa() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
