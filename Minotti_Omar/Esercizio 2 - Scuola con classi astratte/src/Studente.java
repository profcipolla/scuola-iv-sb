
public class Studente extends Persona {
	public static int CONTATORE_MATRICOLA = 1;
	
	private int matricola;
	private int classe;
	private String sezione;
	
	public Studente(int classe, String sezione) {
		this.matricola = Studente.CONTATORE_MATRICOLA++;
		this.setClasse(classe);
		this.setSezione(sezione);
	}
	
	@Override
	public String getIdentificativo() {
		return "Matricola " + matricola;
	}
	
	public int getMatricola() {
		return matricola;
	}

	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}

	public int getClasse() {
		return classe;
	}

	public void setClasse(int classe) {
		this.classe = classe;
	}

	public String getSezione() {
		return sezione;
	}

	public void setSezione(String sezione) {
		this.sezione = sezione;
	}

}