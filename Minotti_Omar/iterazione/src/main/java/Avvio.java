import java.util.Iterator;

public class Avvio {

	public static void main(String[] args) throws Exception {
		Classe miaClasse = new Classe();
		miaClasse.add(new Studente("Nathan", "Drake"));
		miaClasse.add(new Studente("Victor", "Sullivan"));
		
		Iterator<Studente> ci = miaClasse.iterator();
		while (ci.hasNext()) {
			System.out.println(ci.next());
		}
		
		// for each:
		for (Studente s: miaClasse) {
			System.out.println(s);
		}
	}

}
