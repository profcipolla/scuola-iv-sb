import java.util.Iterator;

public class ClasseIterator implements Iterator<Studente> {
	
	private Studente[] studenti;
	private int posizioneCorrente;
	
	public ClasseIterator(Studente[] studenti) {
		this.studenti = studenti;
		this.posizioneCorrente = -1;
	}

	public boolean hasNext() {
		if (null != studenti[posizioneCorrente + 1]) {
			return true;
		} else {
			return false;
		}
		// return null != studenti[posizioneCorrente + 1]
	}

	public Studente next() {
		posizioneCorrente++;
		return studenti[posizioneCorrente];
	}
}
