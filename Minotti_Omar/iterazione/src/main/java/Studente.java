import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Studente {
	
	private String nome;
	private String cognome;
}
