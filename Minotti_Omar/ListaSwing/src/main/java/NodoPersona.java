import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
//@NoArgs
public class NodoPersona {
	private Persona persona;
	private NodoPersona succ;
	private int progressivo;
}
