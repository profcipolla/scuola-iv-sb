import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PersonaUi extends JFrame implements ActionListener {
	
	private JTextField tProgressivo = new JTextField();
	private JTextField tNome = new JTextField();
	private JTextField tCognome = new JTextField();
	private JButton bInizio = new JButton("Inizio");
	private JButton bSuccessivo =  new JButton("Successivo");
	
	private ListaPersona lp;
	
	public PersonaUi(ListaPersona lp) {
		this.lp = lp;
		this.initUi();
		this.init();
	}
	
	private void initUi() {
		setLayout(new GridLayout(4,2));
		
		add(new JLabel("Progressivo:"));
		add(tProgressivo);
		
		add(new JLabel("Nome:"));
		add(tNome);
		
		add(new JLabel("Cognome:"));
		add(tCognome);
		
		add(bInizio);
		add(bSuccessivo);
		
		bInizio.addActionListener(this);
		bSuccessivo.addActionListener(this);
		
		setSize(600,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	private void init() {
		this.lp.inizio();
		PersonaConProgressivo pcp = lp.getCorrenteProgressivo(); // prendere il current dalla lista
		
		if(null != pcp) {
			this.tProgressivo.setText("" + pcp.getProgressivo());
			
			Persona p = pcp.getPersona();
			this.tNome.setText(p.getNome());
			this.tCognome.setText(p.getCognome());
		}
	}

	public void actionPerformed(ActionEvent e) {
		JButton bottone = (JButton)e.getSource();
		
		if(bInizio.equals(bottone)) {
			// riporto la lista su inizio
			this.lp.inizio();
		}
		Persona p = lp.getCorrente(); // prendere il current dalla lista
		
		if(null != p) {
			this.tNome.setText(p.getNome());
			this.tCognome.setText(p.getCognome());
		}
	}
}
