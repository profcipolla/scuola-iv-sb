
public class Avvio {

	public static void main(String[] args) {
		ListaPersona lp = new ListaPersona();
		
		lp.add(new Persona("Omar","Minotti"));
		lp.add(new Persona("Giulio","Cesare"));
		lp.add(new Persona("Alessandro","Magno"));
		lp.add(new Persona("Ottaviano","Augusto"));
		lp.add(new Persona("Nerone","Imperatore"));
		
		new PersonaUi(lp);
	}

}
