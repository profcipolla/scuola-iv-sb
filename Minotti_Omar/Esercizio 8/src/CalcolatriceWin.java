
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class CalcolatriceWin {
	//Creo i pulsanti;
	String [] scritte = {"7", "8", "9", "/", "4", "5", "6", "+", "1", "2",
			"3", "-", "0", ",", "=", "*"};
	JButton[] pulsanti = new JButton[16];
		//Creo la calcolatrice;
		public CalcolatriceWin() {
			JFrame calcWin = new JFrame();
			calcWin.setLayout(new GridLayout(2, 2, 5, 5));
			
			//Creo lo spazio vuoto;
			JTextArea text = new JTextArea();
			calcWin.add(text);
			
			//Creo il pannello;
			JPanel mioPan = new JPanel();
			calcWin.add(mioPan);
			mioPan.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.white));
			
			//Creo il layout;
			GridLayout gl = new GridLayout(4, 4, 2, 2);
			mioPan.setLayout(gl);
		
			
			//Creo gli oggetti;
			for (int i = 0; i < scritte.length; i++) {
				pulsanti[i] = new JButton(scritte[i]);
				pulsanti[i].setFont(new Font("Verdana", Font.BOLD, 26));
				mioPan.add(pulsanti[i]);
			}
			
			//Titolo;
			calcWin.setTitle("Calcolatrice");
			//La ridimensiono;
			calcWin.setSize(400, 500);
			//Chiudo l'app quando chiudo la finestra;
			calcWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//La rendo visibile;
			calcWin.setVisible(true);
			//Impacchetta;
			calcWin.pack();
		}
}
