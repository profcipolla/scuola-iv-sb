import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;

public class MiaFinestra {
  
  public MiaFinestra() {
    JFrame win = new JFrame();
    
    win.setTitle("Registrazione");
    win.setSize(800,400);
    
    //-------------------------------------------;
    
    JFrame myframe = new JFrame();
    JMenuBar menubar = new JMenuBar();
    JMenu menu = new JMenu("File");
    JMenuItem size = new JMenuItem("Salva");
    menu.add(size);
    menubar.add(menu);
    myframe.setJMenuBar(menubar);
    
    JMenu menu2 = new JMenu("Modifica");
    JMenuItem size2 = new JMenuItem("Cancella");
    menu2.add(size2);
    menubar.add(menu2);
    
    JMenu menu3 = new JMenu("Aiuto");
    menubar.add(menu3);
    
    JMenu menu4 = new JMenu("?");
    menubar.add(menu4);
    
    //-------------------------------------------;
    
    //Gestisce la disposizione dei componenti all'interno della finestra;
    //FL � un gestore che accorda i componenti finche c'� spazio sulla riga,
    //poi va su una nuova riga;
    LayoutManager mioLayout = new FlowLayout(); 
    GridLayout layout1 = new GridLayout(13,1);
    win.setLayout(layout1);
    
    JLabel spazio = new JLabel();
    JLabel nome = new JLabel("Nome: ");
    JTextField testoNome = new JTextField("Inserisci Nome");
    JLabel cognome = new JLabel("Cognome: ");
    JTextField testoCognome = new JTextField("Inserisci Cognome");
    JLabel natoIl = new JLabel("Data di nascit�: ");
    JTextField testoNatoIl = new JTextField("Inserisci Data di nascit�");
    JLabel cf = new JLabel("Codice fiscale: ");
    JTextField testoCf = new JTextField("Codice fiscale");
    JLabel spazio2 = new JLabel();
    
    win.add(spazio);
    win.add(nome);
    win.add(testoNome);
    win.add(cognome);
    win.add(testoCognome);
    win.add(natoIl);
    win.add(testoNatoIl);
    win.add(cf);
    win.add(testoCf);
    win.add(spazio2);
    
    JButton b1 = new JButton("Invia");
    JButton b2 = new JButton("Chiudi");

    win.add(b1);
    win.add(b2);

    
    //win.pack();
    //win.size(800,600) per versione 8 java;
    win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//se chiudo la finestra termina l'applicazione;
    win.setVisible(true);//per visualizzare la finestra;
    
    win.setJMenuBar(menubar);
    win.validate();
    win.repaint();
  }

}