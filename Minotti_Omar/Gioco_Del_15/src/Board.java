import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Board implements ActionListener {
	JFrame frame = new JFrame("Gioco del 15 - Minotti Omar");
	JPanel panel = new JPanel();
	JButton [][] button = new JButton[4][4];
	int righe = 4;
	int col = 4;
	JLabel[][] etichette = new JLabel[righe][col];
	int[][] board = new int[righe] [col];
	
	//Creo l'interfaccia;
	Board() {
		panel.setBackground(Color.WHITE);
		panel.setLayout(new GridLayout(4,4));
		frame.setSize(570,570);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		this.Board2();
		
		//Assegno numeri casuali alle posizioni;
		for(int i = 0; i < righe; i++) {
			for(int j = 0; j < col; j++) {
				button[i] [j] = new JButton();
				String text = i + "-" + j;
				button[i][j].setText(text);
				button[i][j].setFont(new Font("Verdana",Font.BOLD,0));
				button[i][j].addActionListener(this);
				
				int val = board[i][j];
				String nomeFile;
				if(val != -1) {
					nomeFile = "Numeri/" + val + ".png";
					etichette[i][j] = new JLabel(new ImageIcon(nomeFile),JLabel.CENTER);
				} else {
					etichette[i][j] = new JLabel("");
				}
				button[i][j].add(etichette[i][j]);
				button[i][j].setBackground(Color.WHITE);
				panel.add(button[i][j]);
		}
		
		frame.add(panel);
		frame.setVisible(true);
		}
		
	}
	
	public void Board2() {
		Random r = new Random();
		int [] a = new int[16];
		for(int i = 0; i < 16; i++) {
			a[i] = i + 1;
		}
		a[15] = -1;
		for(int i = 0; i < 16; i++) {
			int i2 = r.nextInt(16);
			int temp = a[i];
			a[i] = a[i2];
			a[i2] = temp;
		}
		int conta = 0;
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++) {
				board [i][j] = a[conta];
				conta = conta + 1;
				//System.out.print(board[i][j] + "\t");
			}
			//System.out.print("\n");
		}
	}
	
	Boolean vittoria() {
		int conta = 1;
		
		for(int i = 0; i < righe; i++) {
			for(int j = 0; j < col; j++) {
				if(board[i][j] != conta && board[i][j] != -1) {
					return false;
				}
				conta = conta + 1;
			}
		}
		return true;
	}
	
	public void mostraMess() {
		JFrame frame = new JFrame("Complimenti");
		JLabel label = new JLabel("Hai risolto il gioco!", JLabel.CENTER);
		label.setFont(new Font("Verdana",Font.BOLD,25));
		frame.add(label);
		frame.setLayout(new GridLayout(1,1));
		frame.setSize(400,200);
		frame.setBackground(Color.WHITE);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		Boolean b = vittoria();
		if(b == false) {
			String s = ae.getActionCommand().toString();
			int r = Integer.parseInt(s.split("-")[0]);
			int c = Integer.parseInt(s.split("-")[1]);
			
			//Riconosci la riga e la colonna cliccata;
			if(board[r][c] != -1) {
				//Controlla tutte le posizioni (su, gi�, destra e sinistra) e cerca dov'� -1;
				//Ovvero l'immagine del trattino;
				if(r + 1 < righe && board[r + 1][c] == -1) {	//Su;
					//Allora scambia l'immagine in questa posizione;
					etichette[r][c].setIcon(new ImageIcon(""));
					String nomeFile = "Numeri/" + board[r][c] + ".png";
					etichette[r + 1][c].setIcon(new ImageIcon(nomeFile));
					
					int temp = board[r][c];
					board[r][c] = board[r + 1][c];
					board[r + 1][c] = temp;
				} else if(r - 1 >= 0 && board[r - 1][c] == -1) {	//Gi�;
					etichette[r][c].setIcon(new ImageIcon(""));
					String nomeFile = "Numeri/" + board[r][c] + ".png";
					etichette[r - 1][c].setIcon(new ImageIcon(nomeFile));
					
					int temp = board[r][c];
					board[r][c] = board[r - 1][c];
					board[r - 1][c] = temp;
				} else if(c + 1 < col && board[r][c + 1] == -1) {		//Destra;
					etichette[r][c].setIcon(new ImageIcon(""));
					String nomeFile = "Numeri/" + board[r][c] + ".png";
					etichette[r][c + 1].setIcon(new ImageIcon(nomeFile));
					
					int temp = board[r][c];
					board[r][c] = board[r][c + 1];
					board[r][c + 1] = temp;
				} else if(c - 1 >= 0 && board[r][c - 1] == -1) {		//Sinistra;
					etichette[r][c].setIcon(new ImageIcon(""));
					String nomeFile = "Numeri/" + board[r][c] + ".png";
					etichette[r][c - 1].setIcon(new ImageIcon(nomeFile));
					
					int temp = board[r][c];
					board[r][c] = board[r][c - 1];
					board[r][c - 1] = temp;
				}
			}
			b = vittoria();
			if(b == true) {
				mostraMess();
			}
		}
	}
}
