public class Autore {
	private String nome;
	private String cognome;
	private String nazionalita;

	public Autore(String nome, String cognome, String nazionalita) {
		this.setNome(nome);
		this.setCognome(cognome);
		this.setNazionalita(nazionalita);
	}

	public Autore() {
		// TODO Auto-generated constructor stub
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNazionalita() {
		return nazionalita;
	}

	public void setNazionalita(String nazionalita) {
		this.nazionalita = nazionalita;
	}

	@Override
	public String toString() {
		return "Nome: " + this.nome + "\nCognome: " + this.cognome + "\nNazionalita: " + this.nazionalita;
	}

}
