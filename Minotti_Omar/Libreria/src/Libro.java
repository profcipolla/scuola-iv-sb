public class Libro {
	public String titolo;
	public int pagine;
	private Autore[] autori = new Autore[5];

	public Libro() {
	}

	public Libro(String titolo, int pagine) {// era da aggiungere esempio: this.setNome(nome)
		this.setTitolo(titolo);
		this.setPagine(pagine);
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public int getPagine() {
		return pagine;
	}

	public void setPagine(int pagine) {
		this.pagine = pagine;
	}

	public Autore[] getAutori() {
		return autori;
	}

	public void setAutori(Autore[] autori) {
		this.autori = autori;
	}

	public void addAutori(Autore insAut) {
		int i = 0;
		while (i < autori.length && autori[i] != null) {
			i++;
		}
		if (i < autori.length) {
			this.autori[i] = insAut;
		} else
			System.out.println("Errore Array");
	}

	public void stampaAutori() {
		int i = 0;
		while (i < autori.length && autori[i] != null) {
			System.out.println("\nAUTORE\n" + autori[i].toString());
			i++;
		}
		if (i > autori.length) {
			System.out.println("L'array � pieno");
		} return;
	}

	@Override
	public String toString() {
		return "Titolo: " + this.titolo + "\nPagine: " + this.pagine;
	}

}
