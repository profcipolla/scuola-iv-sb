
public class Contatore {

	private int c;
	
	public Contatore(int c) {
		this.c = c;
	}
	
	public synchronized void scrivi0() {
		c = 0;
	}
	
	public synchronized void scrivi1() {
		c = 1;
	}

	public int getContatore() {
		return c;
	}
}
