
public class mioThread extends Thread {
	
	private Contatore contatore;
	private int tipo;
	
	public mioThread(Contatore contatore, int tipo) {
		this.contatore = contatore;
		this.tipo = tipo;
	}
	
	public void run() {
		
		for (int i = 0; i < 100; i++) {
			if (tipo == 0) {
				contatore.scrivi0();
				System.out.print(contatore.getContatore() + " ");
			} else {
				contatore.scrivi1();
				System.out.print(contatore.getContatore() + " ");
			}
		}
		
		try {
			Thread.sleep((long)(Math.random() * 1000));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
} 
