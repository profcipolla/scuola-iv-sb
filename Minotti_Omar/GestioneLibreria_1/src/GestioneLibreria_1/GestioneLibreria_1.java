package GestioneLibreria_1;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

public class GestioneLibreria_1 {

	private JFrame JFrame;
	private JTextField textNomeLibro;
	private JTextField textIdLibro;
	
	Libro libro = new Libro();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestioneLibreria_1 window = new GestioneLibreria_1();
					window.JFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GestioneLibreria_1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		JFrame = new JFrame();
		JFrame.setBackground(new Color(240, 240, 240));
		JFrame.setTitle("Gestione Libreria");
		JFrame.setBounds(100, 100, 546, 429);
		JFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JFrame.getContentPane().setLayout(null);
		
		JPanel JPanel1 = new JPanel();
		JPanel1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		JPanel1.setBounds(8, 10, 514, 130);
		JFrame.getContentPane().add(JPanel1);
		
		JLabel nomeLibro = new JLabel("Nome Libro:");
		
		textNomeLibro = new JTextField();
		//textNomeLibro.addActionListener(this);
		textNomeLibro.setColumns(10);
		
		JLabel idLibro = new JLabel("ID Libro:");
		
		textIdLibro = new JTextField();
		textIdLibro.setColumns(10);
		
		JLabel genereLibro = new JLabel("Genere Libro:");

		
		JComboBox<String> tendinaGeneri = new JComboBox<String>();
		GroupLayout gl_JPanel1 = new GroupLayout(JPanel1);
		gl_JPanel1.setHorizontalGroup(
			gl_JPanel1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_JPanel1.createSequentialGroup()
					.addGap(30)
					.addGroup(gl_JPanel1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_JPanel1.createParallelGroup(Alignment.LEADING, false)
							.addComponent(nomeLibro, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(idLibro, GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE))
						.addComponent(genereLibro, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(gl_JPanel1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_JPanel1.createParallelGroup(Alignment.LEADING, false)
							.addComponent(textIdLibro, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)
							.addComponent(textNomeLibro, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
						.addComponent(tendinaGeneri, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE))
					.addGap(15))
		);
		gl_JPanel1.setVerticalGroup(
			gl_JPanel1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_JPanel1.createSequentialGroup()
					.addGap(19)
					.addGroup(gl_JPanel1.createParallelGroup(Alignment.BASELINE)
						.addComponent(nomeLibro)
						.addComponent(textNomeLibro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(8)
					.addGroup(gl_JPanel1.createParallelGroup(Alignment.BASELINE)
						.addComponent(idLibro)
						.addComponent(textIdLibro, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_JPanel1.createParallelGroup(Alignment.BASELINE)
						.addComponent(genereLibro)
						.addComponent(tendinaGeneri, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(24, Short.MAX_VALUE))
		);
		JPanel1.setLayout(gl_JPanel1);
		
		JPanel JPanel2 = new JPanel();
		JPanel2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		JPanel2.setBounds(8, 150, 514, 73);
		JFrame.getContentPane().add(JPanel2);
		
		JButton addButton = new JButton("AGGIUNGI");
		//addButton.addActionListener(this);
		
		JButton delButton = new JButton("ELIMINA");
		
		JButton searchButton = new JButton("CERCA");
		GroupLayout gl_JPanel2 = new GroupLayout(JPanel2);
		gl_JPanel2.setHorizontalGroup(
			gl_JPanel2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_JPanel2.createSequentialGroup()
					.addGap(39)
					.addComponent(addButton, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
					.addGap(58)
					.addComponent(delButton, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
					.addGap(52)
					.addComponent(searchButton, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(67, Short.MAX_VALUE))
		);
		gl_JPanel2.setVerticalGroup(
			gl_JPanel2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_JPanel2.createSequentialGroup()
					.addGap(22)
					.addGroup(gl_JPanel2.createParallelGroup(Alignment.BASELINE)
						.addComponent(addButton)
						.addComponent(delButton)
						.addComponent(searchButton))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		JPanel2.setLayout(gl_JPanel2);
	}
		
	/*
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		JTextField string = (JTextField) source;
		JButton bottonePremuto = (JButton) source;
		
		if(source instanceof JButton) {
			
			if("AGGIUNGI".equals bottonePremuto) {
				
			}
		} else if (source instanceof JTextField) {
			if(source == textNomeLibro) {
				libro.setTitolo(string.getText());
			}
		}
		
	}*/
}
