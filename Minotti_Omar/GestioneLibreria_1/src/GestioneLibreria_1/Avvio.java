package GestioneLibreria_1;
import java.util.Scanner;

public class Avvio {

	public static void main(String[] args) {

		Scanner tastiera = new Scanner(System.in);

		System.out.println("Inserisci Titolo Libro: ");// String titolo,int pagine
		String titoloT = tastiera.nextLine();
		System.out.println("Inserisci Pagine Libro: ");
		int pagineT = tastiera.nextInt();
		Libro lib = new Libro(titoloT, pagineT);

		String risposta;
		Menu();
		risposta = tastiera.next();
		do {
			if ("S".equals(risposta)) {
				System.out.println("Inserisci Nome Autore: ");// String nome, String cognome, String nazionalita
				String nomeT = tastiera.next();
				System.out.println("Inserisci Cognome Autore: ");
				String cognomeT = tastiera.next();
				System.out.println("Inserisci Nazionalita' Autore: ");
				String nazionalitaT = tastiera.next();
				lib.addAutori(new Autore(nomeT, cognomeT, nazionalitaT));
				Menu();
				risposta = tastiera.next();
			} 

		} while (!"N".equals(risposta));

		tastiera.close();
		System.out.println("== LIBRO ==\n" + lib.toString());
		lib.stampaAutori();
	}

	public static void Menu() {
		System.out.println("Vuoi aggiungere un Autore? S - N");
	}
}
