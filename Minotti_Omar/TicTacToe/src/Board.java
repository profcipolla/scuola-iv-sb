import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Board extends JFrame {
	JLabel titolo;
	Font font = new Font("", Font.BOLD, 20);
	JPanel mainPanel;
	JButton []btns = new JButton[9];
	
	Board() {
		setTitle("Esercizio Minotti Omar");
		setSize(450,450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		setLayout(new BorderLayout());
		titolo = new JLabel("Tic Tac Toe");
		titolo.setFont(font);
		titolo.setHorizontalAlignment(SwingConstants.CENTER);
		add(titolo, BorderLayout.NORTH);
		
		mainPanel = new JPanel();
		
		mainPanel.setLayout(new GridLayout(3,3));
		for (int i = 1; i <= 9; i++) {
			JButton btn = new JButton();
			try {
				ImageIcon foto = new ImageIcon("src/img/O.png"); 
				btn.setIcon(foto);
			} catch(Exception e) {
				System.out.println(e.getMessage());
			}
			btn.setFont(font);
			mainPanel.add(btn);
		}
		this.add(mainPanel,BorderLayout.CENTER);
	}
}
