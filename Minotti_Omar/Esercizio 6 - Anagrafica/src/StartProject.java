
public class StartProject {
	
	public static void main(String[] a) throws Exception {
		Persona persona = new Persona();
		persona.setNome("Omar");
		persona.setCognome("Minotti");
		try {
			persona.setCodiceFiscale(null);
		} catch(Exception exc) {
			try {
				persona.setCodiceFiscale("MBI343U");
			} catch(Exception e) {
				persona.setCodiceFiscale("CRF86F5F6D7D8FJ4");
			}
		}
		
		System.out.println(persona);
	}
}
