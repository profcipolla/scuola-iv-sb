
public class Persona {
	private String nome;
	private String cognome;
	private String codiceFiscale;
	
	public String getNome() {
		return this.nome;
	}
	
	public String getCognome() {
		return this.cognome;
	}
	
	public String getCodiceFiscale() {
		return this.codiceFiscale;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public void setCodiceFiscale(String codiceFiscale) throws Exception{
		if(null == codiceFiscale) {
			throw new Exception("Fornire un CF");
		}
		if(16 != codiceFiscale.length()) {
			throw new Exception("Lunghezza del CF non valida");
		}
		this.codiceFiscale = codiceFiscale;
	}
}
