
public class Avvio {
	public static void main(String[] a) {
		int[] array = new int[] {88, 7, 14, 120, 400, 723, 8, 1, 4, 76};
		
		System.out.println("Array casuale: ");
		for(int i = 0; i < array.length; i++) {
			System.out.println(array[i] + "\t");
		}
		
		quickSort(array);
		System.out.println("\nQuick Sort: ");
		for(int i = 0; i < array.length; i++) {
		System.out.println(array[i] + "\t");
		}
	}
	
	public static void quickSort(int[] arr) {
		partizionamento(arr, 0, arr.length - 1);
	}
	
	private static void partizionamento(int[] arr, int daIndice, int aIndice) {
		
		if(daIndice >= aIndice) {
			return;
		}
		
		int pivot = arr[daIndice];
		int inf = daIndice + 1;
		int sup = aIndice;
		
		while(inf <= sup) {
			while(arr[sup] > pivot) {
				sup--;
			}
			
			while(inf <= sup && arr[inf] <= pivot) {
				inf++;
			}
			
			if(inf < sup) {
				int appo = arr[inf];
				arr[inf] = arr[sup];
				arr[sup] = appo; 
				sup--;
				inf++;
			}
		}
		
		//scambiare il pivot con la posizione centrale
		if(daIndice != sup) {
			arr[daIndice] = arr[sup];
			arr[sup] = pivot;
		}
		partizionamento(arr, daIndice, sup - 1);
		partizionamento(arr, sup + 1, aIndice);
	}
}
