import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Panel;

import javax.swing.JFrame;

public class Campo extends Panel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paint(Graphics g) {
		super.paintComponents(g);
		this.setBackground(Color.GREEN);
		
		Graphics2D campo2d = (Graphics2D) g;
		
		campo2d.setColor(Color.WHITE);
		
		g.drawRect(10, 10, 315, 440);
		
		g.drawLine(10, 220, 325, 220);
		
		g.drawOval(140, 190, 60, 60);
		
		g.drawRect(90, 10, 162, 70);
		g.drawRect(130, 10, 81, 35);
		g.drawRect(90, 380, 162, 70);
		g.drawRect(130, 415, 81, 35);
		
		g.drawString("Cerchi Olimpici", 130, 270);
	}
	
	public static void main(String[] args) {
		JFrame f = new JFrame("Campo da Calcio");
		Campo c = new Campo();
		f.setBounds(0, 0, 350, 500);
		f.add(c);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);
	}
}
