package programmaApp;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

public class FinestraConcessionaria extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MovimentiConcessionaria mC = new MovimentiConcessionaria();
	CasaAutomobilistica casaA = new CasaAutomobilistica();
	private float prezzo = 0.0f;
	private Integer posti = 0;
	private String colore = "";
	private Float cc = 0.0f;
	private Integer cv = 0;
	private String marchio = "";
	
	private JTextField tNumeroPosti;
	private JTextField tColoreAuto;
	private JTextField tCcAuto;
	private JTextField tCvAuto;
	private JTextField tCasaCostruttrice;
	private JTextField tNazione;
	private JTextField tPrezzo;
	private JTextField tAnno;

	public FinestraConcessionaria() {
		setTitle("Gestione Concessionaria");
		setSize(569, 400);
		setLocation(700, 700);
		getContentPane().setLayout(new GridLayout(3, 1, 0, 0));

		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("EditorPane.disabledBackground"));
		getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel titoloLable = new JLabel("Concessionaria Autoveicolo");
		titoloLable.setHorizontalAlignment(SwingConstants.CENTER);
		titoloLable.setForeground(Color.BLUE);
		titoloLable.setBackground(Color.BLACK);
		titoloLable.setFont(new Font("Eras Demi ITC", Font.BOLD, 27));
		panel.add(titoloLable);

		JPanel pAuto = new JPanel();
		pAuto.setBackground(Color.ORANGE);
		getContentPane().add(pAuto);
		pAuto.setLayout(new GridLayout(4, 2, 0, 0));

		JLabel lNumeroPosti = new JLabel("Numero Posti");
		lNumeroPosti.setFont(new Font("Tahoma", Font.BOLD, 12));
		lNumeroPosti.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lNumeroPosti);

		tNumeroPosti = new JTextField();
		tNumeroPosti.setHorizontalAlignment(SwingConstants.CENTER);
		tNumeroPosti.setColumns(10);
		pAuto.add(tNumeroPosti);
		tNumeroPosti.addActionListener(this);

		JLabel lColoreAuto = new JLabel("Colore Auto");
		lColoreAuto.setFont(new Font("Tahoma", Font.BOLD, 12));
		lColoreAuto.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lColoreAuto);

		tColoreAuto = new JTextField();
		tColoreAuto.setHorizontalAlignment(SwingConstants.CENTER);
		tColoreAuto.setColumns(10);
		pAuto.add(tColoreAuto);
		tColoreAuto.addActionListener(this);

		JLabel lCcAuto = new JLabel("Cc");
		lCcAuto.setFont(new Font("Tahoma", Font.BOLD, 12));
		lCcAuto.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lCcAuto);

		tCcAuto = new JTextField();
		tCcAuto.setHorizontalAlignment(SwingConstants.CENTER);
		tCcAuto.setColumns(10);
		pAuto.add(tCcAuto);
		tCcAuto.addActionListener(this);

		JLabel lCvAuto = new JLabel("Cv");
		lCvAuto.setFont(new Font("Tahoma", Font.BOLD, 12));
		lCvAuto.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lCvAuto);

		tCvAuto = new JTextField();
		tCvAuto.setHorizontalAlignment(SwingConstants.CENTER);
		tCvAuto.setColumns(10);
		pAuto.add(tCvAuto);
		tCvAuto.addActionListener(this);

		JLabel lCasaCostruttrice = new JLabel("Casa Costruttrice");
		lCasaCostruttrice.setFont(new Font("Tahoma", Font.BOLD, 12));
		lCasaCostruttrice.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lCasaCostruttrice);

		tCasaCostruttrice = new JTextField();
		tCasaCostruttrice.setHorizontalAlignment(SwingConstants.CENTER);
		tCasaCostruttrice.setColumns(10);
		pAuto.add(tCasaCostruttrice);
		tCasaCostruttrice.addActionListener(this);

		JLabel lNazione = new JLabel("Nazione");
		lNazione.setFont(new Font("Tahoma", Font.BOLD, 12));
		lNazione.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lNazione);

		tNazione = new JTextField();
		tNazione.setHorizontalAlignment(SwingConstants.CENTER);
		tNazione.setColumns(10);
		pAuto.add(tNazione);
		tNazione.addActionListener(this);

		JLabel lPrezzo = new JLabel("Prezzo");
		lPrezzo.setFont(new Font("Tahoma", Font.BOLD, 12));
		lPrezzo.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lPrezzo);

		tPrezzo = new JTextField();
		tPrezzo.setHorizontalAlignment(SwingConstants.CENTER);
		tPrezzo.setColumns(10);
		pAuto.add(tPrezzo);
		
		JLabel lAnno = new JLabel("Anno Produzione");
		lAnno.setFont(new Font("Tahoma", Font.BOLD, 12));
		lAnno.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lAnno);
		
		tAnno = new JTextField();
		tAnno.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(tAnno);
		tAnno.setColumns(10);
		tAnno.addActionListener(this);
		tPrezzo.addActionListener(this);

		JButton bSalva = new JButton("Salva");
		bSalva.setForeground(Color.BLUE);
		bSalva.setFont(new Font("MS UI Gothic", Font.BOLD, 23));
		bSalva.setBackground(Color.LIGHT_GRAY);
		getContentPane().add(bSalva);
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		bSalva.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent in) {

		Object source = in.getSource();
		
		if (source instanceof JTextField) {
			JTextField testoDisplay = (JTextField) source;
			if (source == tCasaCostruttrice) {
				marchio = testoDisplay.getText();
				tCasaCostruttrice.setForeground(Color.GREEN);
			} else if (source == tPrezzo) {
				prezzo = Float.parseFloat(testoDisplay.getText());
				tPrezzo.setForeground(Color.GREEN);
			} else if (source == tNumeroPosti) {
				posti = Integer.parseInt(testoDisplay.getText());
				tNumeroPosti.setForeground(Color.GREEN);
			} else if (source == tColoreAuto) {
				colore = testoDisplay.getText();
				tColoreAuto.setForeground(Color.GREEN);
			} else if (source == tNazione) {
				casaA.setNazione(testoDisplay.getText());
				tNazione.setForeground(Color.GREEN);
			} else if (source == tAnno) {
				casaA.setAnnoProduzione(Integer.parseInt(testoDisplay.getText()));
				tAnno.setForeground(Color.GREEN);
			} else if (source == tCcAuto) {
				cc = Float.parseFloat(testoDisplay.getText());
				tCcAuto.setForeground(Color.GREEN);
			} else if (source == tCvAuto) {
				cv = Integer.parseInt(testoDisplay.getText());
				tCvAuto.setForeground(Color.GREEN);
			}
		}
		else if (source instanceof JButton) {
			mC.addAuto(new Auto(prezzo, posti, colore, cc, cv, marchio));
			tCasaCostruttrice.setForeground(Color.BLACK);
			tPrezzo.setForeground(Color.BLACK);
			tNumeroPosti.setForeground(Color.BLACK);
			tColoreAuto.setForeground(Color.BLACK);
			tNazione.setForeground(Color.BLACK);
			tAnno.setForeground(Color.BLACK);
			tCcAuto.setForeground(Color.BLACK);
			tCvAuto.setForeground(Color.BLACK);
			mC.addCasa(new CasaAutomobilistica(casaA.getAnnoProduzione(),casaA.getNazione()));
			mC.faiTutto();
			mC.StampaArrayAuto();
		}

	}
}
