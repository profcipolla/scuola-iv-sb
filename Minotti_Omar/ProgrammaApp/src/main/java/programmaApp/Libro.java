package programmaApp;

public class Libro {
	private String titolo;
	private int pagine;

	public Libro(String titolo, int pagine) {// era da aggiungere esempio: this.setNome(nome)
		this.setTitolo(titolo);
		this.setPagine(pagine);
	}

	public Libro() {
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public int getPagine() {
		return pagine;
	}

	public void setPagine(int pagine) {
		this.pagine = pagine;
	}

	@Override
	public String toString() {
		return "Titolo: " + this.titolo + "\nPagine: " + this.pagine;
	}

}
