package programmaApp;

public interface Stampabile {
	void print();
	int numeroCopie();
}
