package programmaApp;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalcolatriceWin extends JFrame implements ActionListener {
	private static final String[] scritte = { "C", "CE", "RM", "√", "7", "8", "9", "/", "4", "5", "6", "*", "1", "2",
			"3", "+", ",", "0", "=", "-" };

	private static final String[] comandi = { "C", "CE", "RM", "OFF", "ON" };
	private String operazioneMem = "";
	private Double operando = 0.0d;

	private JButton[] pulsanti = new JButton[20];
	private JTextField display = new JTextField(20);
	
	Font mioFont = new Font("Verdana", Font.BOLD, 20);

	public CalcolatriceWin(String titolo) {
		setBackground(Color.orange);

		setLayout(new GridLayout(2, 1));
		display.setFont(mioFont);
		add(display);
		display.setAlignmentX(RIGHT_ALIGNMENT);

		JPanel pnlPulsanti = new JPanel();
		add(pnlPulsanti);

		// CalcolatriceListener mioListener = new CalcolatriceListener(this.display);
		pnlPulsanti.setLayout(new GridLayout(5, 4));
		for (int i = 0; i < scritte.length; i++) {
			pulsanti[i] = new JButton(scritte[i]);
			pnlPulsanti.add(pulsanti[i]);
			pulsanti[i].setFont(mioFont);

			// pulsanti[i].addActionListener(mioListener);
			pulsanti[i].addActionListener(this);
		}

		setTitle(titolo);
		setSize(335, 535);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	// TODO:
	// possibilitï¿½ di inserire un solo separatore decimale
	// sul display cifre e separatore
	// il + mette da parte il valore nel display, ripulisce il display
	// = se sono disponibili due valori mi darï¿½ la somma
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source instanceof JButton) {
			String contenutoDisplay = display.getText();
			JButton bottonePremuto = (JButton) source;
			String pulsanteText = bottonePremuto.getText();
			switch (pulsanteText) {
			case "0":
			case "1":
			case "2":
			case "3":
			case "4":
			case "5":
			case "6":
			case "7":
			case "8":
			case "9":
				display.setText(contenutoDisplay + pulsanteText);
				break;
			case ",":
				// test se giÃ  presente la virgola, non deve aggiungerne un'altra
				if (!contenutoDisplay.contains(",")) {
					if ("".equals(contenutoDisplay)) {
						contenutoDisplay = "0";
					}
					display.setText(contenutoDisplay + pulsanteText);
				}
				break;
			default:
				if (Arrays.asList(comandi).contains(pulsanteText)) {
					this.gestisciComandi(pulsanteText);
				} else {
					this.gestisciOperazione(pulsanteText, contenutoDisplay);
				}
			}

		} else if (source instanceof JTextField) {
			JTextField text = (JTextField) source;
			System.out.println("Hai premuto un pulsante " + text.getText());
		}

	}

	private void gestisciComandi(String operazione) {
		switch(operazione) {
		case "C":
			this.display.setText("");
			this.operazioneMem = "";
			this.operando = 0.0d;
			break;
		case "CE":
				String string = display.getText();
				display.setText("");
				for(int i = 0; i < string.length() - 1; i ++) {
					display.setText(display.getText() + string.charAt(i));
				}
			break;
		case "RM":
			break;
		case "OFF":
			System.exit(0);
			break;
		}
		

	}
	
	private void gestisciOperazione(String operazione, String contenutoDisplay) {
		System.out.println("Hai richiesto l'operazione " + operazione);
		switch (operazioneMem) {
		case "":
			// mette da parte il primo operando
			if (!"".equals(contenutoDisplay)) {
				this.operando = Double.parseDouble(contenutoDisplay.replace(",", "."));
				this.display.setText("");
			}
			break;
		case "+":
			this.operando = this.operando + Double.parseDouble(contenutoDisplay.replace(",", "."));
			this.display.setText("");
			break;
		case "-":
			this.operando = this.operando - Double.parseDouble(contenutoDisplay.replace(",", "."));
			this.display.setText("");
			break;
		case "/":
			double secondoOperando = Double.parseDouble(contenutoDisplay.replace(",", "."));
			if (secondoOperando > Double.MIN_VALUE) {
				this.operando = this.operando / secondoOperando;
				this.display.setText("");
			} else if (0 - secondoOperando < Double.MIN_VALUE) {
				this.display.setText("ERRORE");
				this.operazioneMem = "";
				this.operando = 0.0d;
				return;
			}
			break;
		case "*":
			this.operando = this.operando * Double.parseDouble(contenutoDisplay.replace(",", "."));
			this.display.setText("");
			break;
		}

		if ("=".equals(operazione)) {
			this.display.setText(String.format("%.2f", this.operando));
			operazioneMem = "";
		} else {
			operazioneMem = operazione;
		}
	}
}
