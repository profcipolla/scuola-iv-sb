package programmaApp;

public class OperazioniLibreria {
	private Autore[] autori = new Autore[5];
	private Libro[] libri = new Libro[5];

	public void addLibro(Libro insLibri) {
		int i = 0;
		while (i < libri.length && libri[i] != null) {
			i++;
		}
		if (i < libri.length) {
			this.libri[i] = insLibri;
		}
	}

	public void stampaLibri() {
		int i = 0;
		while (i < libri.length && libri[i] != null) {
			System.out.println("\nLIBRI\n" + libri[i].toString());
			i++;
		}
		if (i > libri.length) {
			System.out.println("L'array è pieno");
		}
		return;
	}

	public void addAutori(Autore insAut) {
		int i = 0;
		while (i < autori.length && autori[i] != null) {
			i++;
		}
		if (i < autori.length) {
			this.autori[i] = insAut;
		} else
			System.out.println("Errore Array");
	}

	public void stampaAutori() {
		int i = 0;
		while (i < autori.length && autori[i] != null) {
			System.out.println("\nAUTORE\n" + autori[i].toString());
			i++;
		}
		if (i > autori.length) {
			System.out.println("L'array è pieno");
		}
		return;
	}

	public void controllaAutori(String CognomeAutControllo) {

		for (int i = 0; i < autori.length && autori[i] != null; i++) {
			if (autori[i].getCognome().contains(CognomeAutControllo)) {
				System.out.println("presente nel sistema");
				// ..aggiunge l'autore al libro prima dichiarato

			} else {

				// ..lancia menu Autore e salva i dati insieme}
			}

		}
		System.out.println("Aprire menu Autori");

	}
//		if (Arrays.asList(autori).contains(CognomeAutControllo)) {
//			System.out.println("presente nel sistema");
//			//..aggiunge l'autore al libro prima dichiarato
//			
//		} else {
//			System.out.println("NON presente nel sistema");
//			//..lancia menu Autore e salva i dati insieme
//		}

}
