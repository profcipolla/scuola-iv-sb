package programmaApp;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraLibro extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	Libro classeLibro = new Libro();
	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();
	JPanel panelloMain = new JPanel();
	JLabel labelMain = new JLabel("Aggiungi un libro");

	JPanel pannelloInserimentoDati = new JPanel();
	JLabel labelTitolo = new JLabel("Nome Libro:");
	JTextField textTitolo = new JTextField("");
	JLabel labelNumeroPag = new JLabel("N\u00B0 Pagine:");
	JTextField textNumeroPag = new JTextField("");
	JButton bottoneSalva = new JButton("Salva");
	Font fontA = new Font("Solomon Normal", Font.BOLD, 16);
	Font fontB = new Font("Solomon", Font.BOLD | Font.ITALIC, 11);

	public FinestraLibro() {
		getContentPane().setBackground(Color.GRAY);
		setTitle("Gestione Libri");
		setSize(400, 400);
		setLocation(700, 700);
		getContentPane().setLayout(null);
		setBackground(Color.gray);

		panelloMain.setBackground(Color.GRAY);// colore riempimento
		panelloMain.setBounds(0, 0, 384, 70);// posizione
		panelloMain.setLayout(new GridLayout(1, 1));// griglia
		labelMain.setBackground(Color.WHITE);
		labelMain.setHorizontalAlignment(SwingConstants.CENTER);// posizionamento testo
		labelMain.setFont(new Font("Dialog", Font.BOLD, 30));// testo

		pannelloInserimentoDati.setBackground(Color.LIGHT_GRAY);
		pannelloInserimentoDati.setBounds(0, 70, 384, 220);
		pannelloInserimentoDati.setLayout(new GridLayout(2, 2));

		labelTitolo.setFont(fontA);// font grande
		labelNumeroPag.setFont(fontA);

		labelTitolo.setHorizontalAlignment(SwingConstants.CENTER);// testo al centro
		labelNumeroPag.setHorizontalAlignment(SwingConstants.CENTER);

		textTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		textNumeroPag.setHorizontalAlignment(SwingConstants.CENTER);
		
		textTitolo.setBackground(Color.WHITE);
		textNumeroPag.setBackground(Color.WHITE);

		labelTitolo.setForeground(Color.BLACK);// colore testo
		labelNumeroPag.setForeground(Color.BLACK);

		textTitolo.setFont(new Font("Calibri", Font.BOLD, 16));// font piccolo
		textNumeroPag.setFont(new Font("Calibri", Font.BOLD, 16));

		bottoneSalva.setFont(fontA);
		bottoneSalva.setBackground(Color.GRAY);
		bottoneSalva.setBounds(134, 308, 112, 42);

		panelloMain.add(labelMain);// aggiunto Scritta al pannello Titolo
		pannelloInserimentoDati.add(labelTitolo);// layer titolo libro
		pannelloInserimentoDati.add(textTitolo);// testo
		pannelloInserimentoDati.add(labelNumeroPag);// layer pagine
		pannelloInserimentoDati.add(textNumeroPag);

		getContentPane().add(bottoneSalva);
		getContentPane().add(pannelloInserimentoDati);
		getContentPane().add(panelloMain);

		textTitolo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				System.out.println(((JTextField)e.getSource()).getText());
				System.out.println(tastiera.getText());
				classeLibro.setTitolo(tastiera.getText());
				textTitolo.setForeground(new Color(0,168,107));
				textTitolo.setFont(fontA);
			}

		});

		textNumeroPag.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				Integer pagine = Integer.parseInt(tastiera.getText());
				classeLibro.setPagine(pagine);
				textNumeroPag.setForeground(new Color(0,168,107));
				textNumeroPag.setFont(fontA);

			}
		});
		bottoneSalva.addActionListener(this);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!textTitolo.getText().equals("")) {
		textTitolo.setText("");
		textNumeroPag.setText("");
		textTitolo.setForeground(Color.black);
		textNumeroPag.setForeground(Color.black);
		operazioni.addLibro(new Libro(classeLibro.getTitolo(),classeLibro.getPagine()));
		operazioni.stampaLibri();
		} 
	}

}
