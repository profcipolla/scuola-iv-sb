package programmaApp;

public class Moto extends Veicolo {
	
	private String coloreMoto;
	private Float CcMoto;
	private Integer CvMoto;
	
	public Moto (String coloreMoto, Float CcMoto, Integer CvMoto,Float Cc, Integer Cv) {
		super(Cc,Cv);
		this.coloreMoto=coloreMoto;
		this.CcMoto=CcMoto;
		this.CvMoto=CvMoto;
		
	}

	public String getColoreMoto() {
		return coloreMoto;
	}

	public void setColoreMoto(String coloreMoto) {
		this.coloreMoto = coloreMoto;
	}

	public Float getCcMoto() {
		return CcMoto;
	}

	public void setCcMoto(Float ccMoto) {
		CcMoto = ccMoto;
	}

	public Integer getCvMoto() {
		return CvMoto;
	}

	public void setCvMoto(Integer cvMoto) {
		CvMoto = cvMoto;
	}
	
	

}
