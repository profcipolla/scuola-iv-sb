package programmaApp;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

public class FinestraAutore extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();
	JPanel panelloMain = new JPanel();
	JLabel labelMain = new JLabel("Aggiungi un autore");

	JPanel pannelloInserimentoDati = new JPanel();
	JLabel labelNome = new JLabel("Nome");
	JTextField textNome = new JTextField("");
	JLabel labelCognome = new JLabel("Cognome");
	JTextField textCognome = new JTextField("");
	JLabel labelNazionalita = new JLabel("Nazionalit\u00E0");
	JTextField textNazionalita = new JTextField("");
	JButton bottoneSalva = new JButton("SALVA");
	Font fontA = new Font("Solomon Normal", Font.BOLD, 16);
	Font fontB = new Font("Solomon", Font.BOLD | Font.ITALIC, 11);

	public FinestraAutore() {
		getContentPane().setBackground(Color.GRAY);
		setTitle("Gestione Autori");
		setSize(400, 400);
		setLocation(700, 700);
		getContentPane().setLayout(null);
		setBackground(Color.gray);

		panelloMain.setBackground(Color.GRAY);// colore riempimento
		panelloMain.setBounds(0, 0, 384, 70);// posizione
		panelloMain.setLayout(new GridLayout(1, 1));// griglia
		labelMain.setBackground(new Color(240, 240, 240));
		labelMain.setHorizontalAlignment(SwingConstants.CENTER);// posizionamento testo
		labelMain.setFont(new Font("Calibri", Font.BOLD, 30));// testo

		pannelloInserimentoDati.setBackground(Color.LIGHT_GRAY);
		pannelloInserimentoDati.setBounds(0, 70, 384, 220);
		pannelloInserimentoDati.setLayout(new GridLayout(3, 2));
		labelNome.setBackground(Color.LIGHT_GRAY);

		labelNome.setFont(new Font("Calibri", Font.BOLD, 20));// font grande
		labelCognome.setBackground(UIManager.getColor("Button.light"));
		labelCognome.setFont(new Font("Calibri", Font.BOLD, 20));
		labelNazionalita.setBackground(Color.WHITE);
		labelNazionalita.setFont(new Font("Calibri", Font.BOLD, 20));

		labelNome.setHorizontalAlignment(SwingConstants.CENTER);// testo al centro
		labelCognome.setHorizontalAlignment(SwingConstants.CENTER);
		labelNazionalita.setHorizontalAlignment(SwingConstants.CENTER);
		textNome.setToolTipText("");

		textNome.setHorizontalAlignment(SwingConstants.CENTER);
		textCognome.setHorizontalAlignment(SwingConstants.CENTER);
		textNazionalita.setHorizontalAlignment(SwingConstants.CENTER);
		
		textNome.setBackground(Color.WHITE);
		textCognome.setBackground(Color.WHITE);
		textNazionalita.setBackground(Color.WHITE);

		labelNome.setForeground(Color.BLACK);// colore testo
		labelCognome.setForeground(Color.BLACK);
		labelNazionalita.setForeground(Color.BLACK);

		textNome.setFont(new Font("Calibri", Font.BOLD, 16));// font piccolo
		textCognome.setFont(new Font("Calibri", Font.BOLD, 16));
		textNazionalita.setFont(new Font("Calibri", Font.BOLD, 16));

		bottoneSalva.setFont(new Font("Calibri", Font.BOLD, 20));
		bottoneSalva.setBackground(Color.WHITE);
		bottoneSalva.setBounds(134, 308, 112, 42);

		panelloMain.add(labelMain);// aggiunto Scritta al pannello Titolo
		pannelloInserimentoDati.add(labelNome);// layer titolo libro
		pannelloInserimentoDati.add(textNome);// testo
		pannelloInserimentoDati.add(labelCognome);// layer pagine
		pannelloInserimentoDati.add(textCognome);// testo pagine
		pannelloInserimentoDati.add(labelNazionalita);// layer autore
		pannelloInserimentoDati.add(textNazionalita);// testo autore

		getContentPane().add(bottoneSalva);
		getContentPane().add(pannelloInserimentoDati);
		getContentPane().add(panelloMain);

		textNome.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				//System.out.println(((JTextField)e.getSource()).getText());
				//System.out.println(tastiera.getText());
				classeAutore.setNome(tastiera.getText());
				textNome.setForeground(new Color(0,168,107));
				textNome.setFont(fontA);
			}

		});

		textCognome.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				classeAutore.setCognome(tastiera.getText());
				textCognome.setForeground(new Color(0,168,107));
				textCognome.setFont(fontA);

			}
		});
		textNazionalita.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				classeAutore.setNazionalita(tastiera.getText());
				textNazionalita.setForeground(new Color(0,168,107));
				textNazionalita.setFont(fontA);

			}
		});
		bottoneSalva.addActionListener(this);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!textNome.getText().equals("")) {
		textNome.setText("");
		textCognome.setText("");
		textNazionalita.setText("");
		textNome.setForeground(Color.black);
		textCognome.setForeground(Color.black);
		textNazionalita.setForeground(Color.black);
		operazioni.addAutori(new Autore(classeAutore.getNome(),classeAutore.getCognome(),classeAutore.getNazionalita()));
		operazioni.stampaAutori();
		} 
	}

}
