package programmaApp;

public class CasaAutomobilistica {
	private Integer annoProduzione;
	private String nazione;
	
	public CasaAutomobilistica() {}
	public CasaAutomobilistica(Integer annoProduzione,String nazione) {
		this.setAnnoProduzione(annoProduzione);
		this.setNazione(nazione);
	}
	public Integer getAnnoProduzione() {
		return annoProduzione;
	}
	public void setAnnoProduzione(Integer annoProduzione) {
		this.annoProduzione = annoProduzione;
	}
	public String getNazione() {
		return nazione;
	}
	public void setNazione(String nazione) {
		this.nazione = nazione;
	}

}
