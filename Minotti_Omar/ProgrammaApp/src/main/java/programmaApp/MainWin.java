package programmaApp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainWin extends JFrame implements ActionListener {
	public JMenuItem Calcolatrice = new JMenuItem("Calcolatrice");
	
	public MainWin() {
		setTitle("Finestra Principale");
		setSize(1000, 800);
			
		JMenuBar mBar = new JMenuBar();
		setJMenuBar(mBar);
		
		JMenu mFile = new JMenu("File");
		JMenuItem iApriFile = new JMenuItem("Apri File..");
		JMenuItem iExit = new JMenuItem("Esci");
		mFile.add(iApriFile);
		mFile.addSeparator();
		mFile.add(iExit);
		
		

		
		JMenu mApp = new JMenu("App");
		JMenuItem iAnagrafica = new JMenuItem("Anagrafica");
		JMenuItem iCalcolatrice = new JMenuItem("Calcolatrice");
		JMenuItem iConcessionaria = new JMenuItem("Concessionaria");
		JMenuItem iContoCorrente = new JMenuItem("Conto Corrente");
		iConcessionaria.addActionListener(this);
		mApp.add(iConcessionaria);
		
		mApp.add(iAnagrafica);
		iAnagrafica.addActionListener(this);
		
		mApp.add(iCalcolatrice);
		iCalcolatrice.addActionListener(this);
		
		mApp.add(iConcessionaria);
		mApp.add(iContoCorrente);
		
		JMenu mBiblioteca = new JMenu("Biblioteca");
		mApp.add(mBiblioteca);
		JMenuItem mLibro = new JMenuItem("Inserisci Libro");
		mBiblioteca.add(mLibro);
		JMenuItem mAutore = new JMenuItem("Inserisci Autore");
		mBiblioteca.add(mAutore);
		
		mLibro.addActionListener(this);
		mAutore.addActionListener(this);
		
		mApp.addSeparator();
		JMenu mFuture = new JMenu("App Future");
		mApp.add(mFuture);
		mFuture.add(new JMenuItem("Super Mario Bross"));
		mFuture.add(new JMenuItem("Doom"));
		
		JMenu mInfo = new JMenu("Info");
		JMenuItem iAbout = new JMenuItem("About");
		mInfo.add(iAbout);
		
		mBar.add(mFile);
		mBar.add(mApp);
		mBar.add(mInfo);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	    
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem menuSelezionato = (JMenuItem) e.getSource();
		
		switch(menuSelezionato.getText()) {
		case"Calcolatrice":
			new CalcolatriceWin("Calcolatrice");
			break;
		case"Anagrafica":
			MiaFinestra f = new MiaFinestra();
			break;
		case"Inserisci Libro":
			new FinestraLibro();
			break;
		case"Inserisci Autore":
			new FinestraAutore();
		break;
		case"Concessionaria":
			new FinestraConcessionaria();
			break;
		}
	}
}
