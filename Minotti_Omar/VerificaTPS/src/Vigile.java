
public class Vigile {
	String nome;
	String cognome;
	int matricola;
	
	String veicolo;
	int numVerbale;
	String luogo;
	String data;
	int importo;
	String vigile;
	
	Vigile(String nome, String cognome, int matricola) {
		
	}
	
	public void aggContravvenzione(String veicolo, int numVerbale, String luogo, String data, int importo, String vigile) {
		this.veicolo = veicolo;
		this.numVerbale = numVerbale;
		this.luogo = luogo;
		this.data = data;
		this.importo = importo;
		this.vigile = vigile;
	}
	
	
	public String getVeicolo() {
		return veicolo;
	}

	public void setVeicolo(String veicolo) {
		this.veicolo = veicolo;
	}

	public int getNumVerbale() {
		return numVerbale;
	}

	public void setNumVerbale(int numVerbale) {
		this.numVerbale = numVerbale;
	}

	public String getLuogo() {
		return luogo;
	}

	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getImporto() {
		return importo;
	}

	public void setImporto(int importo) {
		this.importo = importo;
	}

	public String getVigile() {
		return vigile;
	}

	public void setVigile(String vigile) {
		this.vigile = vigile;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public int getMatricola() {
		return matricola;
	}
	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}
}
