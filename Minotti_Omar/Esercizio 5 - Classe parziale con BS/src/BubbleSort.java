public class BubbleSort {

	/**
	 * Il bubble sort confronta due elementi adiacenti, se non sono nell'ordine
	 * corretto li scambia. Si riporta all'inizio e ripete la procedura finch� tutto
	 * l'array risulta ordinato.
	 * 
	 * @param arrayToSort
	 */
	public void sort(int[] arrayToSort) {
		int dim = arrayToSort.length;
		int conta = 0;
		int scambio = 0;
		
		int posUltimoScambio = dim - 1;
		while(posUltimoScambio > 0) {
			// riparte ogni volta dalla prima posizione, verr� spostata in avanti
			// se effettivamente � stato fatto uno scambio
			posUltimoScambio = 0;
			for (int i = 0; i < dim - 1; i++) {
				conta++;
				if (arrayToSort[i] > arrayToSort[i + 1]) {
					scambio++;
					int appo = arrayToSort[i];
					arrayToSort[i] = arrayToSort[i + 1];
					arrayToSort[i + 1] = appo;
					posUltimoScambio = i;
				}
			}	
		}

		System.out.println("Ho eseguito " + conta + " confronti");
		System.out.println("Ho eseguito " + scambio + " scambi");
	}
	
	public void sort(Persona[] arrayToSort) {
		int dim = arrayToSort.length;
		int conta = 0;
		int scambio = 0;
		
		int posUltimoScambio = dim - 1;
		while(posUltimoScambio > 0) {
			// riparte ogni volta dalla prima posizione, verr� spostata in avanti
			// se effettivamente � stato fatto uno scambio
			posUltimoScambio = 0;
			for (int i = 0; i < dim - 1; i++) {
				conta++;
				String primaDenominazione = arrayToSort[i].getCognomeENome();
				String secondaDenominazione = arrayToSort[i + 1].getCognomeENome();
				
				if (primaDenominazione.compareToIgnoreCase(secondaDenominazione) > 0) {
					scambio++;
					Persona appo = arrayToSort[i];
					arrayToSort[i] = arrayToSort[i + 1];
					arrayToSort[i + 1] = appo;
					posUltimoScambio = i;
				}
			}	
		}

		System.out.println("Ho eseguito " + conta + " confronti");
		System.out.println("Ho eseguito " + scambio + " scambi");
	}
}