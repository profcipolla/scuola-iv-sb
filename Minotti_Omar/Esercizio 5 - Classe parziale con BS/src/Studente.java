import lombok.Data;

@Data
public class Studente extends Persona {
	public static int CONTATORE_MATRICOLA = 1;
		
	private int matricola;
	private int classe;
	private String sezione;
	private Integer[] voti = new Integer[20];
	
	
	public Studente(String cognome, String nome, int classe, String sezione) {
		this.matricola = Studente.CONTATORE_MATRICOLA++;
		this.setCognome(cognome);
		this.setNome(nome);
		this.setClasse(classe);
		this.setSezione(sezione);
	}
	
	
	public Studente(int classe, String sezione) {
		this.matricola = Studente.CONTATORE_MATRICOLA++;
		this.setClasse(classe);
		this.setSezione(sezione);
	}
	
	
	public void addVoto(int unVoto) {
		// TODO: aggiunge ai voti presenti il nuovo voto
	}
	
	public float calcolaMedia() {
		// TODO: in base ai voti calcola la media
		return 0.0f;
	}
	
	@Override
	public String getIdentificativo() {
		return "Matricola: " + matricola;
	}
}