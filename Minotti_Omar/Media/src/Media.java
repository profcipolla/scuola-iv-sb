import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Media {

	private JFrame frmMedia;
	private JTextField numero1;
	private JTextField numero2;
	private JTextField txtMedia;
	private JButton bReset;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Media window = new Media();
					window.frmMedia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Media() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMedia = new JFrame();
		frmMedia.setTitle("Media");
		frmMedia.setBounds(100, 100, 450, 400);
		frmMedia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMedia.getContentPane().setLayout(new GridLayout(0, 2, 0, 0));
		
		numero1 = new JTextField();
		numero1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				char c = e.getKeyChar();
				
				if(Character.isLetter(c)) {
					try {
					numero1.setEditable(false);
					txtMedia.setText("ERRORE: Inserire numeri");
					numero1.setText("");
					} catch (Exception err) {
						numero1.setEditable(true);
					}
				} else {
					numero1.setEditable(true);
				}
			}
		});
		numero1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		numero1.setHorizontalAlignment(SwingConstants.CENTER);
		frmMedia.getContentPane().add(numero1);
		numero1.setColumns(10);
		
		numero2 = new JTextField();
		numero2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				char c = e.getKeyChar();
				
				if(Character.isLetter(c)) {
					numero2.setEditable(false);
					txtMedia.setText("ERRORE: Inserire numeri");
					numero2.setText("");
				} else {
					numero2.setEditable(true);
				}
			}
		});
		numero2.setHorizontalAlignment(SwingConstants.CENTER);
		numero2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frmMedia.getContentPane().add(numero2);
		numero2.setColumns(10);
		
		JButton bMediaAritmetica = new JButton("Media Aritmetica");
		
		bMediaAritmetica.setFont(new Font("Tahoma", Font.BOLD, 16));
		frmMedia.getContentPane().add(bMediaAritmetica);
		
		JButton bMediaGeometrica = new JButton("Media Geometrica");
		bMediaGeometrica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(numero1.getText().trim().isEmpty() || numero2.getText().trim().isEmpty()) { 
					txtMedia.setText("ERRORE: Inserire valori");
				} else {
					Double totale;
					int fattore1 = Integer.parseInt(numero1.getText());
					int fattore2 = Integer.parseInt(numero2.getText());
				
					totale = (Double) Math.sqrt(fattore1 * fattore2);
					String totale2 = String.format("%.2f", totale);
					txtMedia.setText("La media �: " + totale2);
				}
			}
		});
		bMediaGeometrica.setFont(new Font("Tahoma", Font.BOLD, 16));
		frmMedia.getContentPane().add(bMediaGeometrica);
		
		txtMedia = new JTextField();
		txtMedia.setEditable(false);
		txtMedia.setFont(new Font("Tahoma", Font.BOLD, 16));
		txtMedia.setHorizontalAlignment(SwingConstants.CENTER);
		frmMedia.getContentPane().add(txtMedia);
		txtMedia.setColumns(10);
		
		bReset = new JButton("RESET");
		bReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				numero1.setText("");
				numero2.setText("");
				txtMedia.setText("");
			}
		});
		bReset.setFont(new Font("Tahoma", Font.BOLD, 16));
		frmMedia.getContentPane().add(bReset);
		
		bMediaAritmetica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(numero1.getText().trim().isEmpty() || numero2.getText().trim().isEmpty()) { 
					txtMedia.setText("ERRORE: Inserire valori");
				} else {
					Double totale;
					int fattore1 = Integer.parseInt(numero1.getText());
					int fattore2 = Integer.parseInt(numero2.getText());
					totale = (fattore1 + fattore2) / 2.0;
					txtMedia.setText("La media �: " + totale);
				}
		    }
		});
	}
}
