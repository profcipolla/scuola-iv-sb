import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.Component;
import java.awt.Insets;
import javax.swing.event.ChangeListener;

public class Termometro_GUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Termometro_GUI frame = new Termometro_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Termometro_GUI() {
		setTitle("Termometro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(8, 10, 418, 441);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setMargin(new Insets(30, 70, 2, 2));
		textArea.setAlignmentY(Component.TOP_ALIGNMENT);
		textArea.setAlignmentX(Component.LEFT_ALIGNMENT);
		textArea.setFont(new Font("Calibri", Font.BOLD, 55));
		textArea.setEditable(false);
		textArea.setBounds(8, 25, 252, 115);
		
		JSlider slider = new JSlider();
		textArea.setText("0 �C");
		panel.add(textArea);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				textArea.setText(slider.getValue() + " �C");
			}
		});
		slider.setFont(new Font("Calibri", Font.BOLD, 16));
		slider.setOrientation(SwingConstants.VERTICAL);
		slider.setPaintLabels(true);
		slider.setMajorTickSpacing(20);
		slider.setBounds(282, 25, 116, 374);
		slider.setMinorTickSpacing(2);
		slider.setPaintTicks(true);
		panel.add(slider);
	}
}
