
public class Avvio {

	public static void main(String[] args) {
		int[] array = new int[] {88, 7, 14, 120, 400, 723, 8, 1, 4, 76};
		System.out.println("Array casuale: ");
		for(int i = 0; i < array.length; i++) {
			System.out.println(array[i] + "\t");
		}
		partizionamento(array, 0, array.length - 1);
		System.out.println("\nQuick Sort: ");
		for(int i = 0; i < array.length; i++) {
			System.out.println(array[i] + "\t");
		}
	}
	
	public static void partizionamento(int[] arr, int daIndice, int aIndice) {
		
		if(daIndice >= aIndice) { //VA BENE ANCHE if(daIndice >= aIndice)
			return;
		}
		//si parte da un array (o un sottoinsieme di un array)
		//si sceglie un valore soglia (PIVOT)
		//l'array si divide in due sottoarray: a sinistra vanno i valori <= del PIVOT
		//a destra i valori >= del PIVOT
		
		//come si sceglie il PIVOT? Dovrebbe essere un valore dell'array scelto
		//in modo tale che i due sottoarray siano pi� o meno della stessa dimensione			
		//Scegliere un PIVOT fatto bene � troppo costoso, quindi di solito si ceglie un valore
		//random tra gli elementi dell'array.
		//Noi per semplicit� prenderemo il primo valore.
		
		//il partizionamento si realizza con due indici: inf parte dal basso (inf = 0)
		//sup parte dall'alto (DIM - 1).
		int pivot = arr[daIndice];
		int inf = daIndice + 1;
		int sup = aIndice;
		
		//faccio crescere inf mentre ci sono valori pi� piccoli (<=) del PIVOT
		//while ... inf++;
		//su sup faccio il ragionamento inverso, se ci sono valori pi� grandi del PIVOT vado indietro
		//while (trovo valori maggiori) fai sup--;
		while(sup >= inf) {
			while(sup >= inf && arr[sup] >= pivot) {
				sup--;
			}
			while(inf <= sup && arr[inf] <= pivot) {
				inf++;
			}
			if(inf < sup) {
				int appo = arr[sup];
				arr[sup] = arr[inf];
				arr[inf] = appo;
				inf++;
				sup--;
			}
		}
		
		//quando inf e sup si invertono, porta il PIVOT nella posizione centrale
		//ripetere partiziona sull'array a sinistra (chiamata ricorsiva)
		//ripetere partiziona sull'array a destra (chiamata ricorsiva)
		arr[0] = arr[sup];
		arr[sup] = pivot;
	}
}
