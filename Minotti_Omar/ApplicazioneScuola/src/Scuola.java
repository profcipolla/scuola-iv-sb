import java.util.ArrayList;

public class Scuola {
	private String nome;
	private String circolo;
	
	private ArrayList<Maestro> maestri = new ArrayList();
	
	public void addMaestro(Maestro maestro) {
		maestri.add(maestro);
	}
	public void cancMaestro(Maestro maestro) {
		maestri.removeIf(nome -> nome.equals(maestro));
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCircolo() {
		return circolo;
	}
	public void setCircolo(String circolo) {
		this.circolo = circolo;
	}
}
