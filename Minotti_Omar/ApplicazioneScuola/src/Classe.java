import java.util.ArrayList;

public class Classe {
	
	private char lettera;
	private int numAllievi;
	private ArrayList<Allievo> listaAllievi;
	private int conta;

	
	public Classe(char lettera, int allievi) {
		this.lettera = lettera;
		this.numAllievi = numAllievi;
		this.listaAllievi = new ArrayList<Allievo>();
	}
	
	public boolean addAllievo(Allievo allievo) {
		if(allievo == null || listaAllievi.contains(allievo))
			return false;
		listaAllievi.add(allievo);
		conta++;
		return true;
	}
	public void cancAllievo(Allievo allievo) {
		listaAllievi.removeIf(nome -> nome.equals(allievo));
		conta--;
	}
	
	public int getNumAllievi(char lettera) {
		int conta = 0;
		
		for(Allievo allievo: listaAllievi)
			conta = conta++;
		return conta;
	}
	
	public char getLettera() {
		return lettera;
	}
	public void setLettera(char lettera) {
		this.lettera = lettera;
	}
	public int getAllievi() {
		return numAllievi;
	}
	public void setAllievi(int allievi) {
		this.numAllievi = allievi;
	}
	
	
}
