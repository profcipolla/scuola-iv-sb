import java.awt.List;
import java.util.ArrayList;

public class Allievo {
	private String nome;
	private int et�;
	private String numGenitore;
	public String getNome() {
		return nome;
	}
	
	public Allievo(String nome, int et�, String numGenitore) {
		this.nome = nome;
		this.et� = et�;
		this.numGenitore = numGenitore;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getEt�() {
		return et�;
	}
	public void setEt�(int et�) {
		this.et� = et�;
	}
	public String getNumGenitore() {
		return numGenitore;
	}
	public void setNumGenitore(String numGenitore) {
		this.numGenitore = numGenitore;
	}
	
	
}
