package it.armellini.ivsb.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import it.armellini.ivsb.model.Persona;

public class FileService {
	private final String NOME_FILE = "/tmp/rubrica.txt";

	public List<Persona> persone(String idClasse) throws FileNotFoundException, IOException {
		List<Persona> listaPersone = new ArrayList<Persona>();

		// apro il file
		try (BufferedReader fbr = new BufferedReader(new FileReader(NOME_FILE))) {
			// ciclo riga per riga, e creo la persona da inserire nella lista
			String riga = fbr.readLine();
			while (null != riga) {
				Persona persona = new Persona(riga);
				if (StringUtils.isAllEmpty(idClasse) || idClasse.equals(persona.getClasse())) {
					listaPersone.add(persona);
				}
				riga = fbr.readLine();
			}

			// ci pensa il try()
			// fbr.close();
		}

		return listaPersone;
	}

	public void salva(Persona personaIn) throws FileNotFoundException, IOException {
		List<Persona> listaPersone = new ArrayList<Persona>();
		
		try (BufferedReader fbr = new BufferedReader(new FileReader(NOME_FILE))) {
			boolean presente = false;
			// ciclo riga per riga, e creo la persona da inserire nella lista
			String riga = fbr.readLine();
			while (null != riga) {
				Persona persona = new Persona(riga);
				if (persona.getTelefono().equals(personaIn.getTelefono())) {
					persona.setCognome(personaIn.getCognome());
					persona.setNome(personaIn.getNome());
					persona.setClasse(personaIn.getClasse());
					presente = true;
				}
				listaPersone.add(persona);
			}
			
			if (!presente) {
				// devo aggiungere la nuova persona alla lista
				listaPersone.add(personaIn);
			}
		}
		
		this.scriviSuFile(listaPersone);

	}
	
	private void scriviSuFile(List<Persona> listaDaSalvare) throws IOException {

		try (BufferedWriter fbw = new BufferedWriter(new FileWriter(NOME_FILE))) {
			for(Persona p : listaDaSalvare) {
				fbw.write(p.toString());
				fbw.newLine();
			}
		}
	}
	

}
