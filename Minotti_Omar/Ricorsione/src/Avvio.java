public class Avvio {

  public static void main(String[] args) {
    //new Console();
    System.out.println(fattorialeR(5));
    
    System.out.println(stampaInversaIterativa("Ciao Mondo!"));
    
    System.out.print(stampaInversaRicorsiva("\n" + "New York Times"));
    
    System.out.println("L'ennessimo numero di Fibonacci �: " + fibonacci(9));
  }
  //il fattoriale di un numero n � il prodotto di n per n-1 per n-2 per .... per 2 * 1,
  // per esempio voglio il fatrtoriale di 5 devo fare 5*4*3*2*1
  //                    4        4*3*2*1
  //5| = 5 * 4
  //5 fattoriale si indica con !5, per convernzione 0" = 1
  //0! = 1
  // N! = N * (N -1)!
  
  // soluzione ricorsiva
  public static long fattorialeR(long n) {
    if (0 == n )
      return 1;
    return n * fattorialeR(n-1);
  }
  // soluzione iterativa
  public static long fattorialeI(long n) {
    long prodotto = 1;
      for(long i = n; i>= 1;i++)
        prodotto *= i;
    return prodotto;
  }
  
  //scrivere usando la ricorsione, una string al rovescio, dal'ultimo al primo carattere
  public static String stampaInversaIterativa(String s) {
	/*if(s == null || s.length() <= 1)
		  return s;
	  return stampaInversa(s.substring(1)) + s.charAt(0);*/
	  
	  if (s == null)
		  return null;
	  
	  String risposta = "";
	  for (int i = s.length() - 1; i >= 0; i--)
		  risposta = risposta + s.charAt(i);
	  return risposta;
  }
  
  //versione ricorsiva
  public static String stampaInversaRicorsiva(String s) {
	  if (s == null)
		  return null;
	  return stampaInversaRicorsivaPos(s, 0);
  }
  
  private static String stampaInversaRicorsivaPos(String s, int numCar) {
	  if(numCar == s.length() - 1) {
		  return "" + s.charAt(numCar);
	  }
	  return stampaInversaRicorsivaPos(s, numCar + 1) + s.charAt(numCar);
  }
  
  //finbonacci(n) = finbonacci(n-1) + finbonacci(n-2)
  //calcolo dell'ennesimo numero di Fibonacci
  //1 1 2 3 5 8 13 21 34
  public static long fibonacci(int n) {
	if(n == 0)
		return 0;
	else if(n == 1)
		return 1;
	else {
		return fibonacci(n - 1) + fibonacci(n - 2);
	}
  }
  
  //Ragionate sul gioco delle Torre di Hanoi
}