
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Finestra extends JFrame implements ActionListener {
	private JTextField textProgressivo = new JTextField();
	private JTextField textNome = new JTextField();
	private JTextField textCognome = new JTextField();
	private JButton buttonInizio = new JButton("Inizio");
	private JButton buttonSuccessivo =  new JButton("Avanti");
	
	private ListaPersone lista;
	
	public Finestra(ListaPersone lista) {
		this.lista = lista;
		
		setLayout(new GridLayout(3,2));
		
		add(new JLabel("Numero:"));
		add(textProgressivo);
		
		add(new JLabel("Nome:"));
		add(textNome);
		
		add(new JLabel("Cognome:"));
		add(textCognome);
		
		add(buttonInizio);
		add(buttonSuccessivo);
		
		buttonInizio.addActionListener(this);
		buttonSuccessivo.addActionListener(this);
		
		setSize(600,400);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}
	

	private void init() {
		this.lista.inizio();
		PersonaConProgressivo pcp = lista.getCorrenteProgressivo();
		
		if(null != pcp) {
			this.textProgressivo.setText("" + pcp.getProgressivo());
			
			Persona persona = pcp.getPersona();
			this.textNome.setText(persona.getNome());
			this.textCognome.setText(persona.getCognome());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton bottone = (JButton)e.getSource();
		
		if(buttonInizio.equals(bottone)) {
			this.lista.inizio();
		}
		Persona persona = lista.getCorrente(); 
		
		if(null != persona) {
			this.textNome.setText(persona.getNome());
			this.textCognome.setText(persona.getCognome());
		}
	}
}