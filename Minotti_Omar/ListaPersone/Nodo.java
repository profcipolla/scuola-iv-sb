

public class Nodo {
	private Persona persona;
	private Nodo succ;
	private int progressivo;
	
	public int getProgressivo() {
		return progressivo;
	}

	public void setProgressivo(int progressivo) {
		this.progressivo = progressivo;
	}

	public Persona getPersona() {
		return persona;
	}
	
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public Nodo getSucc() {
		return succ;
	}
	public void setSucc(Nodo succ) {
		this.succ = succ;
	}

}
