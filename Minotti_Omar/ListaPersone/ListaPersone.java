
public class ListaPersone {
	private Nodo head;
	private Nodo curr;

	
	public void add(Persona persona) {
		Nodo nuovoNodo = new Nodo();
		this.head = nuovoNodo;
	}
	
	public void inizio() {
		this.curr = this.head;
	}
	
	public Persona getCorrente() {
		if(null == this.curr) {
			this.inizio();
		}
		Persona p = null;
		if(null != this.curr) {
			p = this.curr.getPersona();
			this.curr = this.curr.getSucc();
		}
		return p;
	}
	
	public PersonaConProgressivo getCorrenteProgressivo() {
		if(null == this.curr) {
			this.inizio();
		}
		
		PersonaConProgressivo pcp = null;
		if(null != this.curr) {
			pcp = new PersonaConProgressivo(this.curr.getPersona(), this.curr.getProgressivo());
			this.curr = this.curr.getSucc();
		}
		return pcp;
	}
}
