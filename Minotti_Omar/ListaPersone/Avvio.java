
public class Avvio {
	public static void main(String[] args) {
		System.out.println("Avvio Programma" + "\n");
		
		ListaPersone lista = new ListaPersone();
		try {
			Thread.sleep(2000);
			System.out.println("Caricamento Lista.." + "\n");
			Thread.sleep(2000);
			
			lista.add(new Persona("Giulio", "Cesare"));
			lista.add(new Persona("Alessandro", "Magno"));
			lista.add(new Persona("Augusto", "Gaio"));
			
			new Finestra();
			
			} catch(Exception exc) {
					System.out.println("Si � verificato l'errore " + exc.getMessage());
			}
	}
}
