package SynchronizedCounterB;

public class contatore {
	
	private int C;
	
	public contatore(int i)
	{ this.C=i;}
	
	public synchronized void incrementa()
	{ C++;
      System.out.print("\n eseguito incremento: valore attuale del contatore:"+getC()+" ....\n");
	}
	
	public synchronized void decrementa()
	{ C--;
      System.out.print("\n eseguito decremento: valore attuale del contatore:"+getC()+" ....\n");
	}

	public synchronized int getC() {
		return C;
	}
} 
