package SynchronizedCounterB;

public class competingproc extends Thread

{ 
	
  contatore r; /* risorsa condivisa */
  int T; // incrementa se tipo = 1; decrementa se tipo=-1

public competingproc(contatore R, int tipo)
{  
	this.r=R;
    this.T=tipo;
}

public void run()
{ 
 
 if (T>0) {  
        for(int i = 0; i < 10; i++) {
     	   r.incrementa();
   }
 }
	
 else if (T<0) { 
	 
	     for(int i = 0; i < 10; i++) {
		   r.decrementa();
	 }
   }
 }
}

