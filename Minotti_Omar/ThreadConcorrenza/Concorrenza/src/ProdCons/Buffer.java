package ProdCons;

public class Buffer {
	
	private int valore;
	private boolean disponibile = false;
	
	public synchronized int get() {
		try { 
		while (disponibile == false) 
		 wait(); 
		disponibile = false;
		 notifyAll();
		
		}
		catch(InterruptedException e){}
		return valore;
	}
	
	public synchronized void put(int value) {
		try { 
		while (disponibile == true) 
		    wait(); 
			valore = value;
			disponibile = true;
			notifyAll();
		
		}
		 catch(InterruptedException e){}
	}
}