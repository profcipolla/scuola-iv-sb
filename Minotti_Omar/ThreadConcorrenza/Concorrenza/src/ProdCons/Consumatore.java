package ProdCons;

public class Consumatore extends Thread {
	
	private Buffer buffer;
	private int numero;
	
	public Consumatore(Buffer b, int numero) {
		buffer = b;
		this.numero = numero;
	}
	
	public void run() {
		
		int valore = 0;
		for (int i = 0; i < 10; i++) {
			valore = buffer.get();
			System.out.println("Consumatore #"+this.numero+"get:"+ valore);
		}
	}
}