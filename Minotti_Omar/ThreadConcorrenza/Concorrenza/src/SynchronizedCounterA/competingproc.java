package SynchronizedCounterA;
public class competingproc extends Thread

{ 
	
  contatore r; /* risorsa condivisa */
  int T; // incrementa se tipo=1; decrementa se tipo=-1

public competingproc(contatore R, int tipo)
{  
	this.r=R;
    this.T=tipo;
}

public void run()
{ 
 
 if (T>0) {  
	 synchronized(r) {   //entro nella sezione critica
        for(int i = 0; i < 10; i++) {
     	   r.incrementa();
	       System.out.print("\n eseguito incremento: valore attuale del contatore:"+r.getC()+" ....\n");
	 }
   }
 }
	
 else if (T<0) { 
	 
	  synchronized(r) { //entro nella sezione critica
	     for(int i = 0; i < 10; i++) {
		   r.decrementa();
	       System.out.print("\n eseguito decremento: valore attuale del contatore:"+r.getC()+" ....\n");
	 }
  }
}

}

}

