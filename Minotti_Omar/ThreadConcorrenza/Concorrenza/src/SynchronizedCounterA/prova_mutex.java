package SynchronizedCounterA;

public class prova_mutex{ // test
	
	public static void main(String args[]) {
		
		
		contatore C = new contatore(10);
		
		competingproc t1 = new competingproc(C, 1);
		competingproc t2 = new competingproc(C, -1);
		
		t1.start();
		t2.start();		
	}
}


/*	
//final int NP=30;			
int i;

for(i=0;i<(NP/2);i++)
	F[i]=new competingproc(C, 1); // incrementa

for(i=(NP/2);i<NP;i++)
	F[i]=new competingproc(C, -1); // decrementa

for(i=0;i<NP;i++)
F[i].start();*/
