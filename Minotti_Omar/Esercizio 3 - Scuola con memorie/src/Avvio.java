public class Avvio {

	public static void main(String[] a) {
		Persona[] persone = new Persona[10];
		
		for (int i = 0; i < 10; i++) {
			int random = (int)(Math.random() * 100);
			if (0 == random % 2) {
				persone[i] = new Professore("CF" + i);
			} else {
				persone[i] = new Studente(4, "SB");		
			}
		}
		
		for (int i = 0; i < 10; i++) {
			Persona temp = persone[i];
			if (temp instanceof Professore) {
				System.out.println("In posizione " + i + " c� un Professore");
			} else {
				System.out.println("In posizione " + i + " c� uno Studente");
			}
		}
		
	}
	
	
	
	public  void fintoMain(String[] args) {
		Studente studente1 = new Studente(4, "SB");
		Studente studente2 = new Studente(4, "SB");
		Studente studente3 = new Studente(4, "SB");
		Studente studente4 = new Studente(4, "SB");

		studente4 = studente1;

		System.out.println(studente1.getMatricola()); // 1
		System.out.println(studente4.getMatricola()); // 1

		studente2 = studente4;
		System.out.println(studente2.getMatricola()); // 1

		studente2.setMatricola(10);
		System.out.println(studente1.getMatricola()); // 1 secondo Riccardo (10 Gabriel)
		System.out.println(studente4.getMatricola()); // 10 secondo Riccardo (1 secondo Omar)
		System.out.println(studente2.getMatricola()); // 10 secondo Riccardo
		
		studente4 = studente3;
		System.out.println(studente4.getMatricola()); // 3 
		
		studente2 = new Studente(4, "SB");
		System.out.println(studente2.getMatricola()); // 2 secondo Omar // secondo me � ... 5
		System.out.println(studente1.getMatricola()); // 10
		
		
		studente1 = new Studente(4, "SB");
		studente2 = new Studente(4, "SB");
		studente3 = new Studente(4, "SB");
		studente4 = new Studente(4, "SB");
		
		// ...
	}

}