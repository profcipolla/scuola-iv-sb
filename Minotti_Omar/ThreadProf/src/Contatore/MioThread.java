package Contatore;

public class MioThread extends Thread {

	private Contatore contatore;
	private int tipo;
	
	public MioThread(Contatore contatore, int tipo) {
		
		this.tipo = tipo;
		this.contatore=contatore;
	}
	
	public void run() {
		
		for(int i=0; i<10; i++) {
			if(tipo == 1)
			  contatore.incrementa();
			else
			  contatore.decrementa();
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
					
}
	
	
}
