package Contatore;

public class Contatore {
	
	private int c;
	
	public Contatore(int c) {
		
		this.c=c;
	}

	public synchronized void incrementa() {
		
		c++;
	    System.out.println("Incremento eseguito, valore"+c);
	}
	
    public synchronized void decrementa() {
		
		c--;
	    System.out.println("Decremento eseguito, valore"+c);

	}
    
    public synchronized int getC() {
    	
    	return c;
    }
	
}
