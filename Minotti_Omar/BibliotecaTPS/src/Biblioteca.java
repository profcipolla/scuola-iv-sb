import java.util.ArrayList;
import java.util.List;

public class Biblioteca {

	private List<Libro> libri = new ArrayList<>();
	
	public int getNumeroLibri() {
		return libri.size();
	}
	
	public void addLibro(Libro libro) {
		libri.add(libro);
	}
	
	public Libro findLibro(String titolo) {
		for(Libro libro: libri) {
			if (libro.getTitolo().equals(titolo))
				return libro;
		}
		System.out.println("ATTENZIONE: Titolo non trovato!");
		return new Libro();
	}
	
	private ArrayList<Libro> findLibriArrayList (String autore) {
		ArrayList<Libro> libriAutore = new ArrayList<>();
		for(Libro libro: libri) {
			if(libro.getAutore().equals(autore))
				libriAutore.add(libro);
		}
		return libriAutore;	
	}
}
