import java.util.ArrayList;

public class GestioneBiblioteca {

	public static void main(String[] args) {
		Biblioteca biblioteca = new Biblioteca();
		Libro libro;
		ArrayList<Libro> libriAutore = new ArrayList<>();
		
		libro = new Libro(1, 2006, "Libro Cuore", "De Amicis", "Mondadori");
		biblioteca.addLibro(libro);
		
		libro = new Libro(2, 4, "L'arte della guerra", "Sun Tzu", "Barbera");
		biblioteca.addLibro(libro);
		
		libro = new Libro(3, 1966, "Silenzio", "Sh�saku End�", "Corbaccio");
		biblioteca.addLibro(libro);
		
		libro = new Libro(4, 2019, "Istantanee intorno al mondo", "Nicol� Balini", "Mondadori");
		biblioteca.addLibro(libro);
		
		libro = new Libro(5, 1947, "Se questo � un uomo", "Primo Levi", "Einaudi");
		biblioteca.addLibro(libro);
		
		libro = new Libro(6, 2020, "Enciclopedia Vol.1", "Repubblica", "Mondadori");
		biblioteca.addLibro(libro);
		
		libro = new Libro(7, 2020, "Enciclopedia Vol.2", "Repubblica", "Mondadori");
		biblioteca.addLibro(libro);
		
		System.out.println("Titoli presenti: " + biblioteca.getNumeroLibri());
		
		libro = biblioteca.findLibro("Pinocchio");
		

		System.out.println("Informazioni titolo: " + libro.getAnno() + libro.getTitolo()
			+ libro.getAutore() + libro.getEditore());
		}
}
