
public class Libro {

	private int codice;
	private String titolo;
	private String autore;
	private String editore;
	private int anno;
	
	public Libro() {
		
	}
	
	public Libro(int codice, int anno, String autore, String editore, String titolo) {
		super();
		this.codice = codice;
		this.titolo = titolo;
		this.autore = autore;
		this.editore = editore;
		this.anno = anno;
	}

	public int getCodice() {
		return codice;
	}

	public String getTitolo() {
		return titolo;
	}

	public String getAutore() {
		return autore;
	}

	public String getEditore() {
		return editore;
	}

	public int getAnno() {
		return anno;
	}

	@Override
	public String toString() {
		return "libro [codice=" + codice + ", titolo=" + titolo + ", autore=" + autore + ", editore=" + editore
				+ ", anno=" + anno + "]";
	}
	
	

}
