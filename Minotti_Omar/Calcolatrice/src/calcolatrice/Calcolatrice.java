package calcolatrice;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calcolatrice implements ActionListener{
	
	JFrame frame;
	JTextField display;
	JButton[] scritte = new JButton[10];
	JButton[] funzioni = new JButton[8];
	JButton aggButton,sottButton, moltButton,divButton;
	JButton decButton,uguButton,delButton,cancButton;
	JPanel panel;
	
	Font mioFont = new Font("Verdana", Font.BOLD, 26);
	
	
	double num1 = 0, num2 = 0, ris = 0;
	char operatore;
	
		
	Calcolatrice() {
		frame = new JFrame("Calcolatricce");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(335, 535);
		frame.setLayout(null);
		
		display = new JTextField();
		display.setBounds(10, 25, 300, 50);
		display.setFont(mioFont);
		display.setEditable(false);
		
		aggButton = new JButton("+");
		sottButton = new JButton("-");
		moltButton = new JButton("*");
		divButton = new JButton("/");
		decButton = new JButton(",");
		uguButton = new JButton("=");
		delButton = new JButton("C");
		cancButton = new JButton("CE");
		
		funzioni[0] = aggButton;
		funzioni[1] = sottButton;
		funzioni[2] = moltButton;
		funzioni[3] = divButton;
		funzioni[4] = decButton;
		funzioni[5] = uguButton;
		funzioni[6] = delButton;
		funzioni[7] = cancButton;
		
		for(int i = 0; i < funzioni.length; i++) {
			funzioni[i].addActionListener(this);
			funzioni[i].setFont(mioFont);
			funzioni[i].setFocusable(false);
		}
		
		for(int i = 0; i < scritte.length; i++) {
			scritte[i] = new JButton(String.valueOf(i));
			scritte[i].addActionListener(this);
			scritte[i].setFont(mioFont);
			scritte[i].setFocusable(false);
		}
		
		delButton.setBounds(10, 430, 145, 50);
		cancButton.setBounds(165, 430, 145, 50);
		//cancButton.setBounds(x, y, width, height);
		
		
		panel = new JPanel();
		panel.setBounds(10, 100, 300, 300);
		panel.setLayout(new GridLayout(4, 4, 10, 10));
		
		panel.add(scritte[7]);
		panel.add(scritte[8]);
		panel.add(scritte[9]);
		panel.add(divButton);
		panel.add(scritte[4]);
		panel.add(scritte[5]);
		panel.add(scritte[6]);
		panel.add(moltButton);
		panel.add(scritte[1]);
		panel.add(scritte[2]);
		panel.add(scritte[3]);
		panel.add(sottButton);
		panel.add(decButton);
		panel.add(scritte[0]);
		panel.add(uguButton);
		panel.add(aggButton);
		
		frame.add(panel);
		frame.add(delButton);
		frame.add(cancButton);
		frame.add(display);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		Calcolatrice calc = new Calcolatrice();
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source instanceof JButton) {
			JButton bottonePremuto = (JButton)source;
			
			String contenutoDisplay = display.getText();
			String cifra = bottonePremuto.getText();
			
			for (int i = 0; i < scritte.length; i ++) {
				if(source == scritte[i]) {
					display.setText(contenutoDisplay.concat(String.valueOf(i)));
				}
			}
			if (source == decButton) {
				display.setText(contenutoDisplay.concat(","));
			}
			if (source == aggButton) {
				num1 = Double.parseDouble(contenutoDisplay);
				operatore = '+';
				display.setText("");
			}
			if (source == sottButton) {
				num1 = Double.parseDouble(contenutoDisplay);
				operatore = '-';
				display.setText("");
			}
			if (source == moltButton) {
				num1 = Double.parseDouble(contenutoDisplay);
				operatore = '*';
				display.setText("");
			}
			if (source == divButton) {
				num1 = Double.parseDouble(contenutoDisplay);
				operatore = '/';
				display.setText("");
			}
			if (source == uguButton) {
				num2 = Double.parseDouble(contenutoDisplay);
				
				switch(operatore) {
				case'+':
					ris = num1 + num2;
					break;
				case'-':
					ris = num1 - num2;
					break;
				case'*':
					ris = num1 * num2;
					break;
				case'/':
					ris = num1 / num2;
					break;
				}
				display.setText(String.valueOf(ris));
				num1 = ris;
			}
			
			if (source == delButton) {
				String string = display.getText();
				display.setText("");
				for(int i = 0; i < string.length() - 1; i ++) {
					display.setText(display.getText() + string.charAt(i));
				}
			}
			if (source == cancButton) {
				display.setText("");
			}
			
		} else if (source instanceof JTextField) {
			JTextField text = (JTextField)source;
			System.out.println(text.getText());
		}
	}
		
	
}