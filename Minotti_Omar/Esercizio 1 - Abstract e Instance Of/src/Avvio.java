public class Avvio {

	public static void main(String[] args) {
		Figura unaFigura;
		unaFigura = new Rettangolo(10, 5);

		if (unaFigura instanceof Triangolo) {
			Triangolo converteTriangolo = (Triangolo) unaFigura;
			System.out.println("La base del triangolo �: " + converteTriangolo.getBase());
		}

		if (unaFigura instanceof Rettangolo) {
			Rettangolo converteRettangolo = (Rettangolo) unaFigura;
			System.out.println("La base del rettangolo �: " + converteRettangolo.getBase());
		}

		// System.out.println("Area del rettangolo: " + unaFigura.calcolaArea());

	}

}