public class Cerchio extends Figura {
	private long raggio;

	public Cerchio(long raggio) {
		this.setRaggio(raggio);
	}
	
	@Override
	public double calcolaArea() {
		return Math.PI * raggio * raggio;
	}

	@Override
	public double calcolaPerimetro() {
		return 2 * Math.PI * raggio;
	}

	public long getRaggio() {
		return raggio;
	}

	public void setRaggio(long raggio) {
		this.raggio = raggio;
	}

}