public abstract class Figura {
	private String nome;

	public abstract double calcolaArea();
	
	public abstract double calcolaPerimetro();
	
	public String getNome() {
		return this.nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
