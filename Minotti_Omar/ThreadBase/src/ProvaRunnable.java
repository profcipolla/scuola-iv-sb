
public class ProvaRunnable {

	public static void main(String[] args) {
		// Metodo 1 (Il migliore):
		
		//Runnable run1 = new ThreadRun("thread1",5);
		//Thread thr1 = new Thread(run1);
		
		// Main:
		//System.out.println(Thread.currentThread().getName()); main thread
		
		// Metodo 2:
		
		ThreadRun run1 = new ThreadRun("thread1", 5);
		Thread thr1 = new Thread(run1);
		
		thr1.start();
		
		ThreadRun run2 = new ThreadRun("thread2", 5);
		Thread thr2 = new Thread(run2);
		
		thr2.start();
		
		// Metodo 3:
		//new Thread(new ThreadRun("thread3",5)).start();
	}

}
