
public class SimpleThread extends Thread {
	
	public SimpleThread(String nome) {
		setName(nome);
	}
	
	public void run() {
		for(int i = 0; i < 10; i++) {
			System.out.println(i + "" + getName());
		}
		try {
			Thread.sleep((long)(Math.random()*100));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
