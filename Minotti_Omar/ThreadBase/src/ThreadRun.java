
public class ThreadRun implements Runnable {
	private String nome;
	private int conta;
	
	public ThreadRun(String nome, int conta) {
		this.nome = nome;
		this.conta = conta;
	}
	
	public void run() {
		for(int i = 0; i < conta; i++) {
			System.out.println(i + "" + this.nome);	
			try {
				Thread.currentThread().sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
