import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

public class Convertitore {

	private JFrame frmConvertitore;
	private JTextField txtImporto;
	private JLabel lblNewLabel_2;
	private JTextField txtValuta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Convertitore window = new Convertitore();
					window.frmConvertitore.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Convertitore() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmConvertitore = new JFrame();
		frmConvertitore.setResizable(false);
		frmConvertitore.setTitle("Convertitore Valute");
		frmConvertitore.setBounds(100, 100, 471, 415);
		frmConvertitore.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Importo:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		int start = 0;
		int end = 20;
		int value = 0;
		JSlider slider = new JSlider(start, end, value);
		
		slider.setPaintTicks(true);
		slider.setMinorTickSpacing(10);
		
		slider.setPaintLabels(true);
		slider.setMajorTickSpacing(10);
		
		Dictionary<Integer, JLabel> labels = new Hashtable<>();
		for(int i = start; i <= end; i += 2) {
			String text = String.format("%.1f", i / 10.0);
			labels.put(i, new JLabel(text));
		}
		slider.setLabelTable(labels);
		
		txtImporto = new JTextField();
		txtImporto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				char c = e.getKeyChar();
				
				if(Character.isLetter(c)) {
					txtImporto.setEditable(false);
					txtValuta.setText("ERRORE: Inserire numeri");
					txtImporto.setText("");
				} else {
					txtImporto.setEditable(true);
				}
			}
		});
		txtImporto.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Da:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JList list = new JList();
		
		lblNewLabel_2 = new JLabel("A:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JComboBox txtDa = new JComboBox();
		txtDa.setName("");
		txtDa.setToolTipText("");
		txtDa.setModel(new DefaultComboBoxModel(new String[] {"--", "EUR", "USD"}));
		
		JComboBox txtA = new JComboBox();
		txtA.setModel(new DefaultComboBoxModel(new String[] {"--", "USD", "EUR"}));
		
		JButton btnNewButton = new JButton("CONVERTI");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtImporto.getText().trim().isEmpty()) {
					txtValuta.setText("ERRORE: Inserire importo");
				} else {
				
				Double totale;
				Double importo = Double.parseDouble(txtImporto.getText());
				
				Integer percentuale = slider.getValue();
				
				if(txtDa.getSelectedItem().toString() == "--" || txtA.getSelectedItem().toString() == "--") {
					txtValuta.setText("ERRORE: Scegliere le valute");
				}
				
				if(txtDa.getSelectedItem().toString() == "EUR" && txtA.getSelectedItem().toString() == "USD") {
					totale = importo * percentuale / 10;
					txtValuta.setText(totale + " $");
				} else if(txtDa.getSelectedItem().toString() == "USD" && txtA.getSelectedItem().toString() == "EUR") {
					totale = importo * percentuale / 10;
					txtValuta.setText(totale + " �");
				}
				
				if(txtDa.getSelectedItem().toString() == "USD" && txtA.getSelectedItem().toString() == "USD") {
					totale = importo;
					txtValuta.setText(totale + " $");
				} else if(txtDa.getSelectedItem().toString() == "EUR" && txtA.getSelectedItem().toString() == "EUR") {
					totale = importo;
					txtValuta.setText(totale + " �");
				}
			  }
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JButton btnReset = new JButton("RESET");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtImporto.setText("");
				txtValuta.setText("");
				txtDa.setSelectedIndex(0);
				txtA.setSelectedIndex(0);
				slider.setValue(0);
			}
		});
		btnReset.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		txtValuta = new JTextField();
		txtValuta.setHorizontalAlignment(SwingConstants.CENTER);
		txtValuta.setEditable(false);
		txtValuta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtValuta.setColumns(10);
		
		JLabel lblVariazione = new JLabel("Tasso:");
		lblVariazione.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		
		GroupLayout groupLayout = new GroupLayout(frmConvertitore.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(71)
					.addComponent(btnReset, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addGap(79))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(106, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel)
								.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(list, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(293, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(txtValuta, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
							.addGap(99))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(txtDa, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtImporto, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtA, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
							.addGap(70))))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addComponent(lblVariazione, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(slider, GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
					.addGap(71))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(37)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(txtImporto, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))
									.addGap(18)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(txtDa, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel_1))
									.addGap(36)
									.addComponent(list, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(132)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(txtA, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
							.addGap(27)
							.addComponent(slider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(25)
							.addComponent(txtValuta, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
							.addGap(18))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(lblVariazione, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
							.addGap(124)))
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnReset, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton))
					.addGap(38))
		);
		frmConvertitore.getContentPane().setLayout(groupLayout);
	}
}
