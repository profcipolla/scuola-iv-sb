
public class mioThread extends Thread {
	
	private Contatore contatore;
	private int tipo;
	
	public mioThread(Contatore contatore, int tipo) {
		this.contatore = contatore;
		this.tipo = tipo;
	}
	
	public void run() {
		
		for (int i = 1; i < 10; i++) {
			if (tipo == 1) {
				contatore.incrementa();
				System.out.println("Incremento eseguito, il valore ora �: " + contatore.getContatore());
			} else {
				contatore.decrementa();
				System.out.println("Decremento eseguito, il valore ora �: " + contatore.getContatore());
			}
		}
		
		try {
			sleep(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
