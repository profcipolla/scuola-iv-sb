package produttoreConsumatore;

public class GestioneProdCons {

	public static void main(String[] args) {
		
		Buffer buffer = new Buffer();
		
		Thread produttore = new Produttore(buffer, 100);
		Thread consumatore = new Consumatore(buffer, 10);
		
		produttore.start();
		consumatore.start();
	}
}
