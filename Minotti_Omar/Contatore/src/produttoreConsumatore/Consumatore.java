package produttoreConsumatore;

public class Consumatore extends Thread {
	
	private Buffer buffer;
	private int numero;
	
	public Consumatore(Buffer buffer, int numero) {
		this.buffer = buffer;
		this.numero = numero;
	}
	
	public void run() {
		int valore = 0;
		
		for (int i = 0; i < 9; i++) {
			valore = buffer.preleva();
			System.out.println("Consumatore " + this.numero + " valore prelevato " + valore);
		}
	}
}
