package produttoreConsumatore;

public class mioThread extends Thread {
	
	private Buffer buffer;
	private int tipo;
	
	public mioThread(Buffer buffer, int tipo) {
		this.buffer = buffer;
		this.tipo = tipo;
	}
	
	public void run() {
		
		int valore = 0;
		
		for (int i = 0; i < 10; i++) {
			if (tipo == 1) {
				buffer.preleva();
				System.out.print(buffer.getContenuto() + " ");
			} else {
				buffer.deposita(10);
				System.out.print(buffer.getContenuto() + " ");
			}
		}
	}
}
