package produttoreConsumatore;

public class Produttore extends Thread {
	
	private Buffer buffer;
	private int numero;
	
	public Produttore(Buffer buffer, int numero) {
		this.buffer = buffer;
		this.numero = numero;
	}
	
	public void run() {
		
		for (int i = 0; i < 9; i++) {
			buffer.deposita(i);
			System.out.println("Produttore " + this.numero + " valore depositato " + i);
		}
	}
}
