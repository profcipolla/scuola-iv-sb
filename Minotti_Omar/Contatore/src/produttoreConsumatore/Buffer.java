package produttoreConsumatore;

public class Buffer {
	
	private int contenuto;
	private boolean pieno = false; // Variabile condizione, funge da "semaforo" tra produttore e consumatore;
	
	public synchronized int preleva() {
		
		// Attendi un dato dai produttori;
		try {
			while (pieno == false) {
				wait();
				pieno = false;
				notify();
			}
		} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		return contenuto;
	}
	
	public synchronized void deposita(int contenuto) {
		
		// Attendi il consumo del dato;
		try {
			while (pieno == true) {
				wait();
				contenuto = contenuto;
				pieno = true;
				notify();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized int getContenuto() {
		return contenuto;
	}	
}
