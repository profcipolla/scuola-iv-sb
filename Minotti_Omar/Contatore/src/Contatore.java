
public class Contatore {
	
	private int c;
	
	public Contatore(int c) {
		this.c = c;
	}
	
	public synchronized void incrementa() {
		c++;
	}
	
	public synchronized void decrementa() {
		c--;
	}
	
	public synchronized int getContatore() {
		return c;
	}
}
