package contatore2;

public class Contatore2 {
	
	private int c;
	
	public Contatore2(int c) {
		this.c = c;
	}
	
	public void incrementa() {
		c++;
	}
	
	public void decrementa() {
		c--;
	}
	
	public int getContatore() {
		return c;
	}
}
