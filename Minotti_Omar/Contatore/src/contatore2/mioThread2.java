package contatore2;

public class mioThread2 extends Thread {
	
	private Contatore2 contatore;
	private int tipo;
	
	public mioThread2(Contatore2 contatore, int tipo) {
		this.contatore = contatore;
		this.tipo = tipo;
	}
	
	public void run() {
		
		// codice critico
		synchronized (contatore) {
			for (int i = 1; i < 10; i++) {
				if (tipo == 1) {
					contatore.incrementa();
					System.out.println(contatore.getContatore());
				} else {
					contatore.decrementa();
					System.out.println(contatore.getContatore());
				}
			}
			
			try {
				sleep(300);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
}
