
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class WinRubrica extends JFrame implements ActionListener {

	private JTextField tfNome = new JTextField(100);
	private JTextField tfCognome = new JTextField(100);
	private JTextField tfTelefono = new JTextField(100);

	private JButton bPrec = new JButton("<");
	private JButton bSucc = new JButton(">");

	private int posizione = 0;
	private List<Persona> dati;

	public WinRubrica() {

		FileService fileService = new FileService();
		try {
			dati = fileService.leggiDati();
			this.visualizza();
		} catch (IOException e) {
			e.printStackTrace();
		}

		setTitle("RUBRICA");
		setLayout(new GridLayout(4, 2));
		add(new JLabel("Nome"));
		add(tfNome);
		add(new JLabel("Cognome"));
		add(tfCognome);
		add(new JLabel("Telefono"));
		add(tfTelefono);

		add(bPrec);
		bPrec.addActionListener(this);
		add(bSucc);
		bSucc.addActionListener(this);

		setSize(800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void visualizza() {
		Persona p = dati.get(this.posizione);
		this.tfNome.setText(p.getNome());
		this.tfCognome.setText(p.getCognome());
		this.tfTelefono.setText(p.getTelefono());
	}

	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		System.out.print(btn.getText());

		// deve sempre essere 0 =< posizione <= dati.size
		if (btn == this.bSucc) {
			if (this.posizione < this.dati.size() - 1) {
				this.posizione++;
				this.visualizza();
			} else {
				this.bSucc.setEnabled(false);
			}
			
			if (this.posizione > 0) {
				this.bPrec.setEnabled(true);
			}
			
		} else {
			if (this.posizione > 0) {
				this.posizione--;
				this.visualizza();
			} else {
				this.bPrec.setEnabled(false);
			}
			
			if (this.posizione < this.dati.size() - 1) {
				this.bSucc.setEnabled(true);
			}
		}
	}
}
