package concessionaria;

public class MovimentiConcessionaria {

	private Auto[] au = new Auto[5];
	private CasaAutomobilistica[] casaAuto = new CasaAutomobilistica[5];
	private MovimentiConcessionaria[] mc = new MovimentiConcessionaria[5];
	
	public MovimentiConcessionaria() {}
	public MovimentiConcessionaria(Auto[] insAuto, CasaAutomobilistica[] insCa) {
		this.au = insAuto;
		this.casaAuto = insCa;
	}

	public void addAuto(Auto ains) {
		int i = 0;
		while (i < au.length && au[i] != null) {
			i++;

		}
		if (i < au.length) {
			au[i] = ains;

		}
		
	}

	public void addCasa(CasaAutomobilistica insCasa) {
		int i = 0;
		while (i < casaAuto.length && casaAuto[i] != null) {
			i++;

		}
		if (i < casaAuto.length) {
			casaAuto[i] = insCasa;

		}
		
	}

	public void faiTutto() {
		arrayAuto(new MovimentiConcessionaria(au, casaAuto));
	}

	public void arrayAuto(MovimentiConcessionaria insTutto) {
		int i = 0;
		while (i < mc.length && mc[i] != null) {
			i++;

		}
		if (i < mc.length) {
			mc[i] = insTutto;
			return;
		}
		System.out.println("ATTENZIONE: Memoria Auto piena!");
	}
	public void StampaArrayAuto() {
		int i = 0;
		    while (i < mc.length && mc[i] != null) {
		      int ID = i+1;
		      System.out.println(
		          ID+"\n"+"Resoconto Automobili:\n\n" 
		          + "Casa Produttrice: "+mc[i].au[i].getCasaMarchio()+ "\n"
		          + "Nazione: "+mc[i].au[i].getCasaMarchio() + "\n"
		          + "Prezzo: " +mc[i].au[i].getPrezzo() + " �\n"
		          + "Numero Posti: " +mc[i].au[i].getNumeroPosti() + "\n"
		          + "Colore: "+mc[i].au[i].getColoreAuto() + "\n"
		          + "Cilindrata: "+mc[i].au[i].getCc() + "cc\n"
		          + "Cavalli: "+mc[i].au[i].getCv() + "cv\n");
		      i++;
		    }
		    if (i > mc.length-1) {
		      System.out.println("Memoria Piena!!!");
		    }

		  }
	}

