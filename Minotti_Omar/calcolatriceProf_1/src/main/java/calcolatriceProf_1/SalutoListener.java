package calcolatriceProf_1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class SalutoListener implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		JButton ilBottone = (JButton)e.getSource();
		System.out.println(ilBottone.getText());
		System.out.println("Hai premuto il pulsante saluto!");
	}
	
}
