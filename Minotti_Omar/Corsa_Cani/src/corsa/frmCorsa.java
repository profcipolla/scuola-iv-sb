package corsa;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.border.EtchedBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class frmCorsa extends JFrame {

    JLabel primo_cane = new JLabel();
    JLabel secondo_cane = new JLabel();
    JLabel traguardo = new JLabel();

    Corsa cane1 = new Corsa(primo_cane, this);
    Corsa cane2 = new Corsa(secondo_cane, this);

    private JPanel contentPane;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    frmCorsa frame = new frmCorsa();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public frmCorsa() {
        setTitle("Cinodromo");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 700, 450);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("CINODROMO");
        lblNewLabel.setBounds(5, 5, 674, 44);
        lblNewLabel.setFont(new Font("Calibri", Font.PLAIN, 36));
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblNewLabel);

        JPanel panel = new JPanel();
        panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        panel.setBounds(15, 59, 661, 260);
        contentPane.add(panel);
        panel.setLayout(null);

        primo_cane.setIcon(new ImageIcon(frmCorsa.class.getResource("/immagini/Cane.gif")));
        primo_cane.setBounds(8, 20, 184, 74);
        panel.add(primo_cane);

        secondo_cane.setIcon(new ImageIcon(frmCorsa.class.getResource("/immagini/Cane.gif")));
        secondo_cane.setBounds(8, 171, 184, 79);
        panel.add(secondo_cane);

        traguardo.setOpaque(true);
        traguardo.setBackground(Color.LIGHT_GRAY);
        traguardo.setBounds(602, 10, 22, 240);
        panel.add(traguardo);

        JButton btnNewButton = new JButton("AVVIA CORSA");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(primo_cane.getLocation().x);
                System.out.println(secondo_cane.getLocation().x);
                System.out.println(traguardo.getLocation().x);

                primo_cane.setLocation(8, primo_cane.getLocation().y);
                secondo_cane.setLocation(8, secondo_cane.getLocation().y);

                cane1.start();
                cane2.start();
            }
        });
        btnNewButton.setBackground(Color.LIGHT_GRAY);
        btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
        btnNewButton.setBounds(269, 340, 159, 44);
        contentPane.add(btnNewButton);
    }

    public JLabel getPrimo_cane() {
        return primo_cane;
    }

    public JLabel getSecondo_cane() {
        return secondo_cane;
    }

    public JLabel getTraguardo() {
        return traguardo;
    }

}
