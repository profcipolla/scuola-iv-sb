package corsa;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Corsa extends Thread {

    private JLabel etichetta;
    private frmCorsa cane;

    public Corsa(JLabel etichetta, frmCorsa cane) {
        this.etichetta = etichetta;
        this.cane = cane;
    }

    @Override
    public void run() {

        int cane1 = 0;
        int cane2 = 0;

        while (true) {
            try {
                sleep((int) (Math.random() * 1000));
                cane1 = cane.getPrimo_cane().getLocation().x;
                cane2 = cane.getSecondo_cane().getLocation().x;

                if (cane1 < cane.getTraguardo().getLocation().x - 125 && cane2 < cane.getTraguardo().getLocation().x - 125) {
                    etichetta.setLocation(etichetta.getLocation().x + 20, etichetta.getLocation().y);
                    cane.repaint();
                } else {
                    break;
                }

            } catch (InterruptedException e) {
                System.out.println(e);
            }

            if (etichetta.getLocation().x >= cane.getTraguardo().getLocation().x - 125) {
                if (cane1 > cane2) {
                    JOptionPane.showMessageDialog(null, "Vince il primo cane");
                } else if (cane2 > cane1) {
                    JOptionPane.showMessageDialog(null, "Vince il secondo cane");
                } else {
                    JOptionPane.showMessageDialog(null, "Pareggio");
                }
            }
        }
    }
}
