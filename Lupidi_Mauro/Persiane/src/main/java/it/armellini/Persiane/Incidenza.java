
package it.armellini.Persiane;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import it.armellini.calcolatrice.CalcolatriceWin;
import it.armellini.concessionaria.FinestraConcessionaria;
import it.armellini.libreria.FinestraAutore;
import it.armellini.libreria.FinestraLibro;


public class Incidenza extends JFrame implements ActionListener{ 

	
	public Incidenza() {
		setTitle("Finestra di Mauro Lupidi");
		setSize(800, 400);
		JMenuBar mBar=new JMenuBar();
		setJMenuBar(mBar);
		JMenu PrimaVoceMenu= new JMenu("File");
		mBar.add(PrimaVoceMenu);
		
		JMenu SecondaVoceMenu= new JMenu("Edit");
		mBar.add(SecondaVoceMenu);
		
		JMenu TerzaVoceMenu= new JMenu("Scuola");
		mBar.add(TerzaVoceMenu);
		
		JMenu QuartaVoceMenu=new JMenu("Documenti");
		mBar.add(QuartaVoceMenu);
		
		
		PrimaVoceMenu.addSeparator();
		JMenu PrimoSottoMenuFile=new JMenu("Nuovo");
		PrimaVoceMenu.add(PrimoSottoMenuFile);
		PrimoSottoMenuFile.add(new JMenuItem("Disegno"));
		
		SecondaVoceMenu.addSeparator();
		JMenuItem SecondoSottoMenuFile=new JMenuItem("Indietro di un passo");
		JMenuItem TerzoSottoMenuFile=new JMenuItem("Avanti di un passo");
		SecondaVoceMenu.add(SecondoSottoMenuFile);
		SecondaVoceMenu.add(TerzoSottoMenuFile);
		
		TerzaVoceMenu.addSeparator();
		JMenuItem QuartoSottoMenuFile=new JMenuItem("Calcolatrice");
		JMenuItem QuintoSottoMenuFile=new JMenuItem("Concessionaria");
		JMenu SestoSottoMenuFile=new JMenu("Biblioteca");
        JMenuItem Iautore=new JMenuItem("Autore");
         SestoSottoMenuFile.add(Iautore);
        JMenuItem Ilibro=new JMenuItem("Libro");
         SestoSottoMenuFile.add(Ilibro);

		
		TerzaVoceMenu.add(QuartoSottoMenuFile);
		TerzaVoceMenu.add(QuintoSottoMenuFile);
		TerzaVoceMenu.add(SestoSottoMenuFile);
		
		QuartoSottoMenuFile.addActionListener(this);
		QuintoSottoMenuFile.addActionListener(this);
		
		Iautore.addActionListener(this);
		Ilibro.addActionListener(this);
		
		
		
		
		
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		
		
		
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	JMenuItem FusioneEsercizi=(JMenuItem) e.getSource();
	switch(FusioneEsercizi.getText()) {
	case "Calcolatrice":
		new CalcolatriceWin("Calcolatrice");
		break;
	case "Concessionaria":
		new FinestraConcessionaria();
		break;
	case "Autore":
		new FinestraAutore();
		break;
	case "Libro":
		new FinestraLibro();
		break;
		
		
	
	
	}
	
	
	
	
	
	
	
	}
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
