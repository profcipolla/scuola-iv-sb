package it.armellini.concessionaria;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraConcessionaria extends JFrame implements ActionListener {
	
	MovimentiConcessionaria mC = new MovimentiConcessionaria();
	CasaAutomobilistica casaA = new CasaAutomobilistica();
	private float prezzo = 0.0f;
	private Integer posti = 0;
	private String colore = "";
	private Float cc = 0.0f;
	private Integer cv = 0;
	private String marchio = "";
	
	private JTextField tNumeroPosti;
	private JTextField tColoreAuto;
	private JTextField tCcAuto;
	private JTextField tCvAuto;
	private JTextField tCasaCostruttrice;
	private JTextField tNazione;
	private JTextField tPrezzo;
	private JTextField tAnno;

	public FinestraConcessionaria() {
		setTitle("Gestione Concessionaria");
		setSize(569, 400);
		setLocation(700, 700);
		getContentPane().setLayout(new GridLayout(3, 1, 0, 0));

		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel titoloLable = new JLabel("Concessionaria Autoveicolo");
		titoloLable.setHorizontalAlignment(SwingConstants.CENTER);
		titoloLable.setForeground(Color.RED);
		titoloLable.setBackground(Color.BLACK);
		titoloLable.setFont(new Font("Yu Gothic Light", Font.PLAIN, 27));
		panel.add(titoloLable);

		JPanel pAuto = new JPanel();
		pAuto.setBackground(Color.MAGENTA);
		getContentPane().add(pAuto);
		pAuto.setLayout(new GridLayout(4, 2, 0, 0));

		JLabel lNumeroPosti = new JLabel("Numero Posti");
		lNumeroPosti.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lNumeroPosti);

		tNumeroPosti = new JTextField();
		tNumeroPosti.setHorizontalAlignment(SwingConstants.CENTER);
		tNumeroPosti.setColumns(10);
		pAuto.add(tNumeroPosti);
		tNumeroPosti.addActionListener(this);

		JLabel lColoreAuto = new JLabel("Colore Auto");
		lColoreAuto.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lColoreAuto);

		tColoreAuto = new JTextField();
		tColoreAuto.setHorizontalAlignment(SwingConstants.CENTER);
		tColoreAuto.setColumns(10);
		pAuto.add(tColoreAuto);
		tColoreAuto.addActionListener(this);

		JLabel lCcAuto = new JLabel("Cc");
		lCcAuto.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lCcAuto);

		tCcAuto = new JTextField();
		tCcAuto.setHorizontalAlignment(SwingConstants.CENTER);
		tCcAuto.setColumns(10);
		pAuto.add(tCcAuto);
		tCcAuto.addActionListener(this);

		JLabel lCvAuto = new JLabel("Cv ");
		lCvAuto.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lCvAuto);

		tCvAuto = new JTextField();
		tCvAuto.setHorizontalAlignment(SwingConstants.CENTER);
		tCvAuto.setColumns(10);
		pAuto.add(tCvAuto);
		tCvAuto.addActionListener(this);

		JLabel lCasaCostruttrice = new JLabel("Casa Costruttrice");
		lCasaCostruttrice.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lCasaCostruttrice);

		tCasaCostruttrice = new JTextField();
		tCasaCostruttrice.setHorizontalAlignment(SwingConstants.CENTER);
		tCasaCostruttrice.setColumns(10);
		pAuto.add(tCasaCostruttrice);
		tCasaCostruttrice.addActionListener(this);

		JLabel lNazione = new JLabel("Nazione");
		lNazione.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lNazione);

		tNazione = new JTextField();
		tNazione.setHorizontalAlignment(SwingConstants.CENTER);
		tNazione.setColumns(10);
		pAuto.add(tNazione);
		tNazione.addActionListener(this);

		JLabel lPrezzo = new JLabel("Prezzo");
		lPrezzo.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lPrezzo);

		tPrezzo = new JTextField();
		tPrezzo.setHorizontalAlignment(SwingConstants.CENTER);
		tPrezzo.setColumns(10);
		pAuto.add(tPrezzo);
		
		JLabel lAnno = new JLabel("Anno Produzione");
		lAnno.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(lAnno);
		
		tAnno = new JTextField();
		tAnno.setHorizontalAlignment(SwingConstants.CENTER);
		pAuto.add(tAnno);
		tAnno.setColumns(10);
		tAnno.addActionListener(this);
		tPrezzo.addActionListener(this);

		JButton bSalva = new JButton("Salva");
		bSalva.setForeground(Color.RED);
		bSalva.setFont(new Font("MS UI Gothic", Font.PLAIN, 23));
		bSalva.setBackground(Color.BLACK);
		getContentPane().add(bSalva);
		setBackground(Color.RED);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		bSalva.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent in) {

		Object source = in.getSource();
		
		
		if (source instanceof JTextField) {
			JTextField testoDisplay = (JTextField) source;
			if (source == tCasaCostruttrice) {
				marchio = testoDisplay.getText();
			} else if (source == tPrezzo) {
				prezzo = Float.parseFloat(testoDisplay.getText());
			} else if (source == tNumeroPosti) {
				posti = Integer.parseInt(testoDisplay.getText());
			} else if (source == tColoreAuto) {
				colore = testoDisplay.getText();
			} else if (source == tNazione) {
				casaA.setNazione(testoDisplay.getText());	
			} else if (source == tAnno) {
				casaA.setAnnoProduzione(Integer.parseInt(testoDisplay.getText()));
			} else if (source == tCcAuto) {
				cc = Float.parseFloat(testoDisplay.getText());
			} else if (source == tCvAuto) {
				cv = Integer.parseInt(testoDisplay.getText());	
			}
		}
		else if (source instanceof JButton) {
			
			mC.addAuto(new Auto(prezzo, posti, colore, cc, cv, marchio));
			mC.addCasa(new CasaAutomobilistica(casaA.getAnnoProduzione(),casaA.getNazione()));
			mC.faiTutto();
			mC.StampaArrayAuto();
		}

	}
}
