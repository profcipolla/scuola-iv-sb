import java.util.Date;
public class Motore
{
  private int Cilindri;
  private int Cc;
  private int Cv;
  private String Alimentazione;
  
  //costruttore
  public Motore( int Cc, int Cv){
      
      this.Cc=Cc;
      this.Cv=Cv;
    }
 //Getter (guarda contenuto)
 public int getCilindri(){
     return this.Cilindri;
    }
    public int getCc(){
        return this.Cc;
    }
    public int getCv(){
        return this.Cv;
    }
    public String getAlimentazione(){
        return this.Alimentazione;
    }
    //setter (modifica contenuto)
    public void setCilindri(int Cilindri){
        this.Cilindri=Cilindri;
    }
    public void setCc(int Cc){
        this.Cc=Cc;
    }
    public void setCv(int Cv){
        this.Cv=Cv;
    }
        public void setAlimentazione(String Alimentazione){
            this.Alimentazione=Alimentazione;
        }
        
    
    
}
