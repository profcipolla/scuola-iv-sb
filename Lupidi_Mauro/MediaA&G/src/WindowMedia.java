
	import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

	public class WindowMedia  extends JFrame implements ActionListener{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JTextField  casellaVuota= new JTextField("num1"); //numero1
		
		private JTextField  casellaVuotaC= new JTextField("num2");//numero2
		private JTextField risultato=new JTextField();//la casella per il risultato
		private JButton bMediaGeometrica=new JButton("Media Geometrica");//bottone per avvio media Geometrica
		private JButton bMediaAritmetica=new JButton("Media Aritmetica");//bottone per avvio media Aritmetica
		
		public WindowMedia() {
			setSize(569,600);
			setTitle("Calcolatrice Media");
			setLayout(new GridLayout(3,2));
			
			JPanel panel = new JPanel();
			panel.setBackground(Color.BLACK);
			getContentPane().add(panel);
			panel.setLayout(new GridLayout(3, 1, 0, 0));
			
			JLabel titoloLable = new JLabel("Benvenuto!");
			titoloLable.setHorizontalAlignment(SwingConstants.CENTER);
			titoloLable.setForeground(Color.RED);
			titoloLable.setBackground(Color.BLACK);
			titoloLable.setFont(new Font("Yu Gothic Light", Font.PLAIN, 27));
			panel.add(titoloLable);
		
			
			add(casellaVuota); //aggiunge i JTextField
			add(casellaVuotaC);//aggiunge i JTextField
			add(bMediaGeometrica);//aggiunge il bottone per la media Giometrica
			add(bMediaAritmetica);//aggiunge il bottone per la media Aritmetica
			bMediaAritmetica.addActionListener(this);//render� funzionante il pulsante
			bMediaGeometrica.addActionListener(this);//render� funzionante il pulsante
			
			JPanel pRisultato = new JPanel();
			pRisultato.setBackground(Color.RED);
			getContentPane().add(pRisultato);
			pRisultato.setLayout(new GridLayout(1, 0, 0, 0));
			
			JLabel lTotale=new JLabel("Risultato:");
			lTotale.setHorizontalAlignment(SwingConstants.CENTER);
			pRisultato.add(lTotale);
			
			
			add(risultato);//aggiunge il testo dove comparir� il risultato
			
			setVisible(true); //render� visibile la finestra
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//al click sulla "X" chiuder� l'applicazione e il processo
		}
		
		public void actionPerformed(ActionEvent e) {
			Object c=e.getSource();
			if(c==bMediaAritmetica) {
				Double numero1=Double.parseDouble(casellaVuota.getText());
				Double numero2=Double.parseDouble(casellaVuotaC.getText());
				Double risultato2=mediaAritmetica(numero1,numero2);
				String risultatoTotale= String.valueOf(risultato2); //converte il risultato che � in "Double" in stringa
				risultato.setText(risultatoTotale); //modifica il testo e lo fa vedere
			}else { //se invece... scegliamo la media Geometrica
				Double numero1=Double.parseDouble(casellaVuota.getText());
				Double numero2=Double.parseDouble(casellaVuotaC.getText());
				Double risultato1=mediaGeometrica(numero1,numero2);
				String risultatoTotale=String.valueOf(risultato1);
				risultato.setText(risultatoTotale);
			}
		}

		private Double mediaGeometrica(Double numero1, Double numero2) {
	       Double risultato=Math.sqrt(numero1*numero2);// Math.sqrt � la radice Quadrata, moltiplicher� il risultato e poi far� la radice Quadrata
			return risultato;
		}

		private Double mediaAritmetica(Double numero1, Double numero2) {
			Double risultato=(numero1+numero2)/2; //la media aritmetica
			return risultato; //ci deve tornare il risultato
		
		}
	public static void main(String[] args) {
			
			new WindowMedia();
		}
		
	}