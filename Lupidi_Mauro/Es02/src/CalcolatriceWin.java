

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class CalcolatriceWin {
	String [] scritte = {"7", "8", "9", "/", "4", "5", "6", "+", "1", "2", "3", "-", "0", ".", "=", "*" };
	JButton[] pulsanti = new JButton[16];
	
	public CalcolatriceWin() {
		JFrame calcWin = new JFrame();
		calcWin.setTitle("Calcolatrice");
		// calcWin.setSize(400, 500);
		
		JPanel mioPan = new JPanel();
		calcWin.add(mioPan);
		GridLayout gl = new GridLayout(4, 4, 2, 2);
		mioPan.setLayout(gl);

		for (int i = 0; i < scritte.length; i++) {
			pulsanti[i] = new JButton(scritte[i]);
			mioPan.add(pulsanti[i]);
		}
		
		calcWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		calcWin.pack();
		calcWin.setVisible(true);
	}
}