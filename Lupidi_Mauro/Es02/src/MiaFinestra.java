
import java.awt.FlowLayout;
import java.awt.LayoutManager;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MiaFinestra {

	public MiaFinestra() {
		JFrame win = new JFrame();
		
		win.setTitle("Questo � il titolo della mia prima finestra");
		win.setSize(800, 400);
		
		// gestisce la disposizione dei componenti all'interno della finestra.
		// FL � un gestore che accoda i componenti finch� c'� spazio sulla riga,
		// poi va su una nuova riga
		LayoutManager mioLayout = new FlowLayout(); // new GridLayout(3, 6)
		win.setLayout(mioLayout);
		
		JLabel l1 = new JLabel("Ecco una etichetta");
		JTextField t1 = new JTextField("Qui potete scrivere");
		
		JButton b1 = new JButton("Bottone 1");
		JButton b2 = new JButton("Bottone 2");
		JButton b3 = new JButton("Bottone 3");	
		JButton b4 = new JButton("Bottone 4");
		
		win.add(l1);
		win.add(t1);
		win.add(b1);
		win.add(b2);
		win.add(b3);
		win.add(b4);
		
		// win.pack();
		// win.size(800, 600)
		
		//win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);
	}
	
}
