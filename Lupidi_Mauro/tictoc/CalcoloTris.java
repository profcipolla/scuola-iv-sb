package it.armellini.tictoc;


public class CalcoloTris {
	
			  

	private String[] caselle = new String[9];
	int count = 0;
	static int avversario = 0;

	public CalcoloTris() {
	}
	public void selezionaCasella(int posizioneTasto) {

		caselle[posizioneTasto - 1] = "X";
		count++;
		if (count == 9) {
			return;
		}
		avversario = (int) (Math.random() * 9);
		while (caselle[avversario] != null) {
			avversario = (int) (Math.random() * 9);
		}
		caselle[avversario] = "O";
		count++;
		vincita();
	}

	public void vincita() {
		String test1 = caselle[0] + caselle[1] + caselle[2];
		if ((test1.equals("XXX")) || (test1.equals("OOO"))) {
			System.out.println("Ha vinto " + test1.substring(0, test1.length()-2));
		}
		String test2 = caselle[3] + caselle[4] + caselle[5];
		if ((test2.equals("XXX")) || (test2.equals("OOO"))) {
			System.out.println("Ha vinto " + test2.substring(0, test2.length()-2));
		}
		String test3 = caselle[6] + caselle[7] + caselle[8];
		if ((test3.equals("XXX")) || (test3.equals("OOO"))) {
			System.out.println("Ha vinto " + test3.substring(0, test3.length()-2));
		}
		String test4 = caselle[0] + caselle[3] + caselle[6];
		if ((test4.equals("XXX")) || (test4.equals("OOO"))) {
			System.out.println("Ha vinto " + test4.substring(0, test4.length()-2));
		}
		String test5 = caselle[1] + caselle[4] + caselle[7];
		if ((test5.equals("XXX")) || (test5.equals("OOO"))) {
			System.out.println("Ha vinto " + test5.substring(0, test5.length()-2));
		}
		String test6 = caselle[2] + caselle[5] + caselle[8];
		if ((test6.equals("XXX")) || (test6.equals("OOO"))) {
			System.out.println("Ha vinto " + test6.substring(0, test6.length()-2));
		}
		String test7 = caselle[0] + caselle[4] + caselle[8];
		if ((test7.equals("XXX")) || (test7.equals("OOO"))) {
			System.out.println("Ha vinto " + test7.substring(0, test7.length()-2));
		}
		String test8 = caselle[2] + caselle[4] + caselle[6];
		if ((test8.equals("XXX")) || (test8.equals("OOO"))) {
			System.out.println("Ha vinto " + test8.substring(0, test8.length()-2));
		}
	}
}