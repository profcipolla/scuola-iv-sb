package it.armellini.tictoc;



import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.Font;

public class Board extends JFrame implements ActionListener {
	CalcoloTris oP = new CalcoloTris();

	JButton Buttone1 = new JButton("0");
	JButton Buttone2 = new JButton("1");
	JButton Buttone3 = new JButton("2");
	JButton Buttone4 = new JButton("3");
	JButton Buttone5 = new JButton("4");
	JButton Buttone6 = new JButton("5");
	JButton Buttone7 = new JButton("6");
	JButton Buttone8 = new JButton("7");
	JButton Buttone9 = new JButton("8");

	public Board() {
		setBackground(Color.WHITE);

		setTitle("Tic Tac Toe");
		setVisible(true);
		setSize(400, 400);
		setLocation(400,400);
		setLayout(new GridLayout(3, 3));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Buttone1.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone1.setBackground(Color.WHITE);
		Buttone1.setForeground(Color.WHITE);
		Buttone2.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone2.setBackground(Color.WHITE);
		Buttone2.setForeground(Color.WHITE);
		Buttone3.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone3.setBackground(Color.WHITE);
		Buttone3.setForeground(Color.WHITE);
		Buttone4.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone4.setBackground(Color.WHITE);
		Buttone4.setForeground(Color.WHITE);
		Buttone5.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone5.setBackground(Color.WHITE);
		Buttone5.setForeground(Color.WHITE);
		Buttone6.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone6.setBackground(Color.WHITE);
		Buttone6.setForeground(Color.WHITE);
		Buttone7.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone7.setBackground(Color.WHITE);
		Buttone7.setForeground(Color.WHITE);
		Buttone8.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone8.setBackground(Color.WHITE);
		Buttone8.setForeground(Color.WHITE);
		Buttone9.setFont(new Font("Tahoma", Font.PLAIN, 0));
		Buttone9.setBackground(Color.WHITE);
		Buttone9.setForeground(Color.WHITE);

		add(Buttone1);
		add(Buttone2);
		add(Buttone3);
		add(Buttone4);
		add(Buttone5);
		add(Buttone6);
		add(Buttone7);
		add(Buttone8);
		add(Buttone9);
		Buttone1.addActionListener(this);
		Buttone2.addActionListener(this);
		Buttone3.addActionListener(this);
		Buttone4.addActionListener(this);
		Buttone5.addActionListener(this);
		Buttone6.addActionListener(this);
		Buttone7.addActionListener(this);
		Buttone8.addActionListener(this);
		Buttone9.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		JButton bottonePremuto = (JButton) source;
		String pulsanteText = bottonePremuto.getText();
		String computer = null;
		
		switch (pulsanteText) {
		case "0":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(1);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "1":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(2);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "2":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(3);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "3":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(4);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "4":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(5);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "5":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(6);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "6":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(7);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "7":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(8);
			computer = String.valueOf(CalcoloTris.avversario);
			break;
		case "8":
			bottonePremuto.setText("X");
			bottonePremuto.setForeground(Color.BLACK);
			bottonePremuto.setFont(new Font("Tahoma", Font.PLAIN, 80));
			oP.selezionaCasella(9);
			computer = String.valueOf(CalcoloTris.avversario);
			break;	
		} 
		
		switch (computer) {
		case "0":
			Buttone1.setText("O");
			Buttone1.setForeground(Color.BLACK);
			Buttone1.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "1":
			Buttone2.setText("O");
			Buttone2.setForeground(Color.BLACK);
			Buttone2.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "2":
			Buttone3.setText("O");
			Buttone3.setForeground(Color.BLACK);
			Buttone3.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "3":
			Buttone4.setText("O");
			Buttone4.setForeground(Color.BLACK);
			Buttone4.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "4":
			Buttone5.setText("O");
			Buttone5.setForeground(Color.BLACK);
			Buttone5.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "5":
			Buttone6.setText("O");
			Buttone6.setForeground(Color.BLACK);
			Buttone6.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "6":
			Buttone7.setText("O");
			Buttone7.setForeground(Color.BLACK);
			Buttone7.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "7":
			Buttone8.setText("O");
			Buttone8.setForeground(Color.BLACK);
			Buttone8.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		case "8":
			Buttone9.setText("O");
			Buttone9.setForeground(Color.BLACK);
			Buttone9.setFont(new Font("Tahoma", Font.PLAIN, 80));
			break;
		}
		
	
	}
}
