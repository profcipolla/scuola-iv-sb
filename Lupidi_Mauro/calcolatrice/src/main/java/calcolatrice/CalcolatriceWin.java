package calcolatrice;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalcolatriceWin extends JFrame implements ActionListener {
	private static final String[] scritte = { "C", "M", "RM", "SQRT", "7", "8", "9", "/", "4", "5", "6", "*", "1", "2",
			"3", "+", ",", "0", "=", "-" };

	private String operazioneMem = "";
	private Double operando = 0.0d;

	private JButton[] pulsanti = new JButton[20];
	private JTextField display = new JTextField(20);

	public CalcolatriceWin(String titolo) {
		setBackground(Color.orange);

		setLayout(new GridLayout(2, 1));
		add(display);
		display.setAlignmentX(RIGHT_ALIGNMENT);

		JPanel pnlPulsanti = new JPanel();
		add(pnlPulsanti);

		pnlPulsanti.setLayout(new GridLayout(5, 4));
		for (int i = 0; i < scritte.length; i++) {
			pulsanti[i] = new JButton(scritte[i]);
			pnlPulsanti.add(pulsanti[i]);

			pulsanti[i].addActionListener(this);
		}

		setTitle(titolo);
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	// TODO:
	// possibilit� di inserire un solo separatore decimale
	// sul display cifre e separatore
	// il + mette da parte il valore nel display, ripulisce il display
	// = se sono disponibili due valori mi dar� la somma
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source instanceof JButton) {
			String contenutoDisplay = display.getText();
			JButton bottonePremuto = (JButton) source;
			String pulsanteText = bottonePremuto.getText();
			switch (pulsanteText) {
			case "0":
			case "1":
			case "2":
			case "3":
			case "4":
			case "5":
			case "6":
			case "7":
			case "8":
			case "9":
				display.setText(contenutoDisplay + pulsanteText);
				break;
			case ",":
				// test se già presente la virgola, non deve aggiungerne un'altra
				if (!contenutoDisplay.contains(",")) {
					if ("".equals(contenutoDisplay)) {
						contenutoDisplay = "0";
					}
					display.setText(contenutoDisplay + pulsanteText);
				}
				break;
			default:
				this.gestisciOperazione(pulsanteText, contenutoDisplay);
			}

		} else if (source instanceof JTextField) {
			JTextField text = (JTextField) source;
			System.out.println("Hai premuto un pulsante " + text.getText());
		}

	}

	private void gestisciOperazione(String operazione, String contenutoDisplay) {
		System.out.println("Hai richiesto l'operazione " + operazione);
		switch (operazioneMem) {
		case "":
			// mette da parte il primo operando
			this.operando = Double.parseDouble(contenutoDisplay.replace(",", "."));
			this.display.setText("");
			break;
		case "+":
			this.operando = this.operando + Double.parseDouble(contenutoDisplay.replace(",", "."));
			// this.display.setText(String.format("%.6f", this.operando));
			this.display.setText("");
			break;
		case "=":
			this.display.setText(String.format("%.6f", this.operando));
			break;
		}
		
		if ("C" == operazione) {
			this.display.setText("");
			this.operazioneMem="";
			this.operando=0.0d;
		}else if ("=".contentEquals(operazione))

		operazioneMem = operazione;
	}
}
