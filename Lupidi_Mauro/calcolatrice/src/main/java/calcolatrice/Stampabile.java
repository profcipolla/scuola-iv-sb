package calcolatrice;

public interface Stampabile {
	void print();
	int numeroCopie();
}
