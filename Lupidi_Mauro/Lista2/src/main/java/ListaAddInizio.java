
public class ListaAddInizio implements ILista {
	private Nodo head = null;

	/**
	 * Aggiunge un'automobile come primo nodo della lista
	 * 
	 * @param automobile
	 */
	public void add(Automobile automobile) {
		Nodo nuovoNodo = new Nodo(automobile, this.head);
		this.head = nuovoNodo;
	}



	
	/**
	 * Inserisce l'automobile nella lista mantenendo l'ordinamento crescente
	 * per targa
	 * 
	 * @param automobile
	 */
	public void addOrdered(Automobile automobile) {
		
		
	}

	public void delete(String targa) {
		if (null == targa || "".equals(targa.trim())) {
			return;
		}
		targa = targa.trim().toUpperCase();
		
		// NB: controllo superfluo
		if (null == this.head) {
			return;
		}
		
		Nodo precNodo = null;
		Nodo corrNodo = this.head;
		while(null != corrNodo && !targa.equals(corrNodo.getInfo().getTarga())) {
			precNodo = corrNodo;
			corrNodo = corrNodo.getSucc();
		}
		
		// o mi trovo su null o sono sull'autoveicolo con targa = targa (parametro), in questo secondo caso posso effettuare
		// la cancellazione
		if (null != corrNodo) {
			if (null == precNodo) {
				this.head = this.head.getSucc();
			} else {
				// precNodo.setSucc(precNodo.getSucc().getSucc());
				precNodo.setSucc(corrNodo.getSucc());
			}
		}
		
	}

	/**
	 * 
	 * 
	 * @param targa
	 * @return
	 */
	public Automobile search(String targa) {
		/*
		while(null != corrNodo && !targa.equals(corrNodo.getInfo().getTarga())) {
			precNodo = corrNodo;
			corrNodo = corrNodo.getSucc();
		}
		*/
		return null;
	}

	/**
	 * Scrive nella console tutte le automobili presenti nella lista, a partire
	 * dalla testa
	 * 
	 */
	public void print() {
		Nodo tempPunt = this.head;

		// se ci sono elementi
		while (null != tempPunt) {
			System.out.println(tempPunt.getInfo());
			tempPunt = tempPunt.getSucc();
		}

	}
}
