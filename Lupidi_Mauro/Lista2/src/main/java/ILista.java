
public interface ILista {
	void add(Automobile auto);
	void print();
	void delete(String chiave);
	Automobile search(String chiave);
}
