package it.armellini.ivsb.anagrafica;

public class Studente extends Persona {
	public static int CONTATORE_MATRICOLA = 1;
		
	private int matricola;
	private int classe;
	private String sezione;
	private Integer[] voti = new Integer[20];
	
	
	public Studente(int classe, String sezione) {
		this.matricola = Studente.CONTATORE_MATRICOLA++;
		this.setClasse(classe);
		this.setSezione(sezione);
	}
	
	
	public void addVoto(int unVoto) {
		// TODO: aggiunge ai voti presenti il nuovo voto
	}
	
	public float calcolaMedia() {
		// TODO: in base ai voti calcola la media
		return 0.0f;
	}
	
	@Override
	public String getIdentificativo() {
		return "Matricola: " + matricola;
	}

	public int getMatricola() {
		return matricola;
	}

	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}

	public int getClasse() {
		return classe;
	}

	public void setClasse(int classe) {
		this.classe = classe;
	}

	public String getSezione() {
		return sezione;
	}
	public void setSezione(String sezione) {
		this.sezione = sezione;
	}

}