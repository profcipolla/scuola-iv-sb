package it.armellini.ivsb.anagrafica;

import java.util.Date;

public abstract class Persona {
	private String nome;
	private String cognome;
	private Date natoIl;
	
	public abstract String getIdentificativo();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getNatoIl() {
		return natoIl;
	}

	public void setNatoIl(Date natoIl) {
		this.natoIl = natoIl;
	}

	
}
