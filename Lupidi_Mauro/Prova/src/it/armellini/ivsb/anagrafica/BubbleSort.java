package it.armellini.ivsb.anagrafica;

public class BubbleSort {

	package it.armellini.ivsb.ordinamento;

	import it.armellini.ivsb.anagrafica.Professore;
	import it.armellini.ivsb.anagrafica.Studente;

	public class BubbleSort {

		/**
		 * Il bubble sort confronta due elementi adiacenti, se non sono nell'ordine
		 * corretto li scambia. Si riporta all'inizio e ripete la procedura finch� tutto
		 * l'array risulta ordinato.
		 * 
		 * @param arrayToSort
		 */
		public void sort(int[] arrayToSort) {
			int dim = arrayToSort.length;
			int conta = 0;
			for (int ripeti = 0; ripeti < dim - 1; ripeti++) {
				for (int i = 0; i < dim - 1; i++) {
					conta++;
					if (arrayToSort[i] > arrayToSort[i + 1]) {
						int appo = arrayToSort[i];
						arrayToSort[i] = arrayToSort[i + 1];
						arrayToSort[i + 1] = appo;
					}
				}
			}
			System.out.println("Ho eseguito " + conta + " confronti");
		}
		
		public void sort(Professore[] arrayToSort) {
			// TODO: ....
		}
		
		public void sort(Studente[] arrayToSort) {
			// TODO: ....
			
			//if (studente[i].mediaVoti > studente[i + 1].mediaVoti) {
				// scambio
			//}
		}
	}
	
	
	
}
