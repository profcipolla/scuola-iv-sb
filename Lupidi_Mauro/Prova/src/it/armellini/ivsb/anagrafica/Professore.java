package it.armellini.ivsb.anagrafica;

public class Professore extends Persona {

	private String codiceFiscale;
	
	public Professore(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	
	@Override
	public String getIdentificativo() {
		return codiceFiscale;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	
}
