import java.util.Date;
public class Cliente
{
    //Proprietà
   private String Nome;
   private String Cognome;
   private String NumeroTelefono;
   private String CodiceFiscale;
   private Date NatoIl;
   //costruttore
   public Cliente(String Nome, String Cognome){
   
   this.Nome=Nome;
   this.Cognome=Cognome;
   
   
}
//getter (guarda contenuto)
public String getNome(){
    return this.Nome;
}
public String getCognome(){
    return this.Cognome;
}
public String getNumeroTelefono(){
    return this.NumeroTelefono;
}
//setter (modifica contenuto)
public void setNome(String Nome){
   this.Nome=Nome;
}
public void setCognome(String Cognome){
    this.Cognome=Cognome;
}
public void setNumeroTelefono(String NumeroTelefono){
    this.NumeroTelefono=NumeroTelefono;
}



}
