import java.util.Date;


public class CasaAutomobilistica  {
    //proprietà
    private String Denominazione;
    private String Nazione;
    //costruttore
    public CasaAutomobilistica(String Denominazione){
        this.Denominazione=Denominazione;
    }
    
   //getter (guarda contenuto)
   public String getDenominazione(){
       return this.Denominazione;
    }
    public String getNazione(){
        return this.Nazione;
    }
    //setter(modifica contenuto)
    public void setDenominazione(String Denominazione){
        this.Denominazione=Denominazione;
    }
    public void setNazione(String Nazione){
        this.Nazione=Nazione;
    }
   
  
}
