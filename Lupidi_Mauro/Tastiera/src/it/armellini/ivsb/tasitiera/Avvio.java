package it.armellini.ivsb.tasitiera;

public class Avvio {

	public static void main(String[] args) {
		MiaTastiera tastiera = new MiaTastiera();
		ContoCorrente mioConto = new ContoCorrente();

		String scelta;
		do {
			stampaMenu();
			scelta = tastiera.leggiTesto();

			// decifra la scelta
			if ("v".equals(scelta)) {
				System.out.print("Inserisci un importo da versare: ");
				Long unImporto = tastiera.leggiLong();
				mioConto.versa(unImporto);
			} else if ("p".equals(scelta)) {
				System.out.print("Inserisci un importo da prelevare: ");
				Long unImporto = tastiera.leggiLong();
				mioConto.preleva(unImporto);
			} else if ("s".equals(scelta)) {
				System.out.println("Il saldo del conto ammonta a: " + mioConto.dammiSaldo());
			} else if ("m".equals(scelta)) {
				System.out.println("Gli ultimi 5 movimenti del conto sono: ");
				Movimento[] ultimiMovimenti = mioConto.dammiMovimenti();
				// ciclo sui movimenti e stampa
				for (int i = 0; i < 5; i++) {
					if (null != ultimiMovimenti[i]) {
						System.out.println(ultimiMovimenti[i]);
					}
				}
			}
		} while (!"x".equals(scelta));

	}

	public static void stampaMenu() {
		System.out.println();
		System.out.println("Operazioni possibili:");
		System.out.println("v - versamento");
		System.out.println("p - prelievo");
		System.out.println("s - saldo");
		System.out.println("m - ultimi 5 movimenti");
		System.out.println("x - uscita");
	}

}
     
}


