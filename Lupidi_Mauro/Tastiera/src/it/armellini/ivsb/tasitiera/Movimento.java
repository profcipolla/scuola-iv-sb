package it.armellini.ivsb.tasitiera;


import java.util.Date;

public class Movimento {
	private Long importo;
	private Date data;
	
	// costruttore
	public Movimento(Long importo, Date data) {
		this.setImporto(importo);
		this.setData(data);
	}
	
	// setter
	public void setImporto(Long importo) {
		this.importo = importo;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	// getter
	public Long getImporto() {
		return this.importo;
	}
	
	public Date getData() {
		return this.data;
	}

	@Override
	public String toString() {
		return "Data: " + this.data + ", importo: " + this.importo;
	}
	
	
}
