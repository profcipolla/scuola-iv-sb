package it.armellini.ivsb.tasitiera;

import java.util.Date;

/*
 	Crea un programma per la gestione di un controcorrente gestito dalla classe CC.
	Il programma deve essere in grado di effettuare versamenti, prelievi, di 
	restituire il saldo e di effettuare la lista degli ultimi 5 movimenti.
*/
public class ContoCorrente {
	
	private String numeroConto;
	private Movimento[] movimenti = new Movimento[1000];
	
	public boolean versa(Long importo) {
		int pos = 0;
		// va alla ricerca di uno spazio disponibile
		while(pos < 1000 && this.movimenti[pos] != null) {
			pos++;
		}
		if (pos < 1000) {
			// inserisce il movimento perch� ha trovato una posizione a null
			Movimento operazione = new Movimento(importo, new Date());
			movimenti[pos] = operazione;
			
			System.out.println("Hai versato " + importo);
			return true;
		} else {
			return false;
		}
	}
	
	public Long preleva(Long importo) {
		if (versa(-importo)) {
			System.out.println("Hai prelevato " + importo);
			return importo;
		} else {
			System.out.println("Non puoi prelevare");
			return 0L;
		}
	}
	
	public Long dammiSaldo() {
		Long saldo = 0L;
		int pos = 0;
		while(pos < 1000 && this.movimenti[pos] != null) {
			saldo += this.movimenti[pos].getImporto();
			pos++;
		}
		return saldo;
	}
	
	public Movimento[] dammiMovimenti() {
		Movimento[] ultimi = new Movimento[5];
		
		int conta = 0;
		int pos = 1000 - 1;
		while(pos >= 0 && conta < 5) {
			if (this.movimenti[pos] != null) {
				ultimi[conta++] = this.movimenti[pos];
			}
			pos--;
		}
		
		return ultimi;
	}
}
	


