package it.armellini.ivsb.figure;

public class Rettangolo extends Figura {
	private long base;
	private long altezza;
	
	
	
	public Rettangolo(long base, long altezza) {
		super();
		this.base = base;
		this.altezza = altezza;
	}

	@Override
	public double calcolaArea( ) {
		return base * altezza;
	}
	
	@Override
	public double calcolaPerimetro() {
		return 2.0d * (base + altezza);
	}

	public long getBase() {
		return base;
	}

	public void setBase(long base) {
		this.base = base;
	}

	public long getAltezza() {
		return altezza;
	}

	public void setAltezza(long altezza) {
		this.altezza = altezza;
	}
	
	
}