package it.armellini.ivsb.figure;

public class Triangolo extends Figura {

	private long base;
	private long lato1;
	private long lato2;
	private long altezza;

	public Triangolo(long base, long altezza, long lato1, long lato2) {
		super();
		this.base = base;
		this.lato1 = lato1;
		this.lato2 = lato2;
		this.altezza = altezza;
	}

	@Override
	public double calcolaArea() {
		return (base * altezza) / 2.0d;
	}

	@Override
	public double calcolaPerimetro() {
		return lato1 + lato2 + base;
	}

	public long getBase() {
		return base;
	}

	public void setBase(long base) {
		this.base = base;
	}

	public long getLato1() {
		return lato1;
	}

	public void setLato1(long lato1) {
		this.lato1 = lato1;
	}

	public long getLato2() {
		return lato2;
	}

	public void setLato2(long lato2) {
		this.lato2 = lato2;
	}

	public long getAltezza() {
		return altezza;
	}

	public void setAltezza(long altezza) {
		this.altezza = altezza;
	}

}