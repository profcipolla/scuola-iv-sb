package it.armellini.ivsb.CompitoExc;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MiaTastiera {
	private BufferedReader tast;
	
	public MiaTastiera() {
	
	
	InputStreamReader isr = new InputStreamReader(System.in);
	tast = new BufferedReader(isr);
	}
	
	public String leggiTesto() {
		try {
			return tast.readLine();
		} catch (IOException e) {
		}
		return "";
	}
	public Integer leggiIntero() {
        try {
            String testo = tast.readLine();
            return Integer.parseInt(testo);
        } catch (IOException e) {
    }
    return 0;
  }
  
  public Long leggiLong() {
        try {
        String testo = tast.readLine();
        return Long.parseLong(testo, 10);
        } catch (IOException e) {
    }
    return 0L;
  }
      




}



