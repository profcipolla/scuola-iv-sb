
public class Persona {
private String nome;
private String cognome;
public Persona() {	
}


public Persona(String nome, String cognome) {
	super();
	this.setNome(nome);
	this.setCognome(cognome);
}

public String getNome() {
	return nome;
}
public void setNome(String nome) {
	this.nome = nome;
}
public String getCognome() {
	return cognome;
}
public void setCognome(String cognome) {
	this.cognome = cognome;
}



}
