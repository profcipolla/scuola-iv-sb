import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FinestraLista extends JFrame implements ActionListener{
private JTextField tNome=new JTextField();
private JTextField tCognome=new JTextField();
private JButton bInizio=new JButton("Inizio");
private JButton bSuccessivo=new JButton("Successivo");
private JButton bCancella= new JButton ("Cancella");
private JButton bModifica= new JButton("Modifica");

private Lista miaLista;


	
	public FinestraLista(Lista miaLista) {
		this.miaLista=miaLista;
		this.initUi();
		this.init();
		
	}
		
	private void initUi() { //inizializzazione interfaccia utente
		
	setLayout(new GridLayout(2, 3));
	
	add(new JLabel("Nome:"));
	add(tNome);
	
	add(new JLabel("Cognome:"));
	add(tCognome);
		

	add(bInizio);
	add(bSuccessivo);
	add(bCancella);
	add(bModifica);
	
	bInizio.addActionListener(this);
	bSuccessivo.addActionListener(this);
	bCancella.addActionListener(this);
	bModifica.addActionListener(this);
	
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setVisible(true);
	this.init();
	}

private void init() { //ha il compito di prendere nella lista la prima persona
	this.miaLista.inizio();
	Persona p=miaLista.getCorrente();
	
	if (null!=p) {
		this.tNome.setText(p.getNome());
		this.tCognome.setText(p.getCognome());
		
	}

}
	public void actionPerformed(ActionEvent e) {
		JButton bottone = (JButton)e.getSource();
		
		if (bInizio.equals(bottone)) {
			//riporto la lista su inizio
			this.miaLista.inizio();
			
		}
		Persona p = miaLista.getCorrente(); //prendere il current dalla lista
		
		if(null !=p) {
			this.tNome.setText(p.getNome());
			this.tCognome.setText(p.getCognome());
		}
		if(bCancella.equals(bottone)) {
	 
	Persona persona = this.testa;
	this.miaLista.delete(persona);
		
		}	
		
	}
}
