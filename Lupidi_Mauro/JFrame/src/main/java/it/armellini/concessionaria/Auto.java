package it.armellini.concessionaria;

import java.awt.event.ActionListener;

public class Auto extends Veicolo {
	 private Integer numeroPosti;
	 private String coloreAuto;
	 private float prezzo;
	 private String casaMarchio;
	 
	
	public Auto (float prezzo,Integer numeroPosti, String coloreAuto,Float Cc, Integer Cv, String casaMarchio) {
		super(Cc,Cv);
		this.setNumeroPosti(numeroPosti);
		this.setColoreAuto(coloreAuto);
		this.setPrezzo(prezzo);
		this.setCasaMarchio(casaMarchio);
		
	}
	
	public String getCasaMarchio() {
		return casaMarchio;
	}

	public void setCasaMarchio(String casaMarchio) {
		this.casaMarchio = casaMarchio;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public Integer getNumeroPosti() {
		return numeroPosti;
	}

	public void setNumeroPosti(Integer numeroPosti) {
		this.numeroPosti = numeroPosti;
	}

	public String getColoreAuto() {
		return coloreAuto;
	}

	public void setColoreAuto(String coloreAuto) {
		this.coloreAuto = coloreAuto;
	}



}
