package it.armellini.libreria;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraAutore extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();
	JPanel panelloMain = new JPanel();
	JLabel labelMain = new JLabel("AUTORI");

	JPanel pannelloInserimentoDati = new JPanel();
	JLabel labelNome = new JLabel("Nome");
	JTextField textNome = new JTextField("Inserire il Nome");
	JLabel labelCognome = new JLabel("Cognome");
	JTextField textCognome = new JTextField("Inserire il Cognome");
	JLabel labelNazionalita = new JLabel("Nazionalità");
	JTextField textNazionalita = new JTextField("Inserire Nazionalità");
	JButton bottoneSalva = new JButton("Salva");
	Font fontA = new Font("Solomon Normal", Font.BOLD, 16);
	Font fontB = new Font("Solomon", Font.BOLD | Font.ITALIC, 11);

	public FinestraAutore() {
		setTitle("Gestione Libro");
		setSize(562, 311);
		setLocation(700, 700);
		setBackground(Color.RED);
		bottoneSalva.setForeground(new Color(0, 0, 139));

		bottoneSalva.setFont(new Font("Dialog", Font.BOLD, 24));
		bottoneSalva.setBackground(new Color(143, 188, 143));
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

		pannelloInserimentoDati.setBackground(new Color(50, 205, 50));
		pannelloInserimentoDati.setLayout(new GridLayout(2, 3));

		labelNome.setFont(fontA);// font grande
		labelCognome.setFont(fontA);

		labelNome.setHorizontalAlignment(SwingConstants.CENTER);// testo al centro
		labelCognome.setHorizontalAlignment(SwingConstants.CENTER);
		textCognome.setForeground(new Color(85, 107, 47));
		textCognome.setHorizontalAlignment(SwingConstants.CENTER);
		textNazionalita.setForeground(new Color(85, 107, 47));
		textNazionalita.setHorizontalAlignment(SwingConstants.CENTER);
		textCognome.setBackground(new Color(230, 230, 250));
		textNazionalita.setBackground(new Color(230, 230, 250));

		labelNome.setForeground(new Color(0, 0, 0));// colore testo
		labelCognome.setForeground(Color.WHITE);
		textCognome.setFont(fontB);
		textNazionalita.setFont(fontB);
		pannelloInserimentoDati.add(labelNome);// layer titolo libro
		pannelloInserimentoDati.add(labelCognome);// layer pagine
		labelNazionalita.setFont(fontA);
		labelNazionalita.setHorizontalAlignment(SwingConstants.CENTER);
		labelNazionalita.setForeground(Color.RED);
		pannelloInserimentoDati.add(labelNazionalita);// layer autore
		textNome.setForeground(new Color(85, 107, 47));

		textNome.setHorizontalAlignment(SwingConstants.CENTER);

		textNome.setBackground(new Color(230, 230, 250));

		textNome.setFont(fontB);// font piccolo
		pannelloInserimentoDati.add(textNome);// testo

		
		panelloMain.setForeground(Color.RED);

		panelloMain.setBackground(new Color(112, 128, 144));
		panelloMain.setLayout(new GridLayout(1, 1));// griglia
		labelMain.setForeground(Color.YELLOW);
		labelMain.setHorizontalAlignment(SwingConstants.CENTER);// posizionamento testo
		labelMain.setFont(new Font("Solomon", Font.BOLD, 30));// testo

		panelloMain.add(labelMain);// aggiunto Scritta al pannello Titolo
		getContentPane().add(panelloMain);
		pannelloInserimentoDati.add(textCognome);// testo pagine
		pannelloInserimentoDati.add(textNazionalita);// testo autore
		getContentPane().add(pannelloInserimentoDati);
		textNome.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				//System.out.println(((JTextField)e.getSource()).getText());
				//System.out.println(tastiera.getText());
				classeAutore.setNome(tastiera.getText());
				textNome.setForeground(new Color(0, 168, 107));
				textNome.setFont(fontA);
			}

		});
		textCognome.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				classeAutore.setCognome(tastiera.getText());
				textCognome.setForeground(new Color(0, 168, 107));
				textCognome.setFont(fontA);

			}
		});
		textNazionalita.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				classeAutore.setNazionalita(tastiera.getText());
				textNazionalita.setForeground(new Color(0, 168, 107));
				textNazionalita.setFont(fontA);

			}
		});

		getContentPane().add(bottoneSalva);
		bottoneSalva.addActionListener(this);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!textNome.getText().equals("")) {
			textNome.setText("");
			textCognome.setText("");
			textNazionalita.setText("");
			textNome.setForeground(Color.black);
			textCognome.setForeground(Color.black);
			textNazionalita.setForeground(Color.black);
			operazioni.addAutori(new Autore(classeAutore.getNome(), classeAutore.getCognome(), classeAutore.getNazionalita()));
			operazioni.stampaAutori();
		}
	}

}
