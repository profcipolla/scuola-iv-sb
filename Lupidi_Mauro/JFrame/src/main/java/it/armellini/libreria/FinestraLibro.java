package it.armellini.libreria;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FinestraLibro extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	Libro classeLibro = new Libro();
	Autore classeAutore = new Autore();
	OperazioniLibreria operazioni = new OperazioniLibreria();
	JPanel panelloMain = new JPanel();
	JLabel labelArchivio = new JLabel("Libri in Archivio");

	JPanel pannelloInserimentoDati = new JPanel();
	JLabel labelTitolo = new JLabel("Titolo Libro");
	JTextField textTitolo = new JTextField("Inserire il titolo");
	JLabel labelNumeroPag = new JLabel("Pagine");
	JTextField textNumeroPag = new JTextField("Numero pagine");
	JButton bottoneSalva = new JButton("Salva");
	Font fontA = new Font("Solomon Normal", Font.BOLD, 16);
	Font fontB = new Font("Solomon", Font.BOLD | Font.ITALIC, 11);

	public FinestraLibro() {
		setTitle("Archivio Libro");
		setSize(525, 336);
		setLocation(700, 700);
		setLayout(new GridLayout(0, 1, 0, 0));
		setBackground(Color.gray);	

		panelloMain.setBackground(new Color(0, 0, 255));
		panelloMain.setLayout(new GridLayout(1, 1));// griglia
		labelArchivio.setForeground(new Color(0, 250, 154));
		labelArchivio.setHorizontalAlignment(SwingConstants.CENTER);// posizionamento testo
		labelArchivio.setFont(new Font("Solomon", Font.BOLD, 30));// testo

		panelloMain.add(labelArchivio);// aggiunto Scritta al pannello Titolo
		getContentPane().add(panelloMain);

		pannelloInserimentoDati.setBackground(new Color(240, 230, 140));
		pannelloInserimentoDati.setLayout(new GridLayout(2, 2));

		labelTitolo.setFont(fontA);// font grande

		labelTitolo.setHorizontalAlignment(SwingConstants.CENTER);// testo al centro
		textTitolo.setForeground(new Color(0, 0, 0));

		textTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		textNumeroPag.setForeground(new Color(0, 0, 0));
		textNumeroPag.setHorizontalAlignment(SwingConstants.CENTER);

		textTitolo.setBackground(new Color(250, 235, 215));
		textNumeroPag.setBackground(new Color(250, 235, 215));

		labelTitolo.setForeground(new Color(0, 191, 255));// colore testo

		textTitolo.setFont(fontB);// font piccolo
		textNumeroPag.setFont(fontB);
		pannelloInserimentoDati.add(labelTitolo);// layer titolo libro
		labelNumeroPag.setFont(fontA);
		labelNumeroPag.setHorizontalAlignment(SwingConstants.CENTER);
		labelNumeroPag.setForeground(new Color(0, 191, 255));
		pannelloInserimentoDati.add(labelNumeroPag);// layer pagine
		pannelloInserimentoDati.add(textTitolo);// testo
		pannelloInserimentoDati.add(textNumeroPag);
		getContentPane().add(pannelloInserimentoDati);
		bottoneSalva.setFont(fontA);
		bottoneSalva.setBackground(new Color(233, 116, 81));
		textTitolo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				// System.out.println(((JTextField)e.getSource()).getText());
				// System.out.println(tastiera.getText());
				classeLibro.setTitolo(tastiera.getText());
				textTitolo.setForeground(new Color(0, 168, 107));
				textTitolo.setFont(fontA);
			}

		});

		textNumeroPag.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				JTextField tastiera = (JTextField) source;
				Integer pagine = Integer.parseInt(tastiera.getText());
				classeLibro.setPagine(pagine);
				textNumeroPag.setForeground(new Color(0, 168, 107));
				textNumeroPag.setFont(fontA);

			}
		});

		getContentPane().add(bottoneSalva);
		bottoneSalva.addActionListener(this);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!textTitolo.getText().equals("")) {
			textTitolo.setText("");
			textNumeroPag.setText("");
			textTitolo.setForeground(Color.black);
			textNumeroPag.setForeground(Color.black);
			operazioni.addLibro(new Libro(classeLibro.getTitolo(), classeLibro.getPagine()));
			operazioni.stampaLibri();
		}
	}

}
