package it.armellini.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import it.armellini.calcolatrice.CalcolatriceWin;
import it.armellini.libreria.FinestraAutore;
import it.armellini.libreria.FinestraLibro;

public class MainWin extends JFrame implements ActionListener {
	private JMenuItem iCalcolatrice = new JMenuItem("Calcolatrice");
	private JMenuItem iMenuVariabile = new JMenuItem("Acceso");

	public MainWin() {
		setTitle("Finestra Principale");
		setSize(291, 342);

		JMenuBar mBar = new JMenuBar();
		setJMenuBar(mBar);
		JMenu mFile = new JMenu("File");
		JMenu mApplicazioni = new JMenu("Applicazioni");
		JMenu mInformazioni = new JMenu("Informazioni");
		mBar.add(mFile);
		mBar.add(mApplicazioni);
		mBar.add(mInformazioni);

		JMenuItem iApriFile = new JMenuItem("Apri File");
		JMenuItem iExit = new JMenuItem("Esci");
		mFile.add(iMenuVariabile);
		iMenuVariabile.addActionListener(this);

		mFile.add(iApriFile);
		mFile.addSeparator();
		mFile.add(iExit);

		JMenuItem iAnagrafica = new JMenuItem("Anagrafica");
		JMenuItem iContoCorrente = new JMenuItem("Conto Corrente");
		JMenuItem iConcessionario = new JMenuItem("Concessionario");
		mApplicazioni.add(iAnagrafica);
		mApplicazioni.add(iContoCorrente);
		mApplicazioni.add(iConcessionario);
		
		
		JMenu mBiblioteca = new JMenu("Biblioteca");
		mApplicazioni.add(mBiblioteca);
		JMenuItem mLibro = new JMenuItem("Inserisci Libro");
		mBiblioteca.add(mLibro);
		JMenuItem mAutore = new JMenuItem("Inserisci Autore");
		mBiblioteca.add(mAutore);
		
		mLibro.addActionListener(this);
		mAutore.addActionListener(this);

		mApplicazioni.add(iCalcolatrice);
		iCalcolatrice.addActionListener(this);

		mApplicazioni.addSeparator();
		JMenu mFuture = new JMenu("Applicazioni Future");
		mApplicazioni.add(mFuture);
		mFuture.add(new JMenuItem("Super Mario Bross"));
		mFuture.add(new JMenuItem("Doom"));

		JMenuItem iAbout = new JMenuItem("About");
		mInformazioni.add(iAbout);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem menuSelezionato = (JMenuItem) e.getSource();

		switch (menuSelezionato.getText()) {
		case "Acceso":
			iMenuVariabile.setText("Spento");
			break;
		case "Inserisci Libro":
			new FinestraLibro();
			break;
		case "Inserisci Autore":
			new FinestraAutore();
			break;		
		case "Calcolatrice":
			new CalcolatriceWin("Prima Calcolatrice");
			break;
		case "Spento":
			iMenuVariabile.setText("Acceso");
			break;
		}

	}

}
