
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class HanoiWin extends JFrame {
   HanoiModel hm= new HanoiModel();
	private JButton[] colonnaSinistra= new JButton[hm.DISCHI];
	private JButton[] colonnaCentrale= new JButton[hm.DISCHI];
	private JButton[] colonnaDestra=new JButton[hm.DISCHI];
    JPanel Pulsanti = new JPanel();
    
    public HanoiWin() {
    	
    	setTitle("Hanoi! Lupidi");
    	setSize(400, 400);
    	setLayout(new GridLayout(1,1));
    
    	Pulsanti.setLayout(new GridLayout(hm.DISCHI, 3));
    	for (int i=0; i< colonnaSinistra.length; i++) {
    		colonnaSinistra[i]= new JButton();
    		colonnaCentrale[i]= new JButton();
    		colonnaDestra[i]= new JButton();
    		Pulsanti.add(colonnaSinistra[i]);
    		Pulsanti.add(colonnaCentrale[i]);
    		Pulsanti.add(colonnaDestra[i]);
    	}
    	add(Pulsanti);
    	setVisible(true);
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    		
    		
    		
    	}
	

// nel costruttore fa un grid layout di DISCHI righe e 3 colonne
	// in ogni cella mette un jbutton
	
	public void disegna(int[] sinistra, int[] centro, int[] destra) {
		// riscrive le caption nei jbutton per simulare la nuova
		// situazione dei dischi
	      for(int i = 0; i<colonnaSinistra.length; i++) {
	    	  if (sinistra[i] !=0) {
	    		  colonnaSinistra[i].setText(""+ sinistra[i]);
	    		  colonnaSinistra[i].setBackground(Color.WHITE);
	    		  colonnaSinistra[i].setForeground(Color.BLACK);
	    	 }else  {
	    		  colonnaSinistra[i].setText("");
	    		  colonnaSinistra[i].setBackground(Color.BLACK);
	    		  colonnaSinistra[i].setForeground(Color.BLACK);
	      }
	    	 
		    	  if (centro[i] !=0) {
		    		  colonnaCentrale[i].setText(""+ centro[i]);
		    		  colonnaCentrale[i].setBackground(Color.WHITE);
		    		  colonnaCentrale[i].setForeground(Color.BLACK);
		    	 }else  {
		    		 colonnaCentrale[i].setText("");
		    		 colonnaCentrale[i].setBackground(Color.BLACK);
		    		 colonnaCentrale[i].setForeground(Color.BLACK);
		    	 }
		    	  if (destra[i] !=0) {
		    		  colonnaDestra[i].setText(""+ destra[i]);
		    		  colonnaDestra[i].setBackground(Color.WHITE);
		    		  colonnaDestra[i].setForeground(Color.BLACK);
		    	 }else  {
		    		  colonnaDestra[i].setText("");
		    		  colonnaDestra[i].setBackground(Color.BLACK);
		    		  colonnaDestra[i].setForeground(Color.BLACK);
	
	
	
	
		    }
	
		}
	
	
	}
		
	
}
