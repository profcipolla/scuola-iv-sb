package it.armellini.ivsb.CompitoExc;

public class Persona  {
	
	private String nome;
	
	private String cognome;
	
	private Integer eta;
	

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public Integer getEta() {
		return eta;
	}

	public void setNome(String nome) throws Exception {
		if (null == nome || "".equals(nome.trim())) {
			throw new NomeException("Il nome non � valido");
		}
		this.nome = nome.trim();
	}

	public void setCognome(String cognome) throws Exception {
		if (null == cognome || "".equals(cognome.trim())) {
			throw new CognomeException("Il cognome non e' valido");
		}
		this.cognome = cognome.trim();
	}

	public void setEta(Integer eta) throws Exception {
		if (eta > 120 || eta<1) {
			throw new EtaException("L'eta non � valida");
		}
		this.eta = eta;
	}
    @Override
	public String toString() {
		return "Name: " + nome +"  "+ "Surname: " + cognome +"  "+ "Et�: " + eta;
	}

}
