package it.armellini.ivsb.concessionaria;

public class Motore extends Veicolo{
	private int cilindri;
	private int cc;
	private int cv;
	private String Alimentazione;
	
	public Motore(int cc, int cv) {
		this.cc=cc;
		this.cv=cv;
	}

	public int getCilindri() {
		return cilindri;
	}

	public void setCilindri(int cilindri) {
		this.cilindri = cilindri;
	}

	public int getCc() {
		return cc;
	}

	public void setCc(int cc) {
		this.cc = cc;
	}

	public int getCv() {
		return cv;
	}

	public void setCv(int cv) {
		this.cv = cv;
	}

	public String getAlimentazione() {
		return Alimentazione;
	}

	public void setAlimentazione(String alimentazione) {
		Alimentazione = alimentazione;
	}






}
