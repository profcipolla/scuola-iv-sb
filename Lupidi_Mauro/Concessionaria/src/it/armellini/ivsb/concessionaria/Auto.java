package it.armellini.ivsb.concessionaria;

public class Auto extends Veicolo {
	
	
	private String colore;
	private Integer NumeroPosti;
	
	
	
	
	public Auto (String colore, Integer NumeroPosti) {
		this.colore=colore;
		this.NumeroPosti=NumeroPosti;
		
	}




	public String getColore() {
		return colore;
	}




	public void setColore(String colore) {
		this.colore = colore;
	}




	public Integer getNumeroPosti() {
		return NumeroPosti;
	}




	public void setNumeroPosti(Integer numeroPosti) {
		NumeroPosti = numeroPosti;
	}
	
	
	
	
	
	

}
