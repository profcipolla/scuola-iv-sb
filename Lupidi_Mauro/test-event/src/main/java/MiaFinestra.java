import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class MiaFinestra extends JFrame {
	private JButton bSaluto = new JButton("Saluto");
	private JTextField tNome = new JTextField(60);
	
	
	public MiaFinestra() {
		setTitle("Prova evento click");
		
		
		setLayout(new FlowLayout());
		add(tNome);
		add(bSaluto);
		bSaluto.addActionListener(new SalutoListener());
		
		setSize(800, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setVisible(true);
	}
	
	
	public static void main(String[] a) {
		new MiaFinestra();
	}
}
