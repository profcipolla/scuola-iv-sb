package it.armellini.ivsb;

import it.armellini.ivsb.anagrafica.Persona;
import it.armellini.ivsb.util.MiaTastiera;

public class Avvio {
	public static void main(String[] a) {
		Persona p = new Persona();
		MiaTastiera kb = new MiaTastiera();
		
		
		boolean isInputCorretto = false;
		
		// lettura del nome, richiedere il nome all'utente finché non è valido
		// (il setter non ritorna una exception
		do {
			System.out.print("Per favore inserisci un nome: ");
			String nome = kb.leggiTesto();
			try {
				p.setNome(nome);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while(!isInputCorretto);
		
		
		// lettura del cognome, richiedere finché non è valido, ovvero il metodo setter non
		// ritorna un valore valido
		do {
			System.out.print("Per favore inserisci un cognome: ");
			String cognome = kb.leggiTesto();
			try {
				p.setCognome(cognome);
				isInputCorretto = true;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} while(!isInputCorretto);
		
		// lettura della eta, costringo l'utente ad inserire finché non inserisce un valore valido
		
		
		
		
		// scrivere il valori inseriti println del toString di persona
	}
}
