package it.armellini.ivsb.anagrafica;

import lombok.Data;

@Data
public class Persona {
	// nome diverso da vuoto
	private String nome;
	
	// cognome diverso da vuoto
	private String cognome;
	
	// eta > 0 e < 120
	private Integer eta;
		
	public void setNome(String nome) throws Exception {
		if (null == nome || "".equals(nome.trim())) {
			throw new Exception("Il nome non è valido");
		}
		this.nome = nome;
	}
}
