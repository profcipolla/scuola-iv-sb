package tabella.anagrafica;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String cognome;
	private String telefono;

	public Persona(String rigaFile) {
		String[] items = rigaFile.split("\t");
		setNome(items[0]);
		setCognome(items[1]);
		setTelefono(items[2]);
	}
	
	public String toString() {
		return String.format("%s\t%s\t%s", this.nome, this.cognome, this.telefono);
	}
}
