import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Studente implements Ricercabile  {
	private String nome;
	private String cognome;
	private Integer matricola;
	
	public Integer getKey() {
		return this.matricola;
	}
}
