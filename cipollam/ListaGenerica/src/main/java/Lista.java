
public class Lista<T extends Ricercabile, Key> {
	private Nodo<T> head = null;
	
	public void add (T info) {
		Nodo<T> nn = new Nodo<T>(info, this.head);
		this.head = nn;
	}
	
	public T search(Key key) {
		if (null == this.head) {
			return null;
		}
		
		Nodo<T> temp = this.head;
		
		while (null != temp && !temp.getInfo().getKey().equals(key)) {
			temp = temp.getSucc();
		}
		
		// se trovato restituisco l'oggetto
		if (null != temp) {
			return temp.getInfo();
		}
		return null;
	}
}
