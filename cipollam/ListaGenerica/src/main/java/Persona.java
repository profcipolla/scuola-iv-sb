import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Persona implements Ricercabile {
	private String nome;
	private String cognome;
	private String codiceFiscale;

	public String getKey() {
		return this.codiceFiscale;
	}
}
