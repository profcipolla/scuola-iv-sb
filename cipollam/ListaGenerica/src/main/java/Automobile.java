import lombok.Data;

@Data
public class Automobile implements Ricercabile {

	private int anno;
	private int cilindrata;
	private String modello;
	private String targa;
	
	public String getKey() {
		return this.targa;
	}

}
