package rubrica.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Persona {
	private String nome;
	private String cognome;
	private String telefono;
	
	public Persona(String allData) {
		String[] dataPart = allData.split("\t");
		this.setNome(dataPart[0]);
		this.setCognome(dataPart[1]);
		this.setTelefono(dataPart[2]);
	}
}
