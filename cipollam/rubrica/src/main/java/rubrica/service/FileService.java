package rubrica.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

import rubrica.model.Persona;

public class FileService {
	private final String NOME_FILE = "rubrica.txt";
	
	public List<Persona> leggiDati() throws FileNotFoundException, IOException  {
		List<Persona> listaPersone = new Vector<Persona>();

		// apro il file
		try (BufferedReader fbr = new BufferedReader(new FileReader(NOME_FILE))) {
			// ciclo riga per riga, e creo la persona da inserire nella lista
			String riga = fbr.readLine();
			while (null != riga) {
				listaPersone.add(new Persona(riga));
				riga = fbr.readLine();
			}
			
			// ci pensa il try()
			// fbr.close();
		}

		return listaPersone;
	}
}
