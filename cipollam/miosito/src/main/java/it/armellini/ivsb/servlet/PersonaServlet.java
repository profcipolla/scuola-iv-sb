package it.armellini.ivsb.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import it.armellini.ivsb.model.JsonResponse;
import it.armellini.ivsb.model.Persona;
import it.armellini.ivsb.service.FileService;

@WebServlet(urlPatterns = "/persona")
public class PersonaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static FileService fileService = new FileService();
	private static Gson gson = new Gson();

	public PersonaServlet() {
		super();
		System.out.println("Wildfly ha costruito PersonaServlet");
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json");
		PrintWriter out = resp.getWriter();
		List<Persona> elencoPersone = new ArrayList<Persona>();

		String telefono = req.getParameter("telefono");
		try {
			if (!StringUtils.isAllBlank(telefono)) {
				elencoPersone = fileService.searchByTelefono(telefono);
			} else {
				String idClasse = req.getParameter("idClasse");
				elencoPersone = fileService.searchByClasse(idClasse);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		out.print(gson.toJson(elencoPersone));
		out.flush();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json");
		String delete = req.getParameter("delete");
		String strPersona = req.getParameter("datiPersona");
	
		String strResponse;
		try {
			Persona personaRequest = gson.fromJson(strPersona, Persona.class);
			if ("S".equalsIgnoreCase(delete)) {
				fileService.cancella(personaRequest.getTelefono());
			} else {
				fileService.salva(personaRequest);
			}
			strResponse = gson.toJson(new JsonResponse("informazione", "Dati aggiornati correttamente", ""));
		} catch (Exception exc) {
			strResponse = gson.toJson(new JsonResponse("errore",
					"Durante l'aggiornamento dei dati si è verificato un errore.", exc.getMessage()));
		}

		PrintWriter out = resp.getWriter();
		out.print(strResponse);
		out.flush();
		out.close();
	}

}
