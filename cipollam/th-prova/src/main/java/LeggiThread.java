import java.util.Random;

public class LeggiThread extends Thread {

	private String nome;
	private Integer ritardo;
	
	private RisorsaCondivisa risorsaCondivisa;
	
	public LeggiThread(String nome, Integer ritardo, RisorsaCondivisa risorsaCondivisa) {
		this.nome = nome;
		this.ritardo = ritardo;
		this.risorsaCondivisa = risorsaCondivisa;
	}
	
	public void run() {
		Random rnd = new Random();
		Integer conta = 0;

		while (++conta <= 10) {
			try {
				Thread.sleep(rnd.nextInt(ritardo * 1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			Integer hoLetto = this.risorsaCondivisa.get();
			
			System.out.println(this.nome + " - get: " + hoLetto);
		}
	}
}
