import java.util.Random;

public class MioThread extends Thread {

	private String nome;
	private Integer ritardo;
	private Integer conta;
	
	public MioThread(String nome, Integer ritardo) {
		this.nome = nome;
		this.ritardo = ritardo;
		this.conta = 0;
	}
	
	public void run() {
		Random rnd = new Random();
		
		// qui il codice eseguito quando si invoca start() sull'oggetto istanziato dalla classe MioThread
		while (++this.conta <= 100) {
			try {
				Thread.sleep(rnd.nextInt(ritardo * 1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(this.nome + " - conta: " + this.conta);
		}
	}
}
