
public class RisorsaCondivisa {
	private Integer valore;
	private boolean disponibile = false;
	
	
	public synchronized Integer get() {
		while (!this.disponibile) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Integer daRestituire = this.valore;
		this.disponibile = false;
		notifyAll();
		return daRestituire;
	}
	
	public synchronized void push(Integer valore) {
		while(this.disponibile) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.valore = valore;
		this.disponibile = true;
		notifyAll();
	}
}
