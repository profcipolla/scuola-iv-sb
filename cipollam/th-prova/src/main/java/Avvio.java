
public class Avvio {

	public static void main(String[] args) {
		/*
		MioThread th1 = new MioThread("PIPPO", 2);
		MioThread th2 = new MioThread("QUI", 3);
		MioThread th3 = new MioThread("PAPERINO", 1);
		
		
		
		th1.start();
		th2.start();
		th3.start();
		
		System.out.println("Fine del main");
		*/
		
		RisorsaCondivisa miaRisorsa = new RisorsaCondivisa();
		LeggiThread lth = new LeggiThread("MATTIA", 2, miaRisorsa);
		ScriviThread sth = new ScriviThread("KAFKA", 1, miaRisorsa);
		
		sth.start();
		lth.start();
	}

}
