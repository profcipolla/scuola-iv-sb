package it.armellini.ivsb.listaswing.struttura;

import it.armellini.ivsb.listaswing.anagrafica.Persona;

public class ListaPersona {
	private int conta = 0;
	private NodoPersona testa = null;
	private NodoPersona current = null;

	public void add(Persona persona) {
		this.testa = new NodoPersona(persona, this.testa, ++this.conta);
	}

	public void inizio() {
		this.current = this.testa;
	}

	public Persona getCorrente() {
		if (null == this.current) {
			this.inizio();
		}

		Persona p = null;
		if (null != this.current) {
			p = this.current.getPersona();
			this.current = this.current.getSucc();
		}
		return p;
	}
	
	public PersonaConProgressivo getCorrenteConProgressivo() {
		if (null == this.current) {
			this.inizio();
		}

		PersonaConProgressivo pcp = null;
		if (null != this.current) {
			pcp = new PersonaConProgressivo(this.current.getPersona(), this.current.getProgressivo());
			this.current = this.current.getSucc();
		}
		return pcp;
	}
}
