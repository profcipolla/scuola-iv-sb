package it.armellini.ivsb.listaswing.struttura;

import it.armellini.ivsb.listaswing.anagrafica.Persona;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NodoPersona {
	private Persona persona;
	private NodoPersona succ;
	private int progressivo;
}
