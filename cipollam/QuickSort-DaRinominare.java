
public class Avvio {
	
	
	// Gli algoritmi classici, basati sul confronto diretto dei valori da ordinare, richiedono, nel caso medio,
	// un tempo O(n^2)
	
	// Se divido l'array in due e ordino separatamente le due metà quale sara i costo dei due algoritmi d'ordinamento?
	// Ciascuno costerà O((n/2)^2) = O((n^2)/4) ovvero complessivamente 2/4 * O(n^2) = 1/2 O(n^2) che è molto
	// meno del valore inizale.
	// Si può ripetere lo stesso ragionamento sulle due parti precedenti, e così via
	
	// 1/2 n^2
	// 1/4
	// 1/8
	// 1/16...
	// O(n * lg n)
	
	// Alcuni confronti fra 
	// n						10       100        1000           1000000
	// O(n^2)  					100      10000      1000000        1000000000000
	// O(n * lg n)              40       700        10000          20000000
	public static void main(String[] a) {
		// int[] array = new int[] { 88, 7, 14, 120, 400, 723, 8, 1, 4, 76 };
		int[] array = new int[] { 1, 2, 3, 5, 7, 44, 88, 100, 120, 888 };
		quickSort(array);
	}
	
	public static void quickSort(int[] arr) {
		partizionamento(arr, 0, arr.length-1);
	}
	
	private static void partizionamento(int[] arr, int daIndice, int aIndice) {

		if (daIndice >= aIndice) {
			return;
		}
		
		// scelta del pivot (prendo il primo elemento del sottoarray)
		int pivot = arr[daIndice];
		int inf = daIndice + 1;
		int sup = aIndice;
		
		while (inf <= sup) {
			// ricerca un elemento a destra che sia più piccolo (o uguale) del pivot
			while(arr[sup] > pivot) {
				sup--;
			}
			
			// ricerca un elemento a sinistra che sia più grande del pivot
			while(inf <= sup && arr[inf] <= pivot) {
				inf++;
			}
			
			// se ha trovato i due numeri fuori posto, effettua lo scambio
			if (inf < sup) {
				int appo = arr[inf];
				arr[inf] = arr[sup];
				arr[sup] = appo;
				sup--;
				inf++;
			}
		}
		
		// scambia il pivot con l'elemento in posizione centrale, in questo modo il pivot si troverà
		// nella sua posizione definitiva, quelle che gli compete ad array ordinato
		if (daIndice != sup) {
			arr[daIndice] = arr[sup];
			arr[sup] = pivot;
		}
		// ripete la procedura nella parte dell'array a sinistra del pivot 
		partizionamento(arr, daIndice, sup - 1);
		// ripete la procedura nella parte dell'array che è all destra del pivot
		partizionamento(arr, sup + 1, aIndice);
	}
}
