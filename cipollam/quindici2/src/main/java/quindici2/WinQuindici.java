package quindici2;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class WinQuindici extends JFrame implements ActionListener {
	private final static int DIM = 8;
	private final static int DIM_QUADRO = DIM * DIM;

	private JButton[] btnGioco = new JButton[WinQuindici.DIM_QUADRO];
	private int[] posLibera = { DIM - 1, DIM - 1 };

	public WinQuindici() {
		this.init();
		this.resetGioco();
	}

	private void init() {

		setLayout(new GridLayout(DIM, DIM));
		for (int i = 0; i < DIM_QUADRO; i++) {
			btnGioco[i] = new JButton();
			add(btnGioco[i]);
			btnGioco[i].addActionListener(this);
		}
		
		setTitle(String.format("Gioco del %d", WinQuindici.DIM_QUADRO - 1));
		setSize(600, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void resetGioco() {
		int[] numeri = new int[DIM_QUADRO - 1];
		for (int i = 0; i < DIM_QUADRO - 1; i++) {
			numeri[i] = i + 1;
		}

		int scambi = (int) (Math.random() * 1000.0) + 100;
		for (int i = 0; i <= scambi; i++) {
			int pos = (int) (Math.random() * (DIM_QUADRO - 1));
			int appo = numeri[0];
			numeri[0] = numeri[pos];
			numeri[pos] = appo;
		}

		while (!checkInversioni(numeri)) {
			int pos = (int) (Math.random() * (DIM_QUADRO - 1));
			int appo = numeri[0];
			numeri[0] = numeri[pos];
			numeri[pos] = appo;
		}

		for (int i = 0; i < DIM_QUADRO - 1; i++) {
			btnGioco[i].setText("" + numeri[i]);
		}
		btnGioco[DIM_QUADRO - 1].setText("");
		posLibera = new int[] { DIM - 1, DIM - 1 };
	}

	private boolean checkInversioni(int[] numeri) {
		int contaInversioni = 0;

		for (int i = 0; i < numeri.length - 1; i++) {
			for (int j = i + 1; j < numeri.length; j++) {
				if (numeri[i] > numeri[j]) {
					contaInversioni++;
				}
			}
		}

		return 0 == contaInversioni % 2;
	}

	public void actionPerformed(ActionEvent e) {
		 JButton btnClicked = (JButton)e.getSource();
		 int[] posClicked = getCoordinate(btnClicked);
		 System.out.println(String.format("Hai cliccato su %d %d", posClicked[0], posClicked[1]));
		 
		 muovi(posClicked);
		 
	}
	
	private void muovi(int[] posClicked) {
		int row = posLibera[0];
		int col = posLibera[1];
		
		if (row == posClicked[0]) {
			System.out.println("Spostamento lungo la riga");
			int toCol = posClicked[1];
			int delta = col > toCol ? -1 : 1;
			while(col != toCol) {
				copiaEtichetta(new int[] {row, col + delta} , new int[] {row, col});
				col += delta;
			}
			
			// aggiorna la posizione libera e cancella il numero
			posLibera[1] = col;
			svuota(posLibera);
		} else if (col == posClicked[1]) {
			System.out.println("Spostamento lungo la colonna");
		}
	}
	
	private void copiaEtichetta(int[] da, int[] a) {
		int indiceDa = this.getIndex(da);
		int indiceA = this.getIndex(a);
		
		String numero = this.btnGioco[indiceDa].getText();
		this.btnGioco[indiceA].setText(numero);
	}
	
	private void svuota(int[] pos) {
		int indicePos = this.getIndex(pos);
		
		this.btnGioco[indicePos].setText("");
	}
	
	/**
	 * Riceve in input il bottone e restituisce le coordinate sulla griglia
	 * 
	 * @param btnClicked il bottone
	 * @return restituisce le coordinate (riga, colonna) del bottone sul gridlayout
	 */
	private int[] getCoordinate(JButton btnClicked) {
		for (int r = 0; r < DIM; r++) {
			for (int c = 0; c < DIM; c++) {
				int indice = getIndex (new int[] {r, c} );
				if (btnClicked == this.btnGioco[indice]) {
					return new int[] {r, c};
				}
			}
		}
		
		
		return null;
	}
	
	private int getIndex(int[] coordinate) {
		return coordinate[0] * DIM + coordinate[1];
	}

}
