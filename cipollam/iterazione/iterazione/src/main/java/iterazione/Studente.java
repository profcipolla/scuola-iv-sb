package iterazione;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Studente {
	private String nome;
	private String cognome;
}
