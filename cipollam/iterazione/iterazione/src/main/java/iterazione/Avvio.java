package iterazione;

import java.util.Iterator;

public class Avvio {

	public static void main(String[] args) throws Exception {
		Classe miaClasse = new Classe();
		miaClasse.add(new Studente("Mario", "Rossi"));
		miaClasse.add(new Studente("Luigi", "Bianchi"));
		
		Iterator<Studente> ci = miaClasse.iterator();
		while(ci.hasNext()) {
			System.out.println(ci.next());
		}
		
		for(Studente s: miaClasse) {
			System.out.println(s);
		}
	}

}
