package iterazione;

import java.util.Iterator;

public class Classe implements Iterable<Studente> {
	private Studente[] studenti = new Studente[50];

	public Iterator<Studente> iterator() {
		return new ClasseIterator(this.studenti);
	}
	
	public void add(Studente studente) throws Exception {
		// && è true se e solo se tutte e due le proposizioni sono vere
		// || è true se almeno una delle due è vera
		int i = 0;
		while(i < 50 && null != this.studenti[i]) {
			i++;
		}
		
		if (i < 50) {
			this.studenti[i] = studente;
		} else {
			throw new Exception("La classe è piena");
		}
	}

}
