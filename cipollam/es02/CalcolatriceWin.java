package es02;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class CalcolatriceWin {
	String[] scritte = { "7", "8", "9", "/", "4", "5", "6", "+", "1", "2", "3", "-", "0", ".", "=", "*" };
	JButton[] pulsanti = new JButton[16];

	public CalcolatriceWin() {
		JFrame calcWin = new JFrame();
		calcWin.setTitle("Calcolatrice");
		calcWin.setLayout(new GridLayout(1, 1, 4, 4));

		// calcWin.setSize(400, 500);

		JPanel mioPan = new JPanel();
		mioPan.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.gray));
		calcWin.add(mioPan);
		GridLayout gl = new GridLayout(4, 4, 2, 2);
		mioPan.setLayout(gl);

		for (int i = 0; i < scritte.length; i++) {
			pulsanti[i] = new JButton(scritte[i]);
			pulsanti[i].setFont(new Font("Verdana", Font.BOLD, 26));
			mioPan.add(pulsanti[i]);
		}

		calcWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		calcWin.pack();
		calcWin.setVisible(true);
	}
}
