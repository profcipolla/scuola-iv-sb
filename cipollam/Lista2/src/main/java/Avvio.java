import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Avvio {

	public static void main(String[] args) {

		ILista miaLista = new ListaAddInOrdine();
		try {
			Automobile lamborghini = new Automobile("RM0004", 2000, "Lamborghini Miura");
			miaLista.add(lamborghini);
			miaLista.add(new Automobile("RM0001", 1999, "Fiat Punto"));
			miaLista.add(new Automobile("RM0002", 1999, "Fiat Panda"));
			Automobile ferrari = new Automobile("RM0003", 2010, "Ferrari Testarossa");
			miaLista.add(ferrari);

			Automobile nuovaFerrari = new Automobile("ZG0003", 2011, "Ferrari Testarossa");
			miaLista.add(nuovaFerrari);
			
			Automobile lamborghini2 = new Automobile("ST0004", 2004, "Lamborghini Miura");
			miaLista.add(lamborghini2);
			
			miaLista.print();
			miaLista.print();
			miaLista.print();


			miaLista.delete("FR000");
			miaLista.print();
			miaLista.delete("RM0003");
			miaLista.print();
			miaLista.delete("RM0004");
			miaLista.delete("RM0001");
			miaLista.print();
		} catch (Exception exc) {
			System.out.println("Si è verificato l'errore " + exc.getMessage());
		}
	}
}
