
public class Nodo {
	private Automobile info;
	private Nodo succ;
	
	public Nodo(Automobile automobile, Nodo succ) {
		this.info = automobile;
		this.succ = succ;
	}

	public Automobile getInfo() {
		return info;
	}

	public void setInfo(Automobile info) {
		this.info = info;
	}

	public Nodo getSucc() {
		return succ;
	}

	public void setSucc(Nodo succ) {
		this.succ = succ;
	}
	
}
