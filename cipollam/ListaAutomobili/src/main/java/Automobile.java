
public class Automobile {
	private String targa;
	private String modello;
	private int anno;

	public Automobile(String targa, int anno, String modello) throws Exception {
		this.setTarga(targa);
		this.setModello(modello);
		this.setAnno(anno);
	}

	public String getTarga() {
		return this.targa;
	}

	public String getModello() {
		return this.modello;
	}

	public int getAnno() {
		return this.anno;
	}

	public void setTarga(String targa) throws Exception {
		if (null == targa || 0 == targa.trim().length()) {
			throw new Exception("Targa non valida");
		}
		this.targa = targa.trim().toUpperCase();
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public void setAnno(int anno) throws Exception {
		if (anno < 1880) {
			throw new Exception("Anno di costruzione non valido");
		}
		this.anno = anno;
	}

	@Override
	public String toString() {
		return String.format("Targa: %s, Modello: %s, Anno: %d", this.targa, this.modello, this.anno);
	}
}
