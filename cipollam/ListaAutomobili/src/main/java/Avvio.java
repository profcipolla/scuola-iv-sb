
public class Avvio {

	public static void main(String[] args) {
		Lista miaLista = new Lista();
		try {
			miaLista.add(new Automobile("RM0001", 1999, "Fiat Punto"));
			miaLista.add(new Automobile("RM0002", 1999, "Fiat Panda"));
			Automobile ferrari = new Automobile("RM0003", 2010, "Ferrari Testarossa");
			miaLista.add(ferrari);

			miaLista.print();
			miaLista.print();
			miaLista.print();
			
			Automobile lamborghini = new Automobile("RM0004", 2000, "Lamborghini Miura");
			miaLista.add2end(lamborghini);
		} catch (Exception exc) {
			System.out.println("Si è verificato l'errore " + exc.getMessage());
		}
	}
}
