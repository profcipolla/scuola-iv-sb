
public class Lista {
	private Nodo head;

	public Lista() {
		this.head = null;
	}

	/**
	 * Aggiunge un'automobile come primo nodo della lista
	 * 
	 * @param automobile
	 */
	public void add(Automobile automobile) {
		Nodo nuovoNodo = new Nodo(automobile, this.head);
		this.head = nuovoNodo;
	}

	/**
	 * Aggiunge un'automobile cone ultimo nodo della lista
	 * 
	 * @param automobile
	 */
	public void add2end(Automobile automobile) {

	}

	public void delete(String targa) {

	}

	/**
	 * 
	 * 
	 * @param targa
	 * @return
	 */
	public Automobile search(String targa) {
		return null;
	}

	/**
	 * Scrive nella console tutte le automobili presenti nella lista, a partire
	 * dalla testa
	 * 
	 */
	public void print() {
		Nodo tempPunt = this.head;

		// se ci sono elementi
		while (null != tempPunt) {
			System.out.println(tempPunt.getInfo());
			tempPunt = tempPunt.getSucc();
		}

	}
}
