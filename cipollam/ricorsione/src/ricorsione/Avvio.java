package ricorsione;

public class Avvio {

	public static void main(String[] args) {
		// il fattoriale di un numero n è il prodotto di n * (n-1) * (n-2) *....*2 * 1,
		// per esempio voglio il fattoriale di 5 devo fare 5*4*3*2*1
		// 4!= 4*3*2*1
		// 5! = 5 * 4!
		// 5 fattoriale si indica con 5!, per convenzione 0! = 1
		//

		System.out.println(fibonacciR(100));
		/*
		System.out.println(fibonacciR(1));
		
		System.out.println(fibonacciR(3));
		System.out.println(fibonacciR(11));
		System.out.println(fibonacciR(12));
		System.out.println(fibonacciR(13));
		System.out.println(fibonacciR(14));
		*/
		// System.out.println(rovesciaI("pippo"));
		// System.out.println(rovesciaR("pippo"));
		
		// System.out.println(fattorialeR(26));
		// System.out.println(fattorialeI(26));
		
		// Ragionate sul gioco delle torri di hanoi
		
		// S C D
		// Sposta il disco da T(SCD) a T(SCD)

	}

	
	public static long fibonacciR(int n) {
		// System.out.println("Start fibonacci " + n);
		if (0 == n) {
			return 0;
		}
		if (1 == n) {
			return 1;
		}
		
		return fibonacciR(n - 1) + fibonacciR(n - 2);
	}
	
	// scrivo una stringa al contrario versione ricorsiva
	public static String rovesciaR(String input) {
		if (null == input) {
			return null;
		}
		return rovesciaRPos(input, 0);
	}
	
	private static String rovesciaRPos(String input, int numCar) {
		System.out.println("Hai richiamato rovesciaRPos con numCar " + numCar);
		if (numCar == input.length() - 1) {
			System.out.println("Stai per uscire da rovesciaRPos con numCar " + numCar + " risposta " + input.charAt(numCar));
			return "" + input.charAt(numCar);
		}
		String temp = rovesciaRPos(input, numCar + 1) + input.charAt(numCar); 
		System.out.println("Stai per uscire da rovesciaRPos con numCar " + numCar + " risposta " + temp);
		return temp;
	}
	
	
	
	// scrivo una stringa al contrario versione iterativa
	public static String rovesciaI(String input) {
		if (null == input) {
			return null;
		}
		
		StringBuilder risposta = new StringBuilder();
		for (int i = input.length() - 1; i >= 0; i--) {
			risposta.append(input.charAt(i));
		}
		
		return risposta.toString();
	}

	// 0! = 1
	// N! = N * (N -1)!
	// soluzione ricorsiva
	public static long fattorialeR(long n) {
		if (0 == n) {
			return 1;
		}
		return n * fattorialeR(n - 1);
	}

	// soluzione iterativa
	public static long fattorialeI(long n) {
		long prodotto = 1;
		for (long i = n; i >= 1; i--) {
			prodotto *= i;
		}
		return prodotto;
	}

	// fibonacci(n) = fibonacci(n-1) + fibonacci(n-2)

	// scrivere, usando la ricorsione, una stringa al rovescio, dall'ultimo al primo
	// carattere
	// da pippo a oppip



}
