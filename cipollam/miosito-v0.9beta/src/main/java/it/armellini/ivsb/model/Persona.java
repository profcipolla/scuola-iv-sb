package it.armellini.ivsb.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Persona {
	private String nome;
	private String cognome;
	private String telefono;
	private String classe;
	
	public Persona(String allData) throws Exception {
		String[] dataPart = allData.split("\t");
		if (dataPart.length < 4) {
			throw new Exception(String.format("Valore record %s non corretto", allData));
		}
		this.setNome(dataPart[0]);
		this.setCognome(dataPart[1]);
		this.setTelefono(dataPart[2]);
		this.setClasse(dataPart[3]);
	}
	
	public String toString() {
		return String.format("%s\t%s\t%s\t%s", this.nome, this.cognome, this.telefono, this.classe);
	}
}
