package it.armellini.ivsb.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JsonResponse {
	// avviso, informazione, errore
	private String tipoRisposta;
	private String messaggio;
	
	// informazioni aggiuntive
	private String info;
}
